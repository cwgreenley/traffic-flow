import sys
import csv
import time
from mpl_toolkits.mplot3d import axes3d
import matplotlib
##matplotlib.use('qt4agg')
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import math as m
import itertools
import json
from scipy.optimize import least_squares
import peakutils
import scipy.signal as sig
import matplotlib as mpl
label_size = 14
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size

def importData(fileName):

  with open(fileName,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()
    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "sampleRate"  : int(infoArray[7]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fftPadding"  : int(infoArray[19])
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])

    maxQ    =  int(info['trackLength']/2)
    firstCol = 0 
    lastCol = maxQ #Decrease lastCol to only import lower Q values

    info['nRows']   =  info['fftSamples']*info['fftPadding']
    info['nCols']   =  lastCol - firstCol

    #Prepare numpy array for holding
    dataShape = [info['nRows'], info['nCols']]
    dataArray=np.empty(dataShape,dtype=float)

    midPoint = info['nRows']/2-1
    for i,row in enumerate(data):
      if(i<=midPoint):
        index = midPoint - i
      else:
        index = info['nRows'] + midPoint - i
      dataArray[index] = row[firstCol:lastCol]

  return info,infoArray,dataArray

#Pass in a parameter array 
#p[0] = height of the peak (ish...)
#p[1] = center of peak
#p[2] = FWHM
def lorentz(p,x):
  return p[0]*.5*p[2]/((x-p[1])**2 + (.5*p[2])**2)

def residuals(p,x,y):
  return lorentz(p,x) - y

def convertW(Wn,samples):
  return 2*np.pi*(Wn)/(samples)

def convertQ(Wn,trackLength):
  return 2*np.pi*(Wn)/(trackLength)

def two_largest(peakIndexes,zq):
  peakValues=np.zeros(peakIndexes.size)
  for i,pI, in enumerate(peakIndexes):
    peakValues[i] = zq[pI]
  thresholdFrac = .025 #ignore tiny peaks
  m1 = m2 = float('-inf') #Store peak values
  x1 = x2 = -1 #Store peak locations
  for x,m in enumerate(peakValues):
      if m > m2:
          if m >= m1:
              m1, m2 = m, m1            
              x1, x2 = x, x1
          else:
              m2 = m
              x2 = x
  if m1 < m2*thresholdFrac:
    result = [x2]
  elif m2 < m1*thresholdFrac:
    result = [x1]
  elif x1<x2:
    result = [x1,x2]
  else:
    result = [x2,x1]
  return np.array([peakIndexes[i] for i in result])

def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963


#Q = int(sys.argv[1]);
dataFiles = [
########################
##### On Server ########
########################
####### 2018-07 data #######

#"/shared/home/chris/201807_data/25/vel5_density0.11779_fft4096_track8192_slowP10_runs1.50e+03_numCars965_sampleRate1_numDataPoints5.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12060_fft4096_track8192_slowP10_runs1.50e+03_numCars988_sampleRate1_numDataPoints6.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12340_fft4096_track8192_slowP10_runs1.50e+03_numCars1011_sampleRate1_numDataPoints6.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12621_fft4096_track8192_slowP10_runs1.50e+03_numCars1034_sampleRate1_numDataPoints6.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12901_fft4096_track8192_slowP10_runs1.50e+03_numCars1057_sampleRate1_numDataPoints6.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13182_fft4096_track8192_slowP10_runs1.50e+03_numCars1080_sampleRate1_numDataPoints6.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13322_fft4096_track8192_slowP10_runs1.50e+03_numCars1091_sampleRate1_numDataPoints6.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13462_fft4096_track8192_slowP10_runs1.50e+03_numCars1103_sampleRate1_numDataPoints6.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13603_fft4096_track8192_slowP10_runs1.50e+03_numCars1114_sampleRate1_numDataPoints6.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13743_fft4096_track8192_slowP10_runs1.50e+03_numCars1126_sampleRate1_numDataPoints6.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13813_fft4096_track8192_slowP10_runs1.50e+03_numCars1132_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13883_fft4096_track8192_slowP10_runs1.50e+03_numCars1137_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13953_fft4096_track8192_slowP10_runs1.50e+03_numCars1143_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14023_fft4096_track8192_slowP10_runs1.50e+03_numCars1149_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14093_fft4096_track8192_slowP10_runs1.50e+03_numCars1154_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14163_fft4096_track8192_slowP10_runs1.50e+03_numCars1160_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14234_fft4096_track8192_slowP10_runs1.50e+03_numCars1166_sampleRate1_numDataPoints7.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14304_fft4096_track8192_slowP10_runs1.50e+03_numCars1172_sampleRate1_numDataPoints7.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14444_fft4096_track8192_slowP10_runs1.50e+03_numCars1183_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14584_fft4096_track8192_slowP10_runs1.50e+03_numCars1195_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14724_fft4096_track8192_slowP10_runs1.50e+03_numCars1206_sampleRate1_numDataPoints7.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14865_fft4096_track8192_slowP10_runs1.50e+03_numCars1218_sampleRate1_numDataPoints7.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.15145_fft4096_track8192_slowP10_runs1.50e+03_numCars1241_sampleRate1_numDataPoints7.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.15426_fft4096_track8192_slowP10_runs1.50e+03_numCars1264_sampleRate1_numDataPoints7.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07001_fft4096_track8192_slowP10_runs1.50e+03_numCars574_sampleRate1_numDataPoints3.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07168_fft4096_track8192_slowP10_runs1.50e+03_numCars587_sampleRate1_numDataPoints3.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07335_fft4096_track8192_slowP10_runs1.50e+03_numCars601_sampleRate1_numDataPoints3.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07502_fft4096_track8192_slowP10_runs1.50e+03_numCars615_sampleRate1_numDataPoints3.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07668_fft4096_track8192_slowP10_runs1.50e+03_numCars628_sampleRate1_numDataPoints3.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07835_fft4096_track8192_slowP10_runs1.50e+03_numCars642_sampleRate1_numDataPoints3.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07918_fft4096_track8192_slowP10_runs1.50e+03_numCars649_sampleRate1_numDataPoints4.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08002_fft4096_track8192_slowP10_runs1.50e+03_numCars656_sampleRate1_numDataPoints4.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08085_fft4096_track8192_slowP10_runs1.50e+03_numCars662_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08168_fft4096_track8192_slowP10_runs1.50e+03_numCars669_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08210_fft4096_track8192_slowP10_runs1.50e+03_numCars673_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08252_fft4096_track8192_slowP10_runs1.50e+03_numCars676_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08293_fft4096_track8192_slowP10_runs1.50e+03_numCars679_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08335_fft4096_track8192_slowP10_runs1.50e+03_numCars683_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel9_density0.08377_fft4096_track8192_slowP10_runs1.50e+03_numCars686_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel9_density0.08418_fft4096_track8192_slowP10_runs1.50e+03_numCars690_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel9_density0.08460_fft4096_track8192_slowP10_runs1.50e+03_numCars693_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel9_density0.08502_fft4096_track8192_slowP10_runs1.50e+03_numCars696_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08585_fft4096_track8192_slowP10_runs1.50e+03_numCars703_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08669_fft4096_track8192_slowP10_runs1.50e+03_numCars710_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08752_fft4096_track8192_slowP10_runs1.50e+03_numCars717_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08835_fft4096_track8192_slowP10_runs1.50e+03_numCars724_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.09002_fft4096_track8192_slowP10_runs1.50e+03_numCars737_sampleRate1_numDataPoints4.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.09169_fft4096_track8192_slowP10_runs1.50e+03_numCars751_sampleRate1_numDataPoints4.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.11711_fft4096_track16384_slowP10_runs1.50e+03_numCars1919_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.11990_fft4096_track16384_slowP10_runs1.50e+03_numCars1964_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12269_fft4096_track16384_slowP10_runs1.50e+03_numCars2010_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12547_fft4096_track16384_slowP10_runs1.50e+03_numCars2056_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12826_fft4096_track16384_slowP10_runs1.50e+03_numCars2101_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13105_fft4096_track16384_slowP10_runs1.50e+03_numCars2147_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13245_fft4096_track16384_slowP10_runs1.50e+03_numCars2170_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13384_fft4096_track16384_slowP10_runs1.50e+03_numCars2193_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13523_fft4096_track16384_slowP10_runs1.50e+03_numCars2216_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13663_fft4096_track16384_slowP10_runs1.50e+03_numCars2239_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13732_fft4096_track16384_slowP10_runs1.50e+03_numCars2250_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13802_fft4096_track16384_slowP10_runs1.50e+03_numCars2261_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13872_fft4096_track16384_slowP10_runs1.50e+03_numCars2273_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.13942_fft4096_track16384_slowP10_runs1.50e+03_numCars2284_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14011_fft4096_track16384_slowP10_runs1.50e+03_numCars2296_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14081_fft4096_track16384_slowP10_runs1.50e+03_numCars2307_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14151_fft4096_track16384_slowP10_runs1.50e+03_numCars2318_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14220_fft4096_track16384_slowP10_runs1.50e+03_numCars2330_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14360_fft4096_track16384_slowP10_runs1.50e+03_numCars2353_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14499_fft4096_track16384_slowP10_runs1.50e+03_numCars2376_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14639_fft4096_track16384_slowP10_runs1.50e+03_numCars2398_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14778_fft4096_track16384_slowP10_runs1.50e+03_numCars2421_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.15057_fft4096_track16384_slowP10_runs1.50e+03_numCars2467_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.15336_fft4096_track16384_slowP10_runs1.50e+03_numCars2513_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.06933_fft4096_track16384_slowP10_runs1.50e+03_numCars1136_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07098_fft4096_track16384_slowP10_runs1.50e+03_numCars1163_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07263_fft4096_track16384_slowP10_runs1.50e+03_numCars1190_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07428_fft4096_track16384_slowP10_runs1.50e+03_numCars1217_sampleRate1_numDataPoints7.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07593_fft4096_track16384_slowP10_runs1.50e+03_numCars1244_sampleRate1_numDataPoints7.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07758_fft4096_track16384_slowP10_runs1.50e+03_numCars1271_sampleRate1_numDataPoints7.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07841_fft4096_track16384_slowP10_runs1.50e+03_numCars1285_sampleRate1_numDataPoints7.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07923_fft4096_track16384_slowP10_runs1.50e+03_numCars1298_sampleRate1_numDataPoints8.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08006_fft4096_track16384_slowP10_runs1.50e+03_numCars1312_sampleRate1_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08088_fft4096_track16384_slowP10_runs1.50e+03_numCars1325_sampleRate1_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08130_fft4096_track16384_slowP10_runs1.50e+03_numCars1332_sampleRate1_numDataPoints8.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08171_fft4096_track16384_slowP10_runs1.50e+03_numCars1339_sampleRate1_numDataPoints8.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08212_fft4096_track16384_slowP10_runs1.50e+03_numCars1345_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08254_fft4096_track16384_slowP10_runs1.50e+03_numCars1352_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08295_fft4096_track16384_slowP10_runs1.50e+03_numCars1359_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08336_fft4096_track16384_slowP10_runs1.50e+03_numCars1366_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08377_fft4096_track16384_slowP10_runs1.50e+03_numCars1372_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08419_fft4096_track16384_slowP10_runs1.50e+03_numCars1379_sampleRate1_numDataPoints8.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08501_fft4096_track16384_slowP10_runs1.50e+03_numCars1393_sampleRate1_numDataPoints8.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08584_fft4096_track16384_slowP10_runs1.50e+03_numCars1406_sampleRate1_numDataPoints8.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08666_fft4096_track16384_slowP10_runs1.50e+03_numCars1420_sampleRate1_numDataPoints8.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08749_fft4096_track16384_slowP10_runs1.50e+03_numCars1433_sampleRate1_numDataPoints8.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08914_fft4096_track16384_slowP10_runs1.50e+03_numCars1460_sampleRate1_numDataPoints9.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.09079_fft4096_track16384_slowP10_runs1.50e+03_numCars1488_sampleRate1_numDataPoints9.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.11670_fft4096_track32768_slowP10_runs1.50e+03_numCars3824_sampleRate1_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.11948_fft4096_track32768_slowP10_runs1.50e+03_numCars3915_sampleRate1_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12226_fft4096_track32768_slowP10_runs1.50e+03_numCars4006_sampleRate1_numDataPoints2.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12504_fft4096_track32768_slowP10_runs1.50e+03_numCars4097_sampleRate1_numDataPoints2.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.12782_fft4096_track32768_slowP10_runs1.50e+03_numCars4188_sampleRate1_numDataPoints2.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13199_fft4096_track32768_slowP10_runs1.50e+03_numCars4325_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13338_fft4096_track32768_slowP10_runs1.50e+03_numCars4371_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13477_fft4096_track32768_slowP10_runs1.50e+03_numCars4416_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13616_fft4096_track32768_slowP10_runs1.50e+03_numCars4462_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13685_fft4096_track32768_slowP10_runs1.50e+03_numCars4484_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.13824_fft4096_track32768_slowP10_runs1.50e+03_numCars4530_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.13893_fft4096_track32768_slowP10_runs1.50e+03_numCars4552_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.13963_fft4096_track32768_slowP10_runs1.50e+03_numCars4575_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14032_fft4096_track32768_slowP10_runs1.50e+03_numCars4598_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14102_fft4096_track32768_slowP10_runs1.50e+03_numCars4621_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/25/vel5_density0.14310_fft4096_track32768_slowP10_runs1.50e+03_numCars4689_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14449_fft4096_track32768_slowP10_runs1.50e+03_numCars4735_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14588_fft4096_track32768_slowP10_runs1.50e+03_numCars4780_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.14727_fft4096_track32768_slowP10_runs1.50e+03_numCars4826_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel5_density0.15005_fft4096_track32768_slowP10_runs1.50e+03_numCars4917_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.06892_fft4096_track32768_slowP10_runs1.50e+03_numCars2258_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07057_fft4096_track32768_slowP10_runs1.50e+03_numCars2312_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07221_fft4096_track32768_slowP10_runs1.50e+03_numCars2366_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07385_fft4096_track32768_slowP10_runs1.50e+03_numCars2420_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07549_fft4096_track32768_slowP10_runs1.50e+03_numCars2474_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07713_fft4096_track32768_slowP10_runs1.50e+03_numCars2527_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07795_fft4096_track32768_slowP10_runs1.50e+03_numCars2554_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07877_fft4096_track32768_slowP10_runs1.50e+03_numCars2581_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.07959_fft4096_track32768_slowP10_runs1.50e+03_numCars2608_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08041_fft4096_track32768_slowP10_runs1.50e+03_numCars2635_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08082_fft4096_track32768_slowP10_runs1.50e+03_numCars2648_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08123_fft4096_track32768_slowP10_runs1.50e+03_numCars2662_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08164_fft4096_track32768_slowP10_runs1.50e+03_numCars2675_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08205_fft4096_track32768_slowP10_runs1.50e+03_numCars2689_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08246_fft4096_track32768_slowP10_runs1.50e+03_numCars2702_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08287_fft4096_track32768_slowP10_runs1.50e+03_numCars2715_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08328_fft4096_track32768_slowP10_runs1.50e+03_numCars2729_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08369_fft4096_track32768_slowP10_runs1.50e+03_numCars2742_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08451_fft4096_track32768_slowP10_runs1.50e+03_numCars2769_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08534_fft4096_track32768_slowP10_runs1.50e+03_numCars2796_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08616_fft4096_track32768_slowP10_runs1.50e+03_numCars2823_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08698_fft4096_track32768_slowP10_runs1.50e+03_numCars2850_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.08862_fft4096_track32768_slowP10_runs1.50e+03_numCars2904_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/25/vel9_density0.09026_fft4096_track32768_slowP10_runs1.50e+03_numCars2958_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",

##############################################################################################################################

#"/shared/home/chris/201807_data/11/vel5_density0.131_fft16384_track8192_slowP10_runs5.00e+03_numCars1073_sampleRate1_numDataPoints8.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.133_fft16384_track8192_slowP10_runs5.00e+03_numCars1089_sampleRate1_numDataPoints8.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.134_fft16384_track8192_slowP10_runs5.00e+03_numCars1097_sampleRate1_numDataPoints9.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.136_fft16384_track8192_slowP10_runs5.00e+03_numCars1114_sampleRate1_numDataPoints9.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.078_fft16384_track8192_slowP10_runs5.01e+03_numCars638_sampleRate1_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.079_fft16384_track8192_slowP10_runs5.00e+03_numCars647_sampleRate1_numDataPoints5.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.080_fft16384_track8192_slowP10_runs5.00e+03_numCars655_sampleRate1_numDataPoints5.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.081_fft16384_track8192_slowP10_runs5.00e+03_numCars663_sampleRate1_numDataPoints5.4e+10_pad2_halfdata_NoNorm_hamming_.csv",

#J "/shared/home/chris/201807_data/08/vel5_density0.137_fft16384_track8192_slowP10_runs7.00e+03_numCars1122_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.140_fft16384_track8192_slowP10_runs7.00e+03_numCars1146_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.143_fft16384_track8192_slowP10_runs7.00e+03_numCars1171_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.146_fft16384_track8192_slowP10_runs7.00e+03_numCars1196_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.149_fft16384_track8192_slowP10_runs7.00e+03_numCars1220_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.151_fft16384_track8192_slowP10_runs7.00e+03_numCars1236_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.154_fft16384_track8192_slowP10_runs7.00e+03_numCars1261_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#J "/shared/home/chris/201807_data/08/vel5_density0.157_fft16384_track8192_slowP10_runs7.00e+03_numCars1286_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",
##
#"/shared/home/chris/xmasData/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.081_fft16384_track8192_slowP10_runs7.00e+03_numCars663_sampleRate1_numDataPoints7.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.083_fft16384_track8192_slowP10_runs7.01e+03_numCars679_sampleRate1_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08301_fft16384_track8192_slowP10_runs5.00e+03_numCars680_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08313_fft16384_track8192_slowP10_runs5.00e+03_numCars681_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08325_fft16384_track8192_slowP10_runs5.00e+03_numCars682_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08337_fft16384_track8192_slowP10_runs5.00e+03_numCars683_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08350_fft16384_track8192_slowP10_runs5.00e+03_numCars684_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08362_fft16384_track8192_slowP10_runs5.00e+03_numCars685_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08374_fft16384_track8192_slowP10_runs5.00e+03_numCars686_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08386_fft16384_track8192_slowP10_runs5.00e+03_numCars687_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08398_fft16384_track8192_slowP10_runs5.00e+03_numCars688_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08411_fft16384_track8192_slowP10_runs5.00e+03_numCars689_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08423_fft16384_track8192_slowP10_runs5.00e+03_numCars690_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08435_fft16384_track8192_slowP10_runs5.00e+03_numCars691_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08447_fft16384_track8192_slowP10_runs5.00e+03_numCars692_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08459_fft16384_track8192_slowP10_runs5.00e+03_numCars693_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##error "/shared/home/chris/201807_data/20/vel9_density0.08472_fft16384_track8192_slowP10_runs5.00e+03_numCars694_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08484_fft16384_track8192_slowP10_runs5.00e+03_numCars695_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.085_fft16384_track8192_slowP10_runs7.00e+03_numCars696_sampleRate1_numDataPoints8.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.087_fft16384_track8192_slowP10_runs7.01e+03_numCars712_sampleRate1_numDataPoints8.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.088_fft16384_track8192_slowP10_runs7.01e+03_numCars720_sampleRate1_numDataPoints8.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.090_fft16384_track8192_slowP10_runs7.00e+03_numCars737_sampleRate1_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.092_fft16384_track8192_slowP10_runs7.01e+03_numCars753_sampleRate1_numDataPoints8.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##
#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate100_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate10_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate1_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate100_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate10_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate100_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate10_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate1_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate100_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate10_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate100_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate10_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate1_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate100_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate10_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate1_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",




####### 2018-06 data #######

## Track length 32768

## Vel 5
###"/shared/home/chris/201806_data/vel5_density0.014_fft8192_track32768_slowP10_runs2.82e+03_numCars458_sampleRate2_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate1_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate8_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate32_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs5.00e+03_numCars917_sampleRate1_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs1.28e+03_numCars917_sampleRate8_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs1.28e+03_numCars917_sampleRate32_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

###"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track32768_slowP10_runs5.00e+03_numCars1835_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",

## Vel 9

#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs5.00e+03_numCars262_sampleRate1_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs2.44e+03_numCars262_sampleRate8_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs2.44e+03_numCars262_sampleRate32_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs5.00e+03_numCars557_sampleRate1_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs2.26e+03_numCars557_sampleRate8_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs2.26e+03_numCars557_sampleRate32_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

###"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track32768_slowP10_runs5.00e+03_numCars1081_sampleRate1_numDataPoints8.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

## Track length 8192

## Vel 5

#"/shared/home/chris/201806_data/vel5_density0.015_fft16384_track8192_slowP2_runs1.01e+04_numCars122_sampleRate1_numDataPoints2.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.030_fft16384_track8192_slowP2_runs1.00e+04_numCars245_sampleRate1_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate1_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate8_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate16_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate32_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate1_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate8_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate16_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate32_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate10_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate100_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs1.28e+03_numCars229_sampleRate1000_numDataPoints4.8e+09_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate8_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate16_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate32_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate1_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate8_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate16_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate32_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",

## Vel 9

#"/shared/home/chris/201806_data/vel9_density0.009_fft16384_track8192_slowP2_runs1.01e+04_numCars73_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/201806_data/vel9_density0.018_fft16384_track8192_slowP2_runs1.00e+04_numCars147_sampleRate1_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate1_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate8_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate16_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate32_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

##$"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate1_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
##$"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate8_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
##$"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate16_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
##$"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate32_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate8_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate16_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate32_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate1_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate8_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate16_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate32_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",

## Below here need redone

####### 2018-05 data #######
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_SampleRate16.csv",
##### XMAS data #####
### vel5
#$"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP5_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#$"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#$"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP20_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP40_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP5_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP20_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP40_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#
##"/shared/home/chris/xmasData/vel5_density0.036_fft16384_track8192_slowP5_runs5.02e+03_numCars294_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.072_fft16384_track8192_slowP5_runs5.01e+03_numCars589_numDataPoints4.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.087_fft16384_track8192_slowP5_runs5.00e+03_numCars712_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.101_fft16384_track8192_slowP5_runs5.00e+03_numCars827_numDataPoints6.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP5_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.130_fft16384_track8192_slowP5_runs5.00e+03_numCars1064_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.145_fft16384_track8192_slowP5_runs5.00e+03_numCars1187_numDataPoints9.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#$"/shared/home/chris/xmasData/vel5_density0.035_fft16384_track8192_slowP10_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.070_fft16384_track8192_slowP10_runs5.00e+03_numCars573_numDataPoints4.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.084_fft16384_track8192_slowP10_runs5.00e+03_numCars688_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.098_fft16384_track8192_slowP10_runs5.00e+03_numCars802_numDataPoints6.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##$"/shared/home/chris/xmasData/vel5_density0.112_fft16384_track8192_slowP10_runs5.00e+03_numCars917_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.126_fft16384_track8192_slowP10_runs5.00e+03_numCars1032_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#overlapping peaks "/shared/home/chris/xmasData/vel5_density0.140_fft16384_track8192_slowP10_runs5.00e+03_numCars1146_numDataPoints9.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#$"/shared/home/chris/xmasData/vel5_density0.032_fft16384_track8192_slowP20_runs5.00e+03_numCars262_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.064_fft16384_track8192_slowP20_runs5.00e+03_numCars524_numDataPoints4.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.077_fft16384_track8192_slowP20_runs5.01e+03_numCars630_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.090_fft16384_track8192_slowP20_runs5.00e+03_numCars737_numDataPoints6.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.103_fft16384_track8192_slowP20_runs5.00e+03_numCars843_numDataPoints6.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#issues "/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP20_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.129_fft16384_track8192_slowP20_runs5.00e+03_numCars1056_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.026_fft16384_track8192_slowP40_runs5.02e+03_numCars212_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.052_fft16384_track8192_slowP40_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel5_density0.062_fft16384_track8192_slowP40_runs5.01e+03_numCars507_numDataPoints4.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.073_fft16384_track8192_slowP40_runs5.00e+03_numCars598_numDataPoints4.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.083_fft16384_track8192_slowP40_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.093_fft16384_track8192_slowP40_runs5.00e+03_numCars761_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.104_fft16384_track8192_slowP40_runs5.00e+03_numCars851_numDataPoints7.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
### vel9
##"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP5_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP10_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP20_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP40_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#
##"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP5_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP10_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP20_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP40_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##
##"/shared/home/chris/xmasData/vel9_density0.022_fft16384_track8192_slowP5_runs5.01e+03_numCars180_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.043_fft16384_track8192_slowP5_runs5.00e+03_numCars352_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.052_fft16384_track8192_slowP5_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP5_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.069_fft16384_track8192_slowP5_runs5.00e+03_numCars565_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.078_fft16384_track8192_slowP5_runs5.01e+03_numCars638_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
# invalid literal error"/shared/home/chris/xmasData/vel9_density0.086_fft16384_track8192_slowP5_runs5.00e+03_numCars704_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/trafData/vel9_density0.010_fft16384_track8192_slowP10_runs5.27e+02_numCars81_numDataPoints7.0e+08_halfdata_NoNorm_hamming_padding2_dec13.csv",
##"/shared/home/chris/xmasData/vel9_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP10_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.050_fft16384_track8192_slowP10_runs5.01e+03_numCars409_numDataPoints3.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.058_fft16384_track8192_slowP10_runs5.00e+03_numCars475_numDataPoints3.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.066_fft16384_track8192_slowP10_runs5.01e+03_numCars540_numDataPoints4.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.083_fft16384_track8192_slowP10_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.019_fft16384_track8192_slowP20_runs5.02e+03_numCars155_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.045_fft16384_track8192_slowP20_runs5.01e+03_numCars368_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP20_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP20_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/xmasData/vel9_density0.068_fft16384_track8192_slowP20_runs5.00e+03_numCars557_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.075_fft16384_track8192_slowP20_runs5.00e+03_numCars614_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##done w/ issues "/shared/home/chris/xmasData/vel9_density0.015_fft16384_track8192_slowP40_runs5.04e+03_numCars122_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##done w/ issues "/shared/home/chris/xmasData/vel9_density0.029_fft16384_track8192_slowP40_runs5.01e+03_numCars237_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
##done w/ issues "/shared/home/chris/xmasData/vel9_density0.035_fft16384_track8192_slowP40_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
##done w/ issues "/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP40_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
##done w/ issues "/shared/home/chris/xmasData/vel9_density0.047_fft16384_track8192_slowP40_runs5.00e+03_numCars385_numDataPoints3.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP40_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.059_fft16384_track8192_slowP40_runs5.00e+03_numCars483_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#####################
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding4.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding8.csv",
### 
#"/shared/home/chris/trafData/vel5_density0.120_fft1024_track8192_slowP10_runs4.97e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft16384_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints1.0e+11_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft32768_track8192_slowP10_runs1.55e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.135_fft4096_track8192_slowP10_runs1.10e+04_numCars1105_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.138_fft4096_track8192_slowP10_runs1.08e+04_numCars1130_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.140_fft4096_track8192_slowP10_runs1.07e+04_numCars1146_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.140_fft32768_track8192_slowP10_runs6.65e+02_numCars1146_numDataPoints2.5e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.145_fft4096_track8192_slowP10_runs1.03e+04_numCars1187_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.150_fft4096_track8192_slowP10_runs9.94e+03_numCars1228_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.116_fft4096_track8192_slowP10_runs2.06e+04_numCars950_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.107_fft4096_track8192_slowP20_runs2.23e+04_numCars876_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.087_fft4096_track8192_slowP40_runs2.74e+04_numCars712_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.075_fft4096_track8192_slowP50_runs3.18e+04_numCars614_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft2048_track8192_slowP50_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.060_fft2048_track8192_slowP50_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.080_fft2048_track8192_slowP50_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.100_fft2048_track8192_slowP50_runs2.98e+04_numCars819_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
### short tests
#"/shared/home/chris/trafData/vel5_density0.025_fft16384_track8192_slowP10_runs4.48e+02_numCars204_numDataPoints1.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.050_fft16384_track8192_slowP10_runs3.43e+02_numCars409_numDataPoints2.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.060_fft16384_track8192_slowP10_runs4.97e+02_numCars491_numDataPoints4.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.070_fft16384_track8192_slowP10_runs4.79e+02_numCars573_numDataPoints4.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.080_fft16384_track8192_slowP10_runs4.65e+02_numCars655_numDataPoints5.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.090_fft16384_track8192_slowP10_runs4.55e+02_numCars737_numDataPoints5.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.100_fft16384_track8192_slowP10_runs3.20e+02_numCars819_numDataPoints4.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.110_fft16384_track8192_slowP10_runs4.06e+02_numCars901_numDataPoints6.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#
#"/shared/home/chris/trafData/vel9_density0.020_fft16384_track8192_slowP10_runs4.86e+02_numCars163_numDataPoints1.3e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.030_fft16384_track8192_slowP10_runs4.98e+02_numCars245_numDataPoints2.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft16384_track8192_slowP10_runs5.03e+02_numCars327_numDataPoints2.7e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.050_fft16384_track8192_slowP10_runs4.92e+02_numCars409_numDataPoints3.3e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.060_fft16384_track8192_slowP10_runs4.97e+02_numCars491_numDataPoints4.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.070_fft16384_track8192_slowP10_runs5.00e+02_numCars573_numDataPoints4.7e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#
#"/shared/home/chris/trafData/vel5_density0.080_fft16384_track8192_slowP5_runs9.31e+02_numCars655_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.077_fft16384_track8192_slowP10_runs1.01e+03_numCars630_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.070_fft16384_track8192_slowP20_runs1.02e+03_numCars573_numDataPoints9.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.057_fft16384_track8192_slowP40_runs1.01e+03_numCars466_numDataPoints7.7e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.050_fft16384_track8192_slowP50_runs3.50e+02_numCars409_numDataPoints2.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#
#"/shared/home/chris/trafData/vel9_density0.050_fft16384_track8192_slowP5_runs9.55e+02_numCars409_numDataPoints6.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.045_fft16384_track8192_slowP10_runs1.01e+03_numCars368_numDataPoints6.1e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft16384_track8192_slowP20_runs1.04e+03_numCars327_numDataPoints5.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.033_fft16384_track8192_slowP40_runs9.94e+02_numCars270_numDataPoints4.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.027_fft16384_track8192_slowP50_runs3.59e+02_numCars221_numDataPoints1.3e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
###
#"/shared/home/chris/trafData/vel15_density0.028_fft2048_track8192_slowP10_runs4.26e+04_numCars229_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel15_density0.043_fft8192_track8192_slowP10_runs1.73e+04_numCars352_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel22_density0.019_fft2048_track8192_slowP10_runs6.30e+04_numCars155_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
## OLD
#"/shared/home/chris/trafData/downloaded/vel5_density0.140_fft2048_track8192_runs2.13e+04_numCars1146_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/shared/home/chris/trafData/downloaded/vel9_density0.06_fft2048_track8192_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/shared/home/chris/trafData/downloaded/vel9_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#########################
####### New files #######
#########################
#"/data/trafficFlowData/20171138_newdata/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#####################
####### Vel 5 #######
#####################
#"/data/trafficFlowData/vel5_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.04_fft2048_track8192_runs1.49e+03_numCars327_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.06_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.08_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.09_fft2048_track8192_runs3.31e+04_numCars737_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.10_fft2048_track8192_runs2.98e+03_numCars819_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.11_fft2048_track8192_runs2.71e+04_numCars901_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.12_fft2048_track8192_runs4.96e+02_numCars983_numDataPoints1.0e+09_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.13_fft2048_track8192_runs2.29e+04_numCars1064_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.135_fft2048_track8192_runs4.42e+03_numCars1105_numDataPoints1.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.138_fft2048_track8192_runs4.32e+03_numCars1130_numDataPoints1.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.140_fft2048_track8192_runs2.13e+04_numCars1146_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.145_fft2048_track8192_runs2.06e+04_numCars1187_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.150_fft2048_track8192_runs1.99e+04_numCars1228_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.20_fft2048_track8192_runs5.96e+02_numCars1638_numDataPoints2.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.30_fft2048_track8192_runs9.93e+02_numCars2457_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.40_fft2048_track8192_runs7.45e+02_numCars3276_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.50_fft2048_track8192_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.60_fft2048_track8192_runs4.96e+02_numCars4915_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.70_fft2048_track8192_runs4.25e+02_numCars5734_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.80_fft2048_track8192_runs3.72e+02_numCars6553_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/old/vel5_density0.05_fft2048_track8192_runs1e+04_numCars409_halfdata_Welch.csv",
#####################
####### Vel 9 #######
#####################
#"/data/trafficFlowData/vel9_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.02_fft2048_track8192_runs1.50e+05_numCars163_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.04_fft2048_track8192_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.06_fft2048_track8192_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.08_fft2048_track8192_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.082_fft2048_track8192_runs7.28e+03_numCars671_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.083_fft2048_track8192_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.0835_fft2048_track8192_runs7.14e+03_numCars684_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.084_fft2048_track8192_runs7.10e+03_numCars688_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.085_fft2048_track8192_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.087_fft2048_track8192_runs6.86e+03_numCars712_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.088_fft2048_track8192_runs6.78e+03_numCars720_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.089_fft2048_track8192_runs6.70e+03_numCars729_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.09_fft2048_track8192_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.095_fft2048_track8192_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.10_fft2048_track8192_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.15_fft2048_track8192_runs3.98e+03_numCars1228_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.20_fft2048_track8192_runs2.98e+03_numCars1638_numDataPoints1.0e+10_halfdata_Hamm.csv",
##"/data/trafficFlowData/vel9_density0.30_fft2048_track8192_runs1.99e+03_numCars2457_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.50_fft2048_track8192_runs1.19e+03_numCars4096_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
####### Vel 12 ######
#####################
#"/data/trafficFlowData/vel12_density0.020_fft2048_track8192_runs8.99e+03_numCars163_numDataPoints3.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.030_fft2048_track8192_runs1.99e+04_numCars245_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.040_fft2048_track8192_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.045_fft2048_track8192_runs1.06e+04_numCars368_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.050_fft2048_track8192_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.060_fft2048_track8192_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.070_fft2048_track8192_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.080_fft2048_track8192_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.090_fft2048_track8192_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.100_fft2048_track8192_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.450_fft2048_track8192_runs1.06e+03_numCars3686_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.550_fft2048_track8192_runs1.08e+03_numCars4505_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.650_fft2048_track8192_runs9.17e+02_numCars5324_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.750_fft2048_track8192_runs7.94e+02_numCars6144_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
##### Track 4096 ####
#####################
#"/data/trafficFlowData/track4096/vel5_density0.070_fft2048_track4096_runs1.71e+04_numCars286_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.130_fft2048_track4096_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.138_fft2048_track4096_runs8.64e+03_numCars565_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.145_fft2048_track4096_runs8.23e+03_numCars593_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.160_fft2048_track4096_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.200_fft2048_track4096_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
##
#"/data/trafficFlowData/track4096/vel9_density0.040_fft2048_track4096_runs1.50e+04_numCars163_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.060_fft2048_track4096_runs9.96e+03_numCars245_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.080_fft2048_track4096_runs1.19e+04_numCars327_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.090_fft2048_track4096_runs1.06e+04_numCars368_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.100_fft2048_track4096_runs9.55e+03_numCars409_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.150_fft2048_track4096_runs6.36e+03_numCars614_numDataPoints8.0e+09_halfdata_Hamm.csv",
######################
##### Track 16384 ####
######################
#"/data/trafficFlowData/track16384/vel5_density0.130_fft2048_track16384_runs2.29e+03_numCars2129_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel5_density0.138_fft2048_track16384_runs2.16e+03_numCars2260_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel5_density0.145_fft2048_track16384_runs2.06e+03_numCars2375_numDataPoints1.0e+10_halfdata_Hamm.csv",
##
#"/data/trafficFlowData/track16384/vel9_density0.060_fft2048_track16384_runs4.97e+03_numCars983_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.070_fft2048_track16384_runs4.26e+03_numCars1146_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.075_fft2048_track16384_runs3.98e+03_numCars1228_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.080_fft2048_track16384_runs3.73e+03_numCars1310_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.083_fft2048_track16384_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.085_fft2048_track16384_runs3.51e+03_numCars1392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.090_fft2048_track16384_runs3.31e+03_numCars1474_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.095_fft2048_track16384_runs3.14e+03_numCars1556_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.100_fft2048_track16384_runs2.98e+03_numCars1638_numDataPoints1.0e+10_halfdata_Hamm.csv",
################################
#### VARYING TRACK LENGTH ######
###############################
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track2048_runs3.00e+04_numCars163_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track4096_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track8192_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track16384_runs3.73e+03_numCars1310_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track32768_runs1.86e+03_numCars2621_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track2048_runs2.89e+04_numCars169_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track4096_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track8192_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track16384_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track32786_runs1.79e+03_numCars2721_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track2048_runs2.84e+04_numCars172_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track4096_runs1.42e+04_numCars344_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track8192_runs7.10e+03_numCars688_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track16384_runs3.55e+03_numCars1376_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track32768_runs1.77e+03_numCars2752_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track2048_runs2.81e+04_numCars174_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track4096_runs1.40e+04_numCars348_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track8192_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track16384_runs3.51e+03_numCars1392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track32768_runs1.75e+03_numCars2785_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track2048_runs2.74e+04_numCars178_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track4096_runs1.37e+04_numCars356_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track8192_runs6.86e+03_numCars712_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track16384_runs3.43e+03_numCars1425_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track32768_runs1.71e+03_numCars2850_numDataPoints1.0e+10_halfdata_Hamm.csv",
#
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track2048_runs1.84e+04_numCars266_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track4096_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track8192_runs4.59e+03_numCars1064_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track16384_runs2.29e+03_numCars2129_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track32768_runs1.15e+03_numCars4259_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track2048_runs1.78e+04_numCars274_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track4096_runs8.91e+03_numCars548_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track8192_runs4.45e+03_numCars1097_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track16384_runs2.22e+03_numCars2195_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track32786_runs1.11e+03_numCars4393_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track2048_runs1.73e+04_numCars282_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track4096_runs8.64e+03_numCars565_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track8192_runs4.32e+03_numCars1130_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track16384_runs2.16e+03_numCars2260_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track32768_runs1.08e+03_numCars4521_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track2048_runs1.68e+04_numCars290_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track4096_runs8.40e+03_numCars581_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track8192_runs4.20e+03_numCars1163_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track16384_runs2.10e+03_numCars2326_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track32768_runs1.05e+03_numCars4653_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track2048_runs1.63e+04_numCars299_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track4096_runs8.16e+03_numCars598_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track8192_runs4.08e+03_numCars1196_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track16384_runs2.04e+03_numCars2392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track32768_runs1.02e+03_numCars4784_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
## Phi Vel 9 and 5 ##
#####################
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP10_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP50_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP10_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP50_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP10_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP50_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP10_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP50_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP5_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP10_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP15_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP25_runs3.31e+03_numCars737_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP30_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP40_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP50_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.050_fft2048_track8192_slowP75_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP10_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP50_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
##########################
### P50 Vel 9 and 5 ###
##########################
#"/data/trafficFlowData/highP/vel9_density0.010_fft2048_track8192_runs6.03e+03_numCars81_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.020_fft2048_track8192_runs3.00e+03_numCars163_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.030_fft2048_track8192_runs1.99e+03_numCars245_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.050_fft2048_track8192_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.060_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.070_fft2048_track8192_runs8.52e+02_numCars573_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.075_fft2048_track8192_runs7.95e+02_numCars614_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.080_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.085_fft2048_track8192_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm.csv",
##########################
# Various slow P for V=9 #
##########################
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP5_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP5_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.095_fft2048_track8192_slowP5_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.100_fft2048_track8192_slowP5_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.110_fft2048_track8192_slowP5_runs5.42e+03_numCars901_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP10_runs3.31e+03_numCars737_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP10_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP10_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP20_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP20_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP20_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.080_fft2048_track8192_slowP20_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP20_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP25_runs4.26e+03_numCars573_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP25_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP25_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP30_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP30_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP30_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP30_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP30_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP40_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP40_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP40_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP40_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP40_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP40_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.040_fft2048_track8192_slowP50_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP50_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP50_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP50_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP50_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP50_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#
#"/data/trafficFlowData/phifft/vel9_density0.050_fft2048_track8192_slowP75_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#######################################################
### Finite size effects due to track size difference? #
#######################################################
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_phi_finiteSizeEffects_test.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+05_numCars339_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+04_numCars679_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+04_numCars1359_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track32768_slowP10_runs1.80e+04_numCars2719_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track65536_slowP10_runs8.98e+03_numCars5439_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
]

saveOutput = True
seeking = "FreePeak"
verbose=False #Controls printing of debug statements

markers = itertools.cycle(('o', 'v', '8', 's', '^','p', '*', 'h', '<', 'H', 'D', '>','d'))
colors = itertools.cycle(('b', 'g', 'r', 'brown','c', 'm', 'y', 'k','darksalmon'))
colors2 = itertools.cycle(('b', 'g', 'r', 'brown','c', 'm', 'y', 'k','darksalmon'))

HWplt = plt.figure(figsize=(10,6))
ax1 = HWplt.add_subplot(1,1,1)
#dfig = plt.figure()
#dax = HWplt.add_subplot(2,1,2)
#XXX fftFig = plt.figure()
#XXX realax = fftFig.add_subplot(2,1,1)
#XXX imgax = fftFig.add_subplot(2,1,2)
#ax1 = plt.subplot2grid((2,4),(0,0),colspan=4)
#ax2 = plt.subplot2grid((2,4),(1,0),colspan=4)

slopeVals = np.zeros(len(dataFiles))
slopeSigmas = np.zeros(len(dataFiles))
densityVals = np.zeros(len(dataFiles))
slowPVals = np.zeros(len(dataFiles))

z=0
for fileCounter,file in enumerate(dataFiles):

  start = time.time()

  info,infoArray,z=importData(file)

  end = time.time()
  print("File load time: {}".format(end-start))

  trackLength = info['trackLength']
  sampleRate  = info['sampleRate']
  fftSamples  = info['fftSamples']
  fftRuns     = info['fftRuns']
  numCars     = info['numCars']
  maxVel      = info['maxVel']
  slowP       = info['slowP']
  density     = info['density']
  maxQ        = info['nCols']
  zqLen       = info['nRows']

  slowPVals[fileCounter]=slowP
  densityVals[fileCounter]=density
  densityString = "Density: {:.4f}".format(density)
  legendString = densityString
  #legendString = densityString+"Slow P: {:d}%".format(slowP)
  #legendString = "Track Length: {}".format(trackLength)
  trackString = "Track: {}".format(trackLength)
  infoText=""
  #infoText=infoText+"Num of time steps in each FFT: {}, ".format(fftSamples)
  #infoText=infoText+"Num of FFTs: {}\n".format(fftRuns)
  #infoText=infoText+"Number of cars: {}, ".format(numCars)
  infoText=infoText+densityString
  print info
#  print "Track: {}\n".format(trackLength)
  
  
  jamBool = False
  failedCount=0
  
  #Iterate over q, range(1,... skips the DC column
  qVals = range(1,4*info['nCols']/(info['maxVel']),1) 
  lenQVals = len(qVals)

  FWHM=np.empty(maxQ)
  FWHM.fill(np.NAN)
  fitFWHM=np.empty(maxQ)
  fitFWHM.fill(np.NAN)
  peakArea=np.empty(maxQ)
  peakArea.fill(np.NAN)

  qRad=np.empty(lenQVals)
  qRad.fill(np.NAN)
  wPeakRad=np.empty(lenQVals)
  wPeakRad.fill(np.NAN)
  #dM=np.empty(len(qVals))
  #dM.fill(np.NAN)

#XXX  fig = plt.figure()
#XXX  ax = fig.add_subplot(1,1,1)
 
  #priorL=[0,0]

  for iQ,q in enumerate(qVals):
  
    ##### NORMALIZE AND ROLLING

    normFactor = np.sum(z[:,q])
    rollFactorFree = -int( q*float(maxVel-slowP/100.0)
                       *float(2*sampleRate*fftSamples)
                       /float(trackLength)
                     )
    rollFactorJam = int(q*1.5*float(fftSamples)/float(trackLength))
    if seeking=="FreePeak": rollFactor = rollFactorFree;
    if seeking=="JamPeak": rollFactor = rollFactorJam;
    zq = np.roll(z[:,q]/normFactor,rollFactor)

      
    ##### CHECK SIGNAL TO NOISE RATIO

    average = np.sum(zq)/np.size(zq)
    ratioMaxAvg = np.max(zq)/average
    if ratioMaxAvg < 2.5: #2.5 experimentally determined to be reasonable
      print("SNR too low, processing stopped at q={}".format(q))
      break

    ##### SMOOTHING

    #Smoothing Filter with increasing length as the peak spreads out
    #Constants obtained from fitting a curve of ratioMaxAvg vs desired filterLengths
    filterLength = int(830*ratioMaxAvg**(-0.57)*.5)
    if filterLength<3 or q in [1,2]:
      filterLength = 3
    elif filterLength%2==0: #Must be odd
      filterLength += 1

    if q in range(1,100,20)+range(300,np.size(zq),50):
      print "rollFactor for q={}: {}, filterLen = {}, max/avg = {}"\
            .format(q,rollFactor,filterLength,ratioMaxAvg)

    smoothedData = np.log(zq)
    smoothedData = sig.savgol_filter(smoothedData,filterLength,1,mode='wrap')
    if ratioMaxAvg<200:
      smoothedData = sig.savgol_filter(smoothedData,filterLength,1,mode='wrap')
      smoothedData = sig.savgol_filter(smoothedData,filterLength,1,mode='wrap')
    if ratioMaxAvg<50: 
      loopnum = int((q/200.0)+1)#smooth more times for higher q values
      for i in range(loopnum):
        smoothedData = sig.savgol_filter(smoothedData,filterLength,1,mode='wrap')

    smoothedData += 1000 #offset to keep everything positive otherwise peakutils fails
    peakIndexes = peakutils.peak.indexes( smoothedData, 
                                          min_dist=filterLength*2, 
                                          thres=.6 )
    if peakIndexes.size == 0:
      failedCount += 1
      continue

    #If too many peaks are found, get the biggest two
    if peakIndexes.size > 2:
      if verbose: print "Found {} peaks.".format(peakIndexes.size)
      peakIndexes = two_largest(peakIndexes,smoothedData)#returns locations of max peaks in peakIndexes array 

    if q<20 and verbose:
      print "q = {}, peakIndexes: {},filterLength: {}"\
          .format(q,peakIndexes,filterLength)


    if peakIndexes.size > 1:
#      if q<trackLength/(5*maxVel) and q%10==0: print("{},{},{}".format(q,peakIndexes[0],peakIndexes[1]))
      if jamBool==False: print "First jam peak detected at q={}".format(q)
      jamBool = True #We have now seen two peaks at least once

    critDen = criticalDensity(maxVel,slowP,trackLength)
    if jamBool==False and (seeking=="JamPeak" or density>.99*critDen):
      print("Peaks not yet seperated. Skipping q={}".format(q))
      continue
    if jamBool and peakIndexes.size == 1 and seeking=="JamPeak":
      print("Missing peak. Skipping q={}".format(q))
      continue

#
##    if peakIndexes.size > 2:
##      if peakIndexes[2] > fftSamples*2-200: #ignore the wrap around
##        peakIndexes = peakIndexes[0:2]
##      else:
##        print("Error: Found more than two peaks. Discarding q={}.".format(q))
#
#    if peakIndexes.size == 0:
#      print("Error: No peaks found. Discarding q={}".format(q))
#      continue
#
#    if jamBool and peakIndexes.size == 1:# and peakIndexes[0]>2500:
#      print("Wrong peak? Skipping q={}".format(q))
#      #XXX continue
#
    errorFlag = 0
    
    ## Find peak closest to center
    if(peakIndexes.size == 2):
      diff1 = abs(peakIndexes[0] - fftSamples)
      diff2 = abs(peakIndexes[1] - fftSamples)
      if diff1<diff2: peakI = peakIndexes[0]
      else: peakI = peakIndexes[1]
    else: 
      peakI = peakIndexes[0]

    maxVal = zq[peakI]
  
    halfMax = maxVal/2.0
    tenthMax = maxVal/10.0
    
    
    ################################################################
    #Find FWHM via 'brute force' method
    ################################################################
    fitDataX1 = -1
    fitDataX2 = -1
    highSideIndex = -1
    lowSideIndex = -1
    
    #iterate forward from the peak looking for 
    #the high side index of half max
    for i in xrange(peakI+1,zqLen-3):
      slope = (smoothedData[i+3]-smoothedData[i-1])/4.0
      #print "zq[{}] = {},slope={}".format(i,zq[i],slope)
      #Stop if under half max or if decently far away and the slope becomes positive
      #"Decently far away" grows with q
      if  zq[i] <= halfMax or (i>(peakI+filterLength) and slope>0):
        highSideIndex = i-.5
        break
    if highSideIndex == -1:
      print "High side of half width not found. Discarding q = {}".format(q)
      errorFlag = 1
      failedCount+=1
      continue

    #Iterate more to get data points for use in fitting fuction
    for i in xrange(int(highSideIndex+.5),zqLen-5):
      slope = (smoothedData[i+3]-smoothedData[i-1])/4.0
      fitDataX2 = i
      if zq[i] <= tenthMax or slope>0:
        break

    #Find the high side of where to stop when calculating area under peak
    areaMaxX = fitDataX2 #In case we are super close to the end
    for i in xrange(int(fitDataX2),zqLen-5):
      if fitDataX2>(zqLen-5):
        areaMaxX = zqLen-1
        break
      slope = (smoothedData[i+4]-smoothedData[i-2])/6
      areaMaxX = i
      if slope>0:
        areaMaxX = i
        #if areaMaxX>zqLen:
        #  areaMaxX = zqLen
        break

    ###########################################################

    #iterate backward from the peak looking for 
    #the low side index of half max
    for i in xrange(peakI-1,2,-1): #From peakI back to i=2+1
      slope = (smoothedData[i+1]-smoothedData[i-3])/4.0
      #Stop if under half max or if decently far away and the slope becomes positive
      #"Decently far away" grows with q
      if zq[i] <= halfMax or (i<(peakI-filterLength) and slope<0):
        lowSideIndex = i+.5
        break
    #Error Checking
    if lowSideIndex == -1:
      print "Low side of half width not found. Discarding q = {}".format(q)
      errorFlag = 1
      failedCount+=1
      continue


    #Iterate more to get data points for use in fitting fuction
    for i in xrange(int(lowSideIndex-.5),3,-1):
      slope = (smoothedData[i+1]-smoothedData[i-3])/4.0
      fitDataX1 = i
      if zq[i] <= tenthMax or slope<0:
        break


    
    #Find the low side of where to stop when calculating area under peak
    areaMinX = fitDataX1 #In case we are super close to zero
    for i in xrange(int(fitDataX1),2,-1): #down to 3
      if fitDataX1<3:
        areaMinX = 0
        break
      slope = (smoothedData[i+2]-smoothedData[i-4])/6.0
      areaMinX = i
      if slope<0:
        areaMinX = i
        #if areaMinX<0:
        #  areaMinX = 0
        break

    #### Error checking ####
    if highSideIndex <= lowSideIndex:
      print "High side lower than low side for q={}".format(q)
      errorFlag = 1
      failedCount+=1
      continue
    
    if (lowSideIndex <= 0) or (highSideIndex >= zqLen):
      print "low or high side index out of range for q={}".format(q)
      failedCount+=1
      errorFlag = 1
      continue

    #Adjust fitData to be fairly symetric
    if (fitDataX2-fitDataX1)>20:
      fitDataX1 = int(peakI-min(abs(peakI-fitDataX1),abs(peakI-fitDataX2)))
      fitDataX2 = int(peakI+min(abs(peakI-fitDataX1),abs(peakI-fitDataX2)))
    
    #Calculate FWHM
    fwhm = highSideIndex - lowSideIndex

    ### Error Checking ###
    if fwhm > zqLen/2:
      print "FWHM out of bounds:"
      print q,fwhm,lowSideIndex,highSideIndex
      failedCount+=1
      errorFlag = 1
      continue
    if peakI<fwhm:
      print "Peak too close to zero. Discarding q={}".format(q)
      errorFlag = 1
      failedCount+=1
      continue
    
    #Save to array of full widths
    FWHM[q] = fwhm
    peakArea[q] = np.sum(zq[areaMinX:areaMaxX])
    if verbose:  print "fwhm: {}, peakArea: {} for q={}".format(fwhm,peakArea[q],q)

    
    ################################################################
    #Find FWHM via a non-linear least sqrs fit to a lorenzian
    ################################################################
    if (fitDataX1 < 0) or (fitDataX2 >= len(zq)):
      print "fitDataX1 or X2 out of range, discarding q={}".format(q)
      errorFlag = 1
      continue

    xData = np.arange(fitDataX1,fitDataX2+1)
    yData = np.empty(len(xData))
    for i,j in enumerate(xData):
      yData[i] = zq[j]
      #else: yData[i] = smoothedData[j]
  
    x0 = peakI; #guess for peak location
    guessRange = range(1,int(fwhm/2+1),int(fwhm/5+1))
    if len(guessRange)==0: guessRange=[1]
    for g,guess in enumerate(guessRange):
      p0 = np.array([maxVal,x0,guess]) #initial parameter guesses
      res = least_squares(residuals, p0, args=(xData, yData))
      A = res.x[0]
      X0 = res.x[1]
      L = res.x[2]
      if (X0 < (x0-fwhm/4-1)) or (X0 > (x0+fwhm/4+1)):
        print "Peak location of {} too far from guess of {}. FWHM={}. q={}".format(X0,x0,fwhm,q)
        tryAgain=True
      elif (L < (fwhm*0.8-2)) or (L > (fwhm*1.2+2)):
        print "Fit FWHM of {} much different than guess of {} for q={}".format(L,fwhm,q)
        tryAgain=True
      else:
        tryAgain=False
      
      if not tryAgain: #Found valid fit, can stop guessing
        skip=False
        break
      elif tryAgain and g>=len(guessRange)-1:
        print "Failed to find fit for q={}".format(q)
        failedCount+=1
        skip=True
        errorFlag=1
        break
      else:
        #print "Trying again"
#        smoothedData = sig.savgol_filter(smoothedData,filterLength+30,1,mode='wrap')
#        peakIndexes = peakutils.peak.indexes(smoothedData, thres=0.1, min_dist=filterLength*2)
#        #If too many peaks are found, get the biggest two
#        if peakIndexes.size > 2:
#          if verbose: print "Found {} peaks.".format(peakIndexes.size)
#          peakIndexes = two_largest(peakIndexes,smoothedData)#returns locations of max peaks in peakIndexes array 
#        x0 = peakIndexes[0]
        continue

    if peakIndexes.size>1 and (seeking == "JamPeak" or density>.99*critDen):
      peakSeperation = peakIndexes[1]-peakIndexes[0]
      if peakSeperation < 3*L:
        print("Peaks not seperated enough. Skipping q={}".format(q))
        continue


#    if iQ>1:
#      L0 = priorL[0]
#      L1 = priorL[1]
#      priorSlope = 
#      if ((L < priorL*.5) or (L> priorL*1.4)):
#        print "Half width of {} too different from prior value of {} for q={}".format(L,priorL,q)
#        errorFlag=1
#    else:
#      priorL[0][0] = priorL[1]
#      priorL[1] = L

    ###################################
    #if q in ([1,5,10,50]+range(100,np.size(zq),300)):#errorFlag==1:
    if q in range(1,20,2) and 0:
      #XXX
#      fig = plt.figure()
#      ax = fig.add_subplot(1,1,1)
      c=colors2.next()
      #c='k'
      ax.set_title(densityString)
      ax.plot(((zq)),'o-',ms=2,linewidth=1,color=c,label="q:{},peakQ:{}".format(q,peakI)) 
      ax.plot(np.e**(smoothedData-1000),'-',color=c)
      yMin,yMax=ax.get_ylim()
#      yMin=(zq[peakI]/yMax-.05)*yMax
#      yMax=(zq[peakI]/yMax+.05)*yMax
#      if yMin<0: yMin=0
#XXX      ax.plot([peakI,peakI],[yMin,yMax],"-",color=c,linewidth=2)
#   #   ax.plot([highSideIndex,highSideIndex],[0,yMax],"-",color=c,linewidth=4)
#      ax.plot([fitDataX2,fitDataX2],[0,yMax],"-",color=c,linewidth=2)
#   #   ax.plot([areaMaxX,areaMaxX],[0,yMax],"-",color=c,linewidth=1)
#   #   ax.plot([lowSideIndex,lowSideIndex],[0,yMax],"-",color=c,linewidth=4)
#      ax.plot([fitDataX1,fitDataX1],[0,yMax],"-",color=c,linewidth=2)
#   #   ax.plot([areaMinX,areaMinX],[0,yMax],"-",color=c,linewidth=1)
      ax.plot([peakI-fwhm/2.0,peakI+fwhm/2.0],[halfMax,halfMax],"-",color=c,linewidth=2)
      xFitPoints = np.arange(fitDataX1,fitDataX2,.2)
      yFitPoints = (lorentz(res.x,xFitPoints))
      try: xFitPoints
      except NameError: 
        print "NameError for q={}".format(q)
      else: 
        ax.plot(xFitPoints,yFitPoints,'-*',color=c)
        halfMaxFit = np.amax(yFitPoints)/2.0
        ax.plot([X0-L/2.0,X0+L/2.0],[halfMaxFit,halfMaxFit],"-",color=c,linewidth=4)
      ax.set_ylim(bottom=0)
     # ax.set_xlim((16300,16600))
      plt.legend(loc="upper right")

#    ######################################
#    ### Save single slice data to file ###
#    ######################################
#    if q == 3:
#      xFitPoints = np.arange(fitDataX1,fitDataX2,.2)
#      yFitPoints = (lorentz(res.x,xFitPoints))
#      info['Q'] = q
#
#      outputFileName = file+"_sliceData.csv"
#      with open(outputFileName,'w') as outputFile:
#        outputFile.truncate()
#  
#        json.dump(info,outputFile)
#
#        outputFile.write("\nomega\n")
#        for w in zq:
#          outputFile.write("{:.6e},".format(w))
#  
#        outputFile.write("\nxFitPoints\n")
#        for x in xFitPoints:
#          outputFile.write("{:.6e},".format(x))
#  
#        outputFile.write("\nyFitPoints\n")
#        for y in yFitPoints:
#          outputFile.write("{:.6e},".format(y))
#  
#        outputFile.write("\n")
#        outputFile.write("\n")
#        outputFile.write("\n")
#      print("Saved slice data to file:\n"+outputFileName+"\n")

    #####################################

    if skip or errorFlag: #Skip this q value
      #print "would have skipped"
      continue
    if verbose: print "A = {}, X0 = {}, L = {}".format(A,X0,L)
    fitFWHM[q] = L
####################
#End of qVals loop
####################

  areaArray = peakArea[qVals]
  HWHMarray = np.array([convertW(fw/2.0/sampleRate,zqLen) for fw in fitFWHM[qVals]])
#XXX  HWHMarrayS = sig.savgol_filter(HWHMarray,7,1,mode='interp')
#XXX  HWHMarrayS = sig.savgol_filter(HWHMarray,13,1,mode='interp')
#XXX  for x in range(5):
#XXX    HWHMarrayS = sig.savgol_filter(HWHMarrayS,29,1,mode='interp')

  qValsRadians= np.array([convertQ(qV,trackLength) for qV in qVals])

  m = markers.next()
  c = colors.next()

#  ###############################
#  #### Linear fit to low q region
#  ###############################
#  linFitParam,covariance = np.polyfit(qValsRadians[40:100],HWHMarray[40:100],1,cov=True)
#  M = linFitParam[0]
#  b = linFitParam[1]
#  linFitXdata = qVals
#  dM, db = np.sqrt(np.diagonal(covariance))
#  print "Slope: {:.2e} +/- {:.1e}, y-int: {:.2e} +/- {:.1e}".format(M,dM,b,db)
#  linFitYvals = [M*x+b for x in qValsRadians]

#XXX  ###############################
#XXX  #### derivative
#XXX  ###############################
#XXX  #derivArray = np.zeros(len(qVals))
#XXX  gradArray = np.gradient(HWHMarrayS,qValsRadians[2]-qValsRadians[1])
#XXX  #for i in qVals[3:len(qVals)-4]:
#XXX  #  linFitParam,covariance = np.polyfit(qValsRadians[i-3:i+4],HWHMarrayS[i-3:i+4],1,cov=True)
#XXX  #  derivArray[i] = linFitParam[0]
#XXX
#XXX  #derivArray = sig.savgol_filter(derivArray,11,1,mode='interp')
#XXX  dax.grid(b=True, which='major', color='k', linestyle='-')
#XXX  #dax.plot(qVals,derivArray,"-o",color=c,label="SlowP: {}".format(slowP))
#XXX  dax.plot(qVals,gradArray,"-",linewidth=3,color=c)
#XXX
#XXX  
#XXX  ###############################
#XXX  #### fourier transform
#XXX  ###############################
#XXX  fullHWHMarray = np.hstack((HWHMarray,np.flipud(HWHMarray)[1:]))
#XXX  fftOutput=np.fft.fft(fullHWHMarray)
#XXX  realax.plot(fftOutput.real,"o-",label="Vel: {}, d: {:.3f}".format(maxVel,density))
#XXX  imgax.plot(fftOutput.imag,"o-")
#XXX  realax.grid(b=True, which='major', color='k', linestyle='-')
#XXX  imgax.grid(b=True, which='major', color='k', linestyle='-')
#XXX  realax.legend(loc="upper right",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)


#
##plt.show()
##plt.close()
#####################################
#
#      ###############
#      #Plot Q slices
#      ###############
#      if 1:
#        #xmin=convertW(fitDataX1 - 4*fwhm,fftSamples)
#        #xmax=convertW(fitDataX2 + 4*fwhm,fftSamples)
#        xmin=convertW(areaMinX,fftSamples)
#        xmax=convertW(areaMaxX,fftSamples)
#        #print xmin,xmax
#        fwhmFitX1 = convertW(res.x[1]-res.x[2]/2.0,fftSamples)
#        fwhmFitX2 = convertW(res.x[1]+res.x[2]/2.0,fftSamples)
#        #ymin=0
#        #ymax=10*maxVal#np.nanmax(maxVal,np.nanmax(1.0/fitLineY))
#        fig = plt.figure()
#        zqX = convertW(np.arange(0,zqLen,1),fftSamples)
#        plt.plot(zqX,zq,'-b')
#        plt.plot(zqX,smoothedData,'-k')
#        plt.plot(convertW(xData,fftSamples),yData,'ro')
#        plt.plot(convertW(xFitPoints,fftSamples),yFitPoints,'g*')
#        plt.plot([convertW(lowSideIndex,fftSamples),convertW(highSideIndex,fftSamples)],[halfMax,halfMax],'r-',linewidth=2.0)
#        plt.plot([fwhmFitX1,fwhmFitX2],[max(yFitPoints)/2.0,max(yFitPoints)/2.0],'g-',linewidth=2.0)
#        plt.axvline(xmin,color='c')
#        plt.axvline(xmax,color='c')
#        ymin,ymax=plt.ylim()
#        for x in convertW(peakIndexes,fftSamples):
#          plt.axvline(x,color='k')
#        plt.title( 'Omega distribution for q = {} and density = {:.03f}. Area={:.03f}'.format(q,density,peakArea[q]) )
#        plt.xlabel('Omega')
#        plt.ylabel("Intensity")
#  
#        plt.axis([0, 6.3, 0, ymax])
#        #plt.axis([xmin, xmax, 0, ymax])
#        plt.grid(True)
#        plt.tight_layout()
#  
#        #plt.savefig("/data/Qslices/peak_V{:d}_Q{:04d}_d{:.03f}_phi_hamm.png".format(maxVel,q,density))
#        plt.show()
#        plt.close()
#      
#    if errorFlag == 1:
#      continue
#
#    wPeakRad[iQ] = convertW(peakIndexes[0],fftSamples)
#    qRad[iQ] = convertQ(q,trackLength)
#
#  #####################################################################
#  # Fit line to jam peak 
#  #####################################################################
# 
#  xValsToFit = qRad[~np.isnan(qRad)]
#  yValsToFit = wPeakRad[~np.isnan(wPeakRad)]
# 
#  indx = np.isfinite(xValsToFit) & np.isfinite(yValsToFit)
#  linFitParam,res,_,_,_ = np.polyfit(xValsToFit[indx],yValsToFit[indx],1,full=True)
#  M = linFitParam[0]
#  b = linFitParam[1]
#  #print "res: ",res
#  #print M,b
#  #print xValsToFit[0]
#  xAvg = (xValsToFit[-1]-xValsToFit[0])/2.0
#  #print "xAvg: ",xAvg
#  xSpread = np.sum((xValsToFit-xAvg)**2)**.5
#  #print "xSpread: ",xSpread
#  dM = ((res[0]/(len(xValsToFit)-2))**.5)/xSpread
#  #print "dM: ",dM
#  slopeVals[fileCounter] = M
#  slopeSigmas[fileCounter] = dM
#
# 
#  linFitYvals=np.empty(len(xValsToFit))
#  linFitYvals.fill(np.NAN)
#  
#  j=0
#  for i in xValsToFit:
#    linFitYvals[j] = M*i + b
#    j+=1
#  
  ########################
  ## Save data to file ###
  ########################
  if saveOutput:
    maxQrad = convertQ(maxQ,trackLength)
    if seeking=="FreePeak":
      outputFileName = file+"_peakData.csv"
    if seeking=="JamPeak":
      outputFileName = file+"_JamPeakData.csv"

    outputFile = open(outputFileName,'w')
    outputFile.truncate()
    
    for field in infoArray:
      outputFile.write(field+",")
    outputFile.write("\n")
    outputFile.write("qValsRadians,HWHM,peakArea\n")
    for index,qq in enumerate(qValsRadians):
      outputFile.write("{:.6e},{:.6e},{:.6e}\n".format(qq,HWHMarray[index],areaArray[index]))
    outputFile.close()
    print "Saved data to file:\n"+outputFileName+"\n"

  
  


  #############
  # Plot Things
  #############
  #Use scaling to when plotting data from varing track lengths or time
#  #lengths on the same plot
#  scaleX = .4*np.log(32786/trackLength)
#  scaleY = 1.0*np.log(32786/trackLength)
#  print scaleX,scaleY
  labelText = "density: {:.3f}".format(density)
  ax1.plot(qVals,HWHMarray,color=c,linestyle='',marker=m,ms=3,label=labelText)
  #ax1.plot(qValsRadians,HWHMarrayS,color='k',linestyle='-',marker='',linewidth=2)
  #ax1.plot(qVals,linFitYvals,color=c,linestyle='-',marker='')
#  ax1.plot(fitFWHM/2.0,color=c,linestyle='',marker=m,label=legendString);
##  ax1.plot(np.log(scaleFactor*qDataPI),np.log(convertW(fitFWHM/2.0,fftSamples)),color=c,linestyle='',marker=m,label=legendString)
##  ax1.plot((qDataPI),(),'k',color=c,linestyle='',marker=m,label=legendString)
##  ax1.errorbar(trackLength,M,xerr=0,yerr=.02,color=c,linestyle='',marker=m,label=legendString)
# # plt.plot(gamma,convertW(fitFWHM,fftSamples)/2.0,'g.')
# # plt.plot(gamma,gamma,'b')
#
##  ax1.set_title('Slope of Jam peak in each q slice vs slowdown\n'+infoText)
#  ax1.set_title(infoText)
#
##  ax1.set_ylabel('ln(Area)',fontsize=16)
#  #ymax = np.nanmax(convertW(fitFWHM,fftSamples))/2*1.1
#  ax1.set_xlim([0,maxQ])
##  ax1.set_ylim([0,1])
##  ax1.set_xticklabels([])
  print "Completed file {} with {} failed.\n--------------------\n"\
          .format(fileCounter,failedCount)
##############################
#### END OF LOOP OVER FILES
##############################
#dax.axvline((trackLength/maxVel),color='c',label="(2pi/vmax)")
#ax1.axvline((2*np.pi/maxVel),color='c',label="(2pi/vmax)")
#ax1.axvline((2*np.pi/maxVel),color='c',label="(2pi/vmax)")
ax1.axhline(y=1/16384.0,color='c')
ax1.set_ylabel('HWHM (1 / time step)',fontsize=16)
ax1.set_xlabel('q (integer)',fontsize=16)
ax1.grid(b=True, which='major', color='k', linestyle='-')
ax1.grid(b=True, which='minor', color='r', linestyle='-', alpha=0.2)
ax1.set_ylim(bottom=0)
ax1.set_xlim(left=0)
#ax1.set_xlim([0,np.pi])
#dax.set_xlim(0,trackLength/2.0)
#realax.set_xlim([0,50])
#imgax.set_xlim([0,50])
#dax.set_xlim(left=0)
#XXX ax1.legend(loc="upper left",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)
#dax.legend(loc="upper left",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)
plt.legend(loc="upper left", ncol=2,numpoints=1)
#plt.tight_layout()
plt.show()
plt.close()
#
###  ax2.plot(qRad,wPeakRad,color=c,linestyle='',marker=m,label="Slope: {:.3f} +/- {:.4f}".format(M,dM))
###  ax2.plot(xValsToFit,linFitYvals,color=c,linestyle='-',marker='')
###  ax2.legend(bbox_to_anchor=(0, 1),loc="upper left",ncol=1,shadow=True,fancybox=True,fontsize=13,numpoints=1)
### # plt.plot(gamma,convertW(fitFWHM,fftSamples)/2.0,'g.')
###  ax2.set_xlim([0,maxQrad])
###  ax2.set_ylabel("Peak Location (rad)",fontsize=16)
###
###  #ticks = ax2.xaxis.get_majorticklocs()
###  #tickLabels = ["$\pi$/{:.0f}".format(np.pi/i) for i in ticks]
###  #tickLabels[0] = "0"
###  #ax2.set_xticklabels(tickLabels)
###  ax2.set_xlabel('Wave vector, q (rad / unit distance)',fontsize=16)
###  ax2.grid(b=True, which='major', color='k', linestyle='-')
#
##  peakAreaNoNAN = np.log(peakArea[~np.isnan(peakArea)])
##  qDataPINoNAN = np.log(qDataPI[~np.isnan(peakArea)])
##  slopeOflnArea=np.zeros([qDataPINoNAN.size-1])
##  slopeQ = np.zeros([qDataPINoNAN.size-1])
##  for index,x in enumerate(qDataPINoNAN[0:qDataPINoNAN.size-1]):
##    x1,x2 = x,qDataPINoNAN[index+1]
##    y1,y2 = peakAreaNoNAN[index],peakAreaNoNAN[index+1]
##    slopeOflnArea[index] = (y2-y1)/(x2-x1)
##    slopeQ[index] = (x1+x2)/2.0
#
#  ax2 = plt.subplot(212)
##  ax2.plot(slopeQ,slopeOflnArea,'k',color=c,linestyle='',marker=m,label=legendString)
#  ax2.plot((peakArea),color=c,linestyle='',marker=m,label=legendString)
##  ax2.plot(np.log(qDataPI)-scaleX,np.log(peakArea)+scaleY,color=c,linestyle='',marker=m,label=legendString)
#  ax2.legend(loc="lower right",ncol=2,shadow=True,fancybox=True,fontsize=13,numpoints=1)
#  #ax2.set_xlim([0,np.log(maxQrad)])
#  ax2.set_xlim([0,maxQ])
#  ax2.set_ylim([0,1.1])
#  ax2.set_ylabel("Area",fontsize=16)
#  #ticks = ax2.xaxis.get_majorticklocs()
#  #tickLabels = ["$\pi$/{:.0f}".format(np.pi/i) for i in ticks]
#  #tickLabels[0] = "0"
#  #ax2.set_xticklabels(tickLabels)
#  ax2.set_xlabel('(q)',fontsize=16)
#  ax2.grid(b=True, which='major', color='k', linestyle='-')
# 

#print densityVals
#print slopeVals
#  
#plt.tight_layout()
#plt.subplots_adjust(hspace=.1)#.05)
##plt.savefig("../plots/JamPeakSlope/V{:d}_T{}_temp.svg".format(maxVel,trackLength))
##plt.savefig("../plots/qDependance/V{:d}_T{}_FreePeak_TransDen.pdf".format(maxVel,trackLength))
##plt.savefig("../plots/qDependance/V{:d}_d{:.03f}_JamPeak.png".format(maxVel,density))
#plt.show()
##raw_input()
#
#
