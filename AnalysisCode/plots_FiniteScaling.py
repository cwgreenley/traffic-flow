# Built-in python libraries
import sys
import os
import csv

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import json

markers = itertools.cycle(('o', 'v', 's','*', '^', 'p',  'h', '<', 'H', 'D'))
lineSty = itertools.cycle(([18,4],[4,2],[10,2,3,2],[8,2,3,2,3,2]))
colors = itertools.cycle(('k', 'g', 'b', 'r','brown','c', 'm','y','darksalmon'))

path = "./S0to50.dat"

savePlot = True
saveFileName = "scaling_S0"
xlimits = (None,None)
ylimits = (None,None)

a0 = .16
b0 = .76
c0 = 1.95
nu0 = .11

def main():

  dataDict,trackLengths = importData(path)

  fig,ax = prepare_plot_canvas()
  plot_S0(ax,dataDict,trackLengths)
  formatAxes(ax)
  plt.tight_layout()

  if(savePlot): save_plot(fig)
  plt.show()

###################################################################

def importData(fileName):

  dataList = []

  with open(fileName,'r') as file:
    data = csv.reader(file,delimiter=",")
    for row in data:
      dataList.append(row)


  dataArray = np.array(dataList)
  trackLengths,counts = np.unique(dataArray[:,0],return_counts=True)

  dataDict = {tL: np.full((counts[i],2),np.nan) for i,tL in enumerate(trackLengths)}

  for tL in trackLengths:
    i = 0
    for row in (dataArray):
      if tL in row:
        density = float(row[1])
        Nbar = float(row[2])
        S0 = float(row[3])/(Nbar/float(tL))
        dataDict[str(tL)][i] = (density,S0)
        i += 1
    
  return dataDict,trackLengths


#def prepareData(info,data):
#
#  if q_scaling: 
#    skips = 100/info['maxVel']
#  else: 
#    skips = skipNum
#
#  data = data[0:info['trackLength']/4:skips]
#
#  return data

##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def plot_S0(ax,dataDict,trackLengths):

  makePlots(ax,dataDict,trackLengths)
  formatLabels(ax)
  plt.legend(loc="upper right",numpoints=1,handletextpad=-.1)


def set_style():
#  sns.set()

  singleColWidth_mm = 89.0
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch*1.0

  params = {
    'axes.labelsize': 12,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  #plt.style.use('classic')
  mpl.rcParams.update(params)

def makePlots(ax,dataDict,trackLengths):

  for tL in np.sort(trackLengths):
    m = next(markers)
    cl = next(colors)
    xVals = dataDict[(tL)][:,0]
    yVals = dataDict[(tL)][:,1]
    xVals = xScaling(b0,c0,nu0,xVals,float(tL))*100
    yVals = yScaling(a0,yVals,float(tL))

    ax.plot(xVals,yVals,linestyle='',markersize=5,marker=m,color=cl,label=str(tL))

def xScaling(b,c,nu,xdata,trackLength):
  dc = critDen(9.0,10.0) + c*trackLength**(-b) #dc = d0 + c*L^-b
  return (xdata - dc)*trackLength**nu # (d - dc)L^\nu

def yScaling(a,ydata,trackLength):
  return np.log(ydata*trackLength**(-a)) #L^a

def critDen(vmax,slowp):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp)# + 1.95*trackLen**-.76
  return critDen*.963

def formatAxes(ax):
  # X-axes
  ax.set_xlim(xlimits)
  #ax.set_xticklabels('')
  for tick in ax.get_xticklabels():
    tick.set_rotation(35)

  # Y-axes
  ax.set_ylim(ylimits)
  #ax.set_yticklabels('')

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(ax):
  ax.set_xlabel(r'$(d/d_c-1)L^{\nu}\times 10^2$')
  ax.set_ylabel(r'$\ln(S_\phi(q=0)L^{-a})$')


def save_plot(fig):
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
    #json.dump(info, myfile)
    myfile.write("a,{},b,{},c,{},nu,{}\n".format(a0,b0,c0,nu0))
    myfile.write(path)

#############
if __name__ == "__main__":
  main()


