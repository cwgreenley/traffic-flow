# Built-in python libraries
import sys
import os
import csv
import scipy.signal as sig

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json


dataFiles = [
### 2018-07 ###

"/shared/home/chris/201807_data/25/vel5_density0.11779_fft4096_track8192_slowP10_runs1.50e+03_numCars965_sampleRate1_numDataPoints5.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12060_fft4096_track8192_slowP10_runs1.50e+03_numCars988_sampleRate1_numDataPoints6.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12340_fft4096_track8192_slowP10_runs1.50e+03_numCars1011_sampleRate1_numDataPoints6.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12621_fft4096_track8192_slowP10_runs1.50e+03_numCars1034_sampleRate1_numDataPoints6.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12901_fft4096_track8192_slowP10_runs1.50e+03_numCars1057_sampleRate1_numDataPoints6.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13182_fft4096_track8192_slowP10_runs1.50e+03_numCars1080_sampleRate1_numDataPoints6.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13322_fft4096_track8192_slowP10_runs1.50e+03_numCars1091_sampleRate1_numDataPoints6.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13462_fft4096_track8192_slowP10_runs1.50e+03_numCars1103_sampleRate1_numDataPoints6.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13603_fft4096_track8192_slowP10_runs1.50e+03_numCars1114_sampleRate1_numDataPoints6.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13743_fft4096_track8192_slowP10_runs1.50e+03_numCars1126_sampleRate1_numDataPoints6.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13813_fft4096_track8192_slowP10_runs1.50e+03_numCars1132_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13883_fft4096_track8192_slowP10_runs1.50e+03_numCars1137_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13953_fft4096_track8192_slowP10_runs1.50e+03_numCars1143_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14023_fft4096_track8192_slowP10_runs1.50e+03_numCars1149_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14093_fft4096_track8192_slowP10_runs1.50e+03_numCars1154_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14163_fft4096_track8192_slowP10_runs1.50e+03_numCars1160_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14234_fft4096_track8192_slowP10_runs1.50e+03_numCars1166_sampleRate1_numDataPoints7.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14304_fft4096_track8192_slowP10_runs1.50e+03_numCars1172_sampleRate1_numDataPoints7.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14444_fft4096_track8192_slowP10_runs1.50e+03_numCars1183_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14584_fft4096_track8192_slowP10_runs1.50e+03_numCars1195_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14724_fft4096_track8192_slowP10_runs1.50e+03_numCars1206_sampleRate1_numDataPoints7.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14865_fft4096_track8192_slowP10_runs1.50e+03_numCars1218_sampleRate1_numDataPoints7.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.15145_fft4096_track8192_slowP10_runs1.50e+03_numCars1241_sampleRate1_numDataPoints7.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.15426_fft4096_track8192_slowP10_runs1.50e+03_numCars1264_sampleRate1_numDataPoints7.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07001_fft4096_track8192_slowP10_runs1.50e+03_numCars574_sampleRate1_numDataPoints3.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07168_fft4096_track8192_slowP10_runs1.50e+03_numCars587_sampleRate1_numDataPoints3.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07335_fft4096_track8192_slowP10_runs1.50e+03_numCars601_sampleRate1_numDataPoints3.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07502_fft4096_track8192_slowP10_runs1.50e+03_numCars615_sampleRate1_numDataPoints3.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07668_fft4096_track8192_slowP10_runs1.50e+03_numCars628_sampleRate1_numDataPoints3.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07835_fft4096_track8192_slowP10_runs1.50e+03_numCars642_sampleRate1_numDataPoints3.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07918_fft4096_track8192_slowP10_runs1.50e+03_numCars649_sampleRate1_numDataPoints4.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08002_fft4096_track8192_slowP10_runs1.50e+03_numCars656_sampleRate1_numDataPoints4.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08085_fft4096_track8192_slowP10_runs1.50e+03_numCars662_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08168_fft4096_track8192_slowP10_runs1.50e+03_numCars669_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08210_fft4096_track8192_slowP10_runs1.50e+03_numCars673_sampleRate1_numDataPoints4.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08252_fft4096_track8192_slowP10_runs1.50e+03_numCars676_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08293_fft4096_track8192_slowP10_runs1.50e+03_numCars679_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08335_fft4096_track8192_slowP10_runs1.50e+03_numCars683_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08377_fft4096_track8192_slowP10_runs1.50e+03_numCars686_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08418_fft4096_track8192_slowP10_runs1.50e+03_numCars690_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08460_fft4096_track8192_slowP10_runs1.50e+03_numCars693_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08502_fft4096_track8192_slowP10_runs1.50e+03_numCars696_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08585_fft4096_track8192_slowP10_runs1.50e+03_numCars703_sampleRate1_numDataPoints4.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08669_fft4096_track8192_slowP10_runs1.50e+03_numCars710_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08752_fft4096_track8192_slowP10_runs1.50e+03_numCars717_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08835_fft4096_track8192_slowP10_runs1.50e+03_numCars724_sampleRate1_numDataPoints4.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.09002_fft4096_track8192_slowP10_runs1.50e+03_numCars737_sampleRate1_numDataPoints4.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.09169_fft4096_track8192_slowP10_runs1.50e+03_numCars751_sampleRate1_numDataPoints4.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.11711_fft4096_track16384_slowP10_runs1.50e+03_numCars1919_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.11990_fft4096_track16384_slowP10_runs1.50e+03_numCars1964_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12269_fft4096_track16384_slowP10_runs1.50e+03_numCars2010_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12547_fft4096_track16384_slowP10_runs1.50e+03_numCars2056_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12826_fft4096_track16384_slowP10_runs1.50e+03_numCars2101_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13105_fft4096_track16384_slowP10_runs1.50e+03_numCars2147_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13245_fft4096_track16384_slowP10_runs1.50e+03_numCars2170_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13384_fft4096_track16384_slowP10_runs1.50e+03_numCars2193_sampleRate1_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13523_fft4096_track16384_slowP10_runs1.50e+03_numCars2216_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13663_fft4096_track16384_slowP10_runs1.50e+03_numCars2239_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13732_fft4096_track16384_slowP10_runs1.50e+03_numCars2250_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13802_fft4096_track16384_slowP10_runs1.50e+03_numCars2261_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13872_fft4096_track16384_slowP10_runs1.50e+03_numCars2273_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13942_fft4096_track16384_slowP10_runs1.50e+03_numCars2284_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14011_fft4096_track16384_slowP10_runs1.50e+03_numCars2296_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14081_fft4096_track16384_slowP10_runs1.50e+03_numCars2307_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14151_fft4096_track16384_slowP10_runs1.50e+03_numCars2318_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14220_fft4096_track16384_slowP10_runs1.50e+03_numCars2330_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14360_fft4096_track16384_slowP10_runs1.50e+03_numCars2353_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14499_fft4096_track16384_slowP10_runs1.50e+03_numCars2376_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14639_fft4096_track16384_slowP10_runs1.50e+03_numCars2398_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14778_fft4096_track16384_slowP10_runs1.50e+03_numCars2421_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.15057_fft4096_track16384_slowP10_runs1.50e+03_numCars2467_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.15336_fft4096_track16384_slowP10_runs1.50e+03_numCars2513_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.06933_fft4096_track16384_slowP10_runs1.50e+03_numCars1136_sampleRate1_numDataPoints7.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07098_fft4096_track16384_slowP10_runs1.50e+03_numCars1163_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07263_fft4096_track16384_slowP10_runs1.50e+03_numCars1190_sampleRate1_numDataPoints7.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07428_fft4096_track16384_slowP10_runs1.50e+03_numCars1217_sampleRate1_numDataPoints7.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07593_fft4096_track16384_slowP10_runs1.50e+03_numCars1244_sampleRate1_numDataPoints7.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07758_fft4096_track16384_slowP10_runs1.50e+03_numCars1271_sampleRate1_numDataPoints7.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07841_fft4096_track16384_slowP10_runs1.50e+03_numCars1285_sampleRate1_numDataPoints7.9e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07923_fft4096_track16384_slowP10_runs1.50e+03_numCars1298_sampleRate1_numDataPoints8.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08006_fft4096_track16384_slowP10_runs1.50e+03_numCars1312_sampleRate1_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08088_fft4096_track16384_slowP10_runs1.50e+03_numCars1325_sampleRate1_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08130_fft4096_track16384_slowP10_runs1.50e+03_numCars1332_sampleRate1_numDataPoints8.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08171_fft4096_track16384_slowP10_runs1.50e+03_numCars1339_sampleRate1_numDataPoints8.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08212_fft4096_track16384_slowP10_runs1.50e+03_numCars1345_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08254_fft4096_track16384_slowP10_runs1.50e+03_numCars1352_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08295_fft4096_track16384_slowP10_runs1.50e+03_numCars1359_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08336_fft4096_track16384_slowP10_runs1.50e+03_numCars1366_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08377_fft4096_track16384_slowP10_runs1.50e+03_numCars1372_sampleRate1_numDataPoints8.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08419_fft4096_track16384_slowP10_runs1.50e+03_numCars1379_sampleRate1_numDataPoints8.5e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08501_fft4096_track16384_slowP10_runs1.50e+03_numCars1393_sampleRate1_numDataPoints8.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08584_fft4096_track16384_slowP10_runs1.50e+03_numCars1406_sampleRate1_numDataPoints8.6e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08666_fft4096_track16384_slowP10_runs1.50e+03_numCars1420_sampleRate1_numDataPoints8.7e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08749_fft4096_track16384_slowP10_runs1.50e+03_numCars1433_sampleRate1_numDataPoints8.8e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08914_fft4096_track16384_slowP10_runs1.50e+03_numCars1460_sampleRate1_numDataPoints9.0e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.09079_fft4096_track16384_slowP10_runs1.50e+03_numCars1488_sampleRate1_numDataPoints9.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.11670_fft4096_track32768_slowP10_runs1.50e+03_numCars3824_sampleRate1_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.11948_fft4096_track32768_slowP10_runs1.50e+03_numCars3915_sampleRate1_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12226_fft4096_track32768_slowP10_runs1.50e+03_numCars4006_sampleRate1_numDataPoints2.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12504_fft4096_track32768_slowP10_runs1.50e+03_numCars4097_sampleRate1_numDataPoints2.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.12782_fft4096_track32768_slowP10_runs1.50e+03_numCars4188_sampleRate1_numDataPoints2.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13199_fft4096_track32768_slowP10_runs1.50e+03_numCars4325_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13338_fft4096_track32768_slowP10_runs1.50e+03_numCars4371_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13477_fft4096_track32768_slowP10_runs1.50e+03_numCars4416_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13616_fft4096_track32768_slowP10_runs1.50e+03_numCars4462_sampleRate1_numDataPoints2.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13685_fft4096_track32768_slowP10_runs1.50e+03_numCars4484_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13824_fft4096_track32768_slowP10_runs1.50e+03_numCars4530_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13893_fft4096_track32768_slowP10_runs1.50e+03_numCars4552_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.13963_fft4096_track32768_slowP10_runs1.50e+03_numCars4575_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14032_fft4096_track32768_slowP10_runs1.50e+03_numCars4598_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14102_fft4096_track32768_slowP10_runs1.50e+03_numCars4621_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14310_fft4096_track32768_slowP10_runs1.50e+03_numCars4689_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14449_fft4096_track32768_slowP10_runs1.50e+03_numCars4735_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14588_fft4096_track32768_slowP10_runs1.50e+03_numCars4780_sampleRate1_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.14727_fft4096_track32768_slowP10_runs1.50e+03_numCars4826_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel5_density0.15005_fft4096_track32768_slowP10_runs1.50e+03_numCars4917_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.06892_fft4096_track32768_slowP10_runs1.50e+03_numCars2258_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07057_fft4096_track32768_slowP10_runs1.50e+03_numCars2312_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07221_fft4096_track32768_slowP10_runs1.50e+03_numCars2366_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07385_fft4096_track32768_slowP10_runs1.50e+03_numCars2420_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07549_fft4096_track32768_slowP10_runs1.50e+03_numCars2474_sampleRate1_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07713_fft4096_track32768_slowP10_runs1.50e+03_numCars2527_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07795_fft4096_track32768_slowP10_runs1.50e+03_numCars2554_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07877_fft4096_track32768_slowP10_runs1.50e+03_numCars2581_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.07959_fft4096_track32768_slowP10_runs1.50e+03_numCars2608_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08041_fft4096_track32768_slowP10_runs1.50e+03_numCars2635_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08082_fft4096_track32768_slowP10_runs1.50e+03_numCars2648_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08123_fft4096_track32768_slowP10_runs1.50e+03_numCars2662_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08164_fft4096_track32768_slowP10_runs1.50e+03_numCars2675_sampleRate1_numDataPoints1.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08205_fft4096_track32768_slowP10_runs1.50e+03_numCars2689_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08246_fft4096_track32768_slowP10_runs1.50e+03_numCars2702_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08287_fft4096_track32768_slowP10_runs1.50e+03_numCars2715_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08328_fft4096_track32768_slowP10_runs1.50e+03_numCars2729_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08369_fft4096_track32768_slowP10_runs1.50e+03_numCars2742_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08451_fft4096_track32768_slowP10_runs1.50e+03_numCars2769_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08534_fft4096_track32768_slowP10_runs1.50e+03_numCars2796_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08616_fft4096_track32768_slowP10_runs1.50e+03_numCars2823_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08698_fft4096_track32768_slowP10_runs1.50e+03_numCars2850_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.08862_fft4096_track32768_slowP10_runs1.50e+03_numCars2904_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/25/vel9_density0.09026_fft4096_track32768_slowP10_runs1.50e+03_numCars2958_sampleRate1_numDataPoints1.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
]

#first col is for density, second is for peakVal
dataShape = (len(dataFiles)/6+1,2) 
dataDict = {
  "8192"  : {
    "5" : np.full(dataShape,np.nan),
    "9" : np.full(dataShape,np.nan)},
  "16384" : {
    "5" : np.full(dataShape,np.nan),
    "9" : np.full(dataShape,np.nan)},
  "32768" : {
    "5" : np.full(dataShape,np.nan),
    "9" : np.full(dataShape,np.nan)}
}

markers = itertools.cycle(('o', 'v', 's','*', '^', 'p',  'h', '<', 'H', 'D', '>','d'))
lineSty = itertools.cycle(([18,2],[4,2],[10,2,3,2]))
colors = itertools.cycle(('k', 'r', 'b', 'brown','c', 'm', 'y', 'g','darksalmon'))

savePlot = True

saveFileName = "lifetime_plot_JamDensities_PeakValue"
legend = True
xlimits = (.7,1.2)
ylimits = (0,None)

def main():

  fig,ax = prepare_plot_canvas()
  metaInfo = range(len(dataFiles))

  dataIndex = 0
  priorV = 0
  priorT = 0
  for f,filePath in enumerate(dataFiles):
    info,headers,data = importData(filePath+"_peakData.csv")
    metaInfo[f] = info
    dataIndex += 1
    if priorV != info['maxVel'] or priorT != info['trackLength']:
      priorV = info['maxVel']
      priorT = info['trackLength']
      dataIndex = 0
    prepareData(info,data,dataIndex)

  plot_data(ax)
  formatAxes(ax)

  if(savePlot): save_plot(fig,metaInfo)
  plt.show()

###################################################################

def importData(fileName):

  with open(fileName,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()
    headerArray = data.next()

    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "sampleRate" : int(infoArray[7]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fftPadding"  : int(infoArray[19]),
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])
    dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])
    info['fracCritDen'] = info['density']/dCrit

    maxQ = int(info['trackLength']/2) #max possible Q, may be smaller

    #Prepare numpy array for holding data
    dataShape = [maxQ, 3]
    dataArray=np.empty(dataShape,dtype=float)
    dataArray.fill(np.NAN)

    nRows = 0
    for i,row in enumerate(data):
      dataArray[i] = row
      nRows += 1
     
    info['nRows'] = nRows

  return info,headerArray,dataArray


def prepareData(info,data,f):
  maxQ = int(info['trackLength']/(2.2*info['maxVel']))

  data = data[0:maxQ]
  data[:,1] = sig.medfilt(data[:,1],3)
  #data[:,1] = sig.savgol_filter(data[:,1],5,1,mode='interp')
  #data[:,1] = sig.savgol_filter(data[:,1],5,1,mode='interp')

  peakVal = max(data[:,1])

  trackLenKey = str(info["trackLength"])
  velKey = str(info["maxVel"])
  dataDict[trackLenKey][velKey][f,0] = info["fracCritDen"]
  dataDict[trackLenKey][velKey][f,1] = peakVal

def cleanData(info,data):
  startQ = 60
  ## Make sure the starting value is a valid number
  goodStart = 0
  while(goodStart==0):
    if np.isnan(data[startQ][1]):
      startQ -= 1
    else:
      goodStart = 1

  lastGood = startQ
  for q in range(startQ,0,-1):
    if np.isnan(data[q-1][1]): continue
    thisVal = data[lastGood][1]
    nextVal = data[q-1][1]
    if(nextVal < 0.9*thisVal): ##Too much change indicates bad data
      data[q-1][1] = np.nan
      data[q-1][2] = np.nan
    else:
      lastGood = q-1

  return data

##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def set_style():
#  sns.set()

  singleColWidth_mm = 89.0*1.1
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch#/1.6

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'lines.markersize': 5,
    'lines.linewidth': 1.8,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  plt.style.use('classic')
  mpl.rcParams.update(params)

def plot_data(ax):

  makePlots(ax)
  formatLabels(ax)
  plt.tight_layout()
  if legend: plt.legend(loc="upper left",numpoints=2,ncol=1)

def makePlots(ax):

  #plotQ = int(info['trackLength']/(2*info['maxVel']*1.36))
  velKeys = ["5","9"]
  trackLenKeys = ["8192","32768"]
  for tKey in trackLenKeys:
    c = colors.next()
    l = lineSty.next()
    for vKey in velKeys:
      xData = dataDict[tKey][vKey][:,0]
      yData = dataDict[tKey][vKey][:,1]
      if vKey == "5": 
        ax.plot(xData,yData,dashes=l,marker='',color=c,label=tKey)
      else:
        ax.plot(xData,yData,dashes=l,marker='',color=c)



def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def formatAxes(ax):
  # X-axes
  ax.set_xlim(xlimits)
  # Y-axes
  ax.set_ylim(ylimits)

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(ax):
  ax.set_xlabel("Fraction of Critical density")
  ax.set_ylabel("First lifetime peak")


def save_plot(fig,metaInfo):
  global saveFileName
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
      for f,filePath in enumerate(dataFiles):
        json.dump(metaInfo[f], myfile)
        myfile.write("\n")
        myfile.write(filePath+"\n\n")
  

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))


#############
if __name__ == "__main__":
  main()


