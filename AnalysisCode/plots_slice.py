# Built-in python libraries
import sys
import os
import csv

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json

path = "/shared/home/chris/201807_data/08/vel9_density0.083_fft16384_track8192_slowP10_runs7.01e+03_numCars679_sampleRate1_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv_sliceData.csv"

savePlot = True 
saveFileName = "q_slice_plot"
xlimits = (16340,16510)
ylimits = (0,None)


def main():

  fig,ax = prepare_plot_canvas()

  info,omegaData,xFitPoints,yFitPoints = importData(path)
  plot_slice(info,ax,omegaData,xFitPoints,yFitPoints)
  formatAxes(info,ax)
  plt.tight_layout()

  if(savePlot): save_plot(fig,info)
  plt.show()

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))

###################################################################

def importData(fileName):

  with open(fileName,'rb') as file:
    info = json.loads(file.readline())
    data = csv.reader(file,delimiter=",")
    header1 = (data.next()); print(header1[0])
    omegaData = np.array([float(w) for w in data.next()[:-2]])
    header2 = (data.next()); print(header2[0])
    xFitPoints = np.array([float(x) for x in data.next()[:-2]])
    header3 = (data.next()); print(header3[0])
    yFitPoints = np.array([float(y) for y in data.next()[:-2]])

  info['density'] =  float(info['numCars'])/float(info['trackLength'])
  dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])
  info['fracCritDen'] = info['density']/dCrit

  return info,omegaData,xFitPoints,yFitPoints


def prepareData(info,data):

  data = data[0:info['trackLength']/4:skips]

  return data

##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def plot_slice(info,ax,omegaData,xFitPoints,yFitPoints):

  makePlots(ax,info,omegaData,xFitPoints,yFitPoints)
  formatLabels(info,ax)


def set_style():
#  sns.set()

  singleColWidth_mm = 89.0
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch*1.0

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  plt.style.use('classic')
#  plt.style.use(['seaborn-white', 'seaborn-paper'])
#  sns.set(context='paper',rc=params)
  mpl.rcParams.update(params)
##  matplotlib.rc("font", family="Times New Roman")

def makePlots(ax,info,omegaData,xFitPoints,yFitPoints):

  ax.plot(omegaData,linestyle='',markersize=5,marker='o',color='k')
  ax.plot(xFitPoints,yFitPoints,linewidth=3,linestyle='-',marker='',color='r')


#  legendText = "Vmax: {} dCrtFrac: {:.02} SlowP: {} SampleR: {}"\
#            .format(info['maxVel'],info['density']/dCrit,\
#                    info['slowP'],info['sampleRate'])


def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def formatAxes(info,ax):
  # X-axes
  ax.set_xlim(xlimits)
  ax.set_xticklabels('')

  # Y-axes
  ax.set_ylim(ylimits)
  ax.set_yticklabels('')
  #ax.yaxis.labelpad = -3

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(info,ax):
  ax.set_xlabel(r"Temporal Frequency ($\omega$)")
  ax.set_ylabel("Amplitude")

def format_Q_scaling(value, tick_number):
  # find number of multiples of pi/maxVel 
  frac = (value/(np.pi/maxVel))
  N = int(np.round( frac )) 
  if N == 0:
    return "0"
  elif N==1:
    return r"$\pi/v_{max}$"
  else:
    return r"${0}\pi/v_{{max}}$".format(N)

def format_Q_rad(value, tick_number):
  # find number of multiples of pi/2
  fracOfPi = (value/np.pi)
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"$\pi/8$"
  elif N == 2:
    return r"$\pi/4$"
  elif N == 4:
    return r"$\pi/2$"
  elif N == 8:
    return r"$\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_W(value, tick_number):
  # find number of multiples of pi/2
  fracOfPi = (value/fftSamples)-1
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"${\pi}/{8}$"
    #return r"$\frac{\pi}{8}$"
  elif N == -1:
    return r"$-{\pi}/{8}$"
    #return r"$-\frac{\pi}{8}$"
  elif N == 2:
    return r"${\pi}/{4}$"
    #return r"$\frac{\pi}{4}$"
  elif N == -2:
    return r"$-\pi/4$"
  elif N == 4:
    return r"${\pi}/{2}$"
    #return r"$\frac{\pi}{2}$"
  elif N == -4:
    return r"$-{\pi}/{2}$"
    #return r"$-\frac{\pi}{2}$"
  elif N == 8:
    return r"$\pi$"
  elif N == -8:
    return r"$-\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_Wsum(value, tick_number):
  if value == 0:
    return "0"
  else:
    return "{:.1f}".format(value)


def save_plot(fig,info):
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
    json.dump(info, myfile)
    myfile.write("\n")
    myfile.write(path)


#############
if __name__ == "__main__":
  main()


