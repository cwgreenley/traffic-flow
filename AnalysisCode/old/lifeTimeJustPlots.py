import sys
import csv
from mpl_toolkits.mplot3d import axes3d
import matplotlib
##matplotlib.use('qt4agg')
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import math as mt
import itertools
from itertools import islice, count
from scipy.optimize import least_squares
import peakutils
import scipy.signal as sig
import matplotlib as mpl
label_size = 14
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size

def importData(fileName):

  file=open(fileName,'rb')
  data = csv.reader(file,delimiter=",")
  infoArray = data.next()
  headerArray = data.next()
  trackLength = int(infoArray[5])
  dataArray=np.empty([int(trackLength/2),3]) # Three columns
  dataArray.fill(np.NAN)
  for i,row in enumerate(data):
    dataArray[i]=row
  #i=0
  #while(i<4095):
  #  dataArray[i]=data.next()
  #  for j in xrange(31):
  #    data.next()
  #  i=i+32
  file.close()
  
  return infoArray,headerArray,dataArray


def convertW(Wn,samples):
  return 2*np.pi*(Wn)/(samples)

def convertQ(Wn,trackLength):
  return 2*np.pi*(Wn)/(trackLength)


markers = itertools.cycle(('o', 'v', '8', 's', '^','p', '*', 'h', '<', 'H', 'D', '>','d'))
colors = itertools.cycle(('b', 'g', 'r', 'brown','c', 'm', 'y', 'k','darksalmon'))
#markers.next()
#colors.next()
#################

#Q = int(sys.argv[1]);
dataFiles = [
########################
##### On Server ########
########################
#
####### 2018-06 data #######

## Track length 32768

## Vel 5

#"/shared/home/chris/201806_data/vel5_density0.014_fft8192_track32768_slowP10_runs2.82e+03_numCars458_sampleRate2_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate1_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate8_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track32768_slowP10_runs1.41e+03_numCars458_sampleRate32_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs5.00e+03_numCars917_sampleRate1_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs1.28e+03_numCars917_sampleRate8_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track32768_slowP10_runs1.28e+03_numCars917_sampleRate32_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track32768_slowP10_runs5.00e+03_numCars1835_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",

## Vel 9

#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs5.00e+03_numCars262_sampleRate1_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs2.44e+03_numCars262_sampleRate8_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track32768_slowP10_runs2.44e+03_numCars262_sampleRate32_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs5.00e+03_numCars557_sampleRate1_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs2.26e+03_numCars557_sampleRate8_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track32768_slowP10_runs2.26e+03_numCars557_sampleRate32_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track32768_slowP10_runs5.00e+03_numCars1081_sampleRate1_numDataPoints8.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

## Track length 8192

## Vel 5

###"/shared/home/chris/201806_data/vel5_density0.015_fft16384_track8192_slowP2_runs1.01e+04_numCars122_sampleRate1_numDataPoints2.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/201806_data/vel5_density0.030_fft16384_track8192_slowP2_runs1.00e+04_numCars245_sampleRate1_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate1_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate8_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate16_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate32_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate1_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate8_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate16_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate32_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate8_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate16_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_sampleRate32_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate1_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate8_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate16_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.056_fft16384_track8192_slowP10_runs5.01e+03_numCars458_sampleRate32_numDataPoints3.8e+10_pad2_halfdata_NoNorm_hamming_.csv",

## Vel 9

#"/shared/home/chris/201806_data/vel9_density0.009_fft16384_track8192_slowP2_runs1.01e+04_numCars73_sampleRate1_numDataPoints1.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.018_fft16384_track8192_slowP2_runs1.00e+04_numCars147_sampleRate1_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate1_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate8_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate16_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate32_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate1_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate8_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate16_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate32_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate8_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate16_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.025_fft16384_track8192_slowP10_runs5.02e+03_numCars204_sampleRate32_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate1_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate8_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate16_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.033_fft16384_track8192_slowP10_runs5.01e+03_numCars270_sampleRate32_numDataPoints2.2e+10_pad2_halfdata_NoNorm_hamming_.csv",

####### 2018-05 data #######
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_SampleRate16.csv",
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
####### XMAS data #######
### vel5
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP5_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP20_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP40_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP5_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP20_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP40_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#
#"/shared/home/chris/xmasData/vel5_density0.036_fft16384_track8192_slowP5_runs5.02e+03_numCars294_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.072_fft16384_track8192_slowP5_runs5.01e+03_numCars589_numDataPoints4.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.087_fft16384_track8192_slowP5_runs5.00e+03_numCars712_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.101_fft16384_track8192_slowP5_runs5.00e+03_numCars827_numDataPoints6.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP5_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.130_fft16384_track8192_slowP5_runs5.00e+03_numCars1064_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.145_fft16384_track8192_slowP5_runs5.00e+03_numCars1187_numDataPoints9.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.035_fft16384_track8192_slowP10_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.070_fft16384_track8192_slowP10_runs5.00e+03_numCars573_numDataPoints4.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.084_fft16384_track8192_slowP10_runs5.00e+03_numCars688_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.098_fft16384_track8192_slowP10_runs5.00e+03_numCars802_numDataPoints6.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.112_fft16384_track8192_slowP10_runs5.00e+03_numCars917_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.126_fft16384_track8192_slowP10_runs5.00e+03_numCars1032_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#overlapping peaks "/shared/home/chris/xmasData/vel5_density0.140_fft16384_track8192_slowP10_runs5.00e+03_numCars1146_numDataPoints9.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.032_fft16384_track8192_slowP20_runs5.00e+03_numCars262_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.064_fft16384_track8192_slowP20_runs5.00e+03_numCars524_numDataPoints4.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.077_fft16384_track8192_slowP20_runs5.01e+03_numCars630_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.090_fft16384_track8192_slowP20_runs5.00e+03_numCars737_numDataPoints6.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel5_density0.103_fft16384_track8192_slowP20_runs5.00e+03_numCars843_numDataPoints6.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#issues "/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP20_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.129_fft16384_track8192_slowP20_runs5.00e+03_numCars1056_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#many nans"/shared/home/chris/xmasData/vel5_density0.026_fft16384_track8192_slowP40_runs5.02e+03_numCars212_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#many nans"/shared/home/chris/xmasData/vel5_density0.052_fft16384_track8192_slowP40_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#many nans"/shared/home/chris/xmasData/vel5_density0.062_fft16384_track8192_slowP40_runs5.01e+03_numCars507_numDataPoints4.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.073_fft16384_track8192_slowP40_runs5.00e+03_numCars598_numDataPoints4.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.083_fft16384_track8192_slowP40_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.093_fft16384_track8192_slowP40_runs5.00e+03_numCars761_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.104_fft16384_track8192_slowP40_runs5.00e+03_numCars851_numDataPoints7.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
### vel9
###"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP5_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP10_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#unrepairable"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP20_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#unrepairable"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP40_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#
###"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP5_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP10_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
### mostly repaired "/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP20_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#unrepairable "/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP40_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
##
###"/shared/home/chris/xmasData/vel9_density0.022_fft16384_track8192_slowP5_runs5.01e+03_numCars180_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.043_fft16384_track8192_slowP5_runs5.00e+03_numCars352_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.052_fft16384_track8192_slowP5_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP5_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.069_fft16384_track8192_slowP5_runs5.00e+03_numCars565_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.078_fft16384_track8192_slowP5_runs5.01e+03_numCars638_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.086_fft16384_track8192_slowP5_runs5.00e+03_numCars704_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not repairable"/shared/home/chris/trafData/vel9_density0.010_fft16384_track8192_slowP10_runs5.27e+02_numCars81_numDataPoints7.0e+08_halfdata_NoNorm_hamming_padding2_dec13.csv",
###"/shared/home/chris/xmasData/vel9_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP10_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.050_fft16384_track8192_slowP10_runs5.01e+03_numCars409_numDataPoints3.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.058_fft16384_track8192_slowP10_runs5.00e+03_numCars475_numDataPoints3.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.066_fft16384_track8192_slowP10_runs5.01e+03_numCars540_numDataPoints4.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.083_fft16384_track8192_slowP10_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.019_fft16384_track8192_slowP20_runs5.02e+03_numCars155_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.045_fft16384_track8192_slowP20_runs5.01e+03_numCars368_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP20_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP20_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
###"/shared/home/chris/xmasData/vel9_density0.068_fft16384_track8192_slowP20_runs5.00e+03_numCars557_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
##?"/shared/home/chris/xmasData/vel9_density0.075_fft16384_track8192_slowP20_runs5.00e+03_numCars614_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#done w/ issues"/shared/home/chris/xmasData/vel9_density0.015_fft16384_track8192_slowP40_runs5.04e+03_numCars122_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#done w/ issues"/shared/home/chris/xmasData/vel9_density0.029_fft16384_track8192_slowP40_runs5.01e+03_numCars237_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#done w/ issues"/shared/home/chris/xmasData/vel9_density0.035_fft16384_track8192_slowP40_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#done w/ issues"/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP40_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#done w/ issues"/shared/home/chris/xmasData/vel9_density0.047_fft16384_track8192_slowP40_runs5.00e+03_numCars385_numDataPoints3.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP40_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#not done"/shared/home/chris/xmasData/vel9_density0.059_fft16384_track8192_slowP40_runs5.00e+03_numCars483_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#####################
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding4.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding8.csv",
### 
#"/shared/home/chris/trafData/vel5_density0.120_fft1024_track8192_slowP10_runs4.97e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft16384_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints1.0e+11_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft32768_track8192_slowP10_runs1.55e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.135_fft4096_track8192_slowP10_runs1.10e+04_numCars1105_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.138_fft4096_track8192_slowP10_runs1.08e+04_numCars1130_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.140_fft4096_track8192_slowP10_runs1.07e+04_numCars1146_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.140_fft32768_track8192_slowP10_runs6.65e+02_numCars1146_numDataPoints2.5e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.145_fft4096_track8192_slowP10_runs1.03e+04_numCars1187_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.150_fft4096_track8192_slowP10_runs9.94e+03_numCars1228_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.116_fft4096_track8192_slowP10_runs2.06e+04_numCars950_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.107_fft4096_track8192_slowP20_runs2.23e+04_numCars876_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.087_fft4096_track8192_slowP40_runs2.74e+04_numCars712_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.075_fft4096_track8192_slowP50_runs3.18e+04_numCars614_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft2048_track8192_slowP50_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.060_fft2048_track8192_slowP50_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.080_fft2048_track8192_slowP50_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.100_fft2048_track8192_slowP50_runs2.98e+04_numCars819_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
### short tests
#"/shared/home/chris/trafData/vel5_density0.025_fft16384_track8192_slowP10_runs4.48e+02_numCars204_numDataPoints1.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.050_fft16384_track8192_slowP10_runs3.43e+02_numCars409_numDataPoints2.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.060_fft16384_track8192_slowP10_runs4.97e+02_numCars491_numDataPoints4.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.070_fft16384_track8192_slowP10_runs4.79e+02_numCars573_numDataPoints4.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.080_fft16384_track8192_slowP10_runs4.65e+02_numCars655_numDataPoints5.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.090_fft16384_track8192_slowP10_runs4.55e+02_numCars737_numDataPoints5.5e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.100_fft16384_track8192_slowP10_runs3.20e+02_numCars819_numDataPoints4.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.110_fft16384_track8192_slowP10_runs4.06e+02_numCars901_numDataPoints6.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#
#"/shared/home/chris/trafData/vel9_density0.020_fft16384_track8192_slowP10_runs4.86e+02_numCars163_numDataPoints1.3e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.030_fft16384_track8192_slowP10_runs4.98e+02_numCars245_numDataPoints2.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft16384_track8192_slowP10_runs5.03e+02_numCars327_numDataPoints2.7e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.050_fft16384_track8192_slowP10_runs4.92e+02_numCars409_numDataPoints3.3e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.060_fft16384_track8192_slowP10_runs4.97e+02_numCars491_numDataPoints4.0e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#"/shared/home/chris/trafData/vel9_density0.070_fft16384_track8192_slowP10_runs5.00e+02_numCars573_numDataPoints4.7e+09_halfdata_NoNorm_hamming_padding2_dec13.csv",
#
#"/shared/home/chris/trafData/vel5_density0.080_fft16384_track8192_slowP5_runs9.31e+02_numCars655_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.077_fft16384_track8192_slowP10_runs1.01e+03_numCars630_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.070_fft16384_track8192_slowP20_runs1.02e+03_numCars573_numDataPoints9.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.057_fft16384_track8192_slowP40_runs1.01e+03_numCars466_numDataPoints7.7e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.050_fft16384_track8192_slowP50_runs3.50e+02_numCars409_numDataPoints2.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#
#"/shared/home/chris/trafData/vel9_density0.050_fft16384_track8192_slowP5_runs9.55e+02_numCars409_numDataPoints6.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.045_fft16384_track8192_slowP10_runs1.01e+03_numCars368_numDataPoints6.1e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft16384_track8192_slowP20_runs1.04e+03_numCars327_numDataPoints5.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.033_fft16384_track8192_slowP40_runs9.94e+02_numCars270_numDataPoints4.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.027_fft16384_track8192_slowP50_runs3.59e+02_numCars221_numDataPoints1.3e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
###
#"/shared/home/chris/trafData/vel15_density0.028_fft2048_track8192_slowP10_runs4.26e+04_numCars229_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel15_density0.043_fft8192_track8192_slowP10_runs1.73e+04_numCars352_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel22_density0.019_fft2048_track8192_slowP10_runs6.30e+04_numCars155_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
## OLD
#"/shared/home/chris/trafData/downloaded/vel5_density0.140_fft2048_track8192_runs2.13e+04_numCars1146_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/shared/home/chris/trafData/downloaded/vel9_density0.06_fft2048_track8192_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/shared/home/chris/trafData/downloaded/vel9_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#########################
####### New files #######
#########################
#"/data/trafficFlowData/20171138_newdata/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#####################
####### Vel 5 #######
#####################
#"/data/trafficFlowData/vel5_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.04_fft2048_track8192_runs1.49e+03_numCars327_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.06_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.08_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.09_fft2048_track8192_runs3.31e+04_numCars737_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.10_fft2048_track8192_runs2.98e+03_numCars819_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.11_fft2048_track8192_runs2.71e+04_numCars901_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.12_fft2048_track8192_runs4.96e+02_numCars983_numDataPoints1.0e+09_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.13_fft2048_track8192_runs2.29e+04_numCars1064_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.135_fft2048_track8192_runs4.42e+03_numCars1105_numDataPoints1.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.138_fft2048_track8192_runs4.32e+03_numCars1130_numDataPoints1.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.140_fft2048_track8192_runs2.13e+04_numCars1146_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.145_fft2048_track8192_runs2.06e+04_numCars1187_numDataPoints5.0e+10_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.150_fft2048_track8192_runs1.99e+04_numCars1228_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.20_fft2048_track8192_runs5.96e+02_numCars1638_numDataPoints2.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.30_fft2048_track8192_runs9.93e+02_numCars2457_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.40_fft2048_track8192_runs7.45e+02_numCars3276_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.50_fft2048_track8192_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.60_fft2048_track8192_runs4.96e+02_numCars4915_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.70_fft2048_track8192_runs4.25e+02_numCars5734_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.80_fft2048_track8192_runs3.72e+02_numCars6553_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/old/vel5_density0.05_fft2048_track8192_runs1e+04_numCars409_halfdata_Welch.csv",
#####################
####### Vel 9 #######
#####################
#"/data/trafficFlowData/vel9_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.02_fft2048_track8192_runs1.50e+05_numCars163_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.04_fft2048_track8192_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.06_fft2048_track8192_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.08_fft2048_track8192_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.082_fft2048_track8192_runs7.28e+03_numCars671_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.083_fft2048_track8192_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.0835_fft2048_track8192_runs7.14e+03_numCars684_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.084_fft2048_track8192_runs7.10e+03_numCars688_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.085_fft2048_track8192_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.087_fft2048_track8192_runs6.86e+03_numCars712_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.088_fft2048_track8192_runs6.78e+03_numCars720_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.089_fft2048_track8192_runs6.70e+03_numCars729_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.09_fft2048_track8192_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.095_fft2048_track8192_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.10_fft2048_track8192_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.15_fft2048_track8192_runs3.98e+03_numCars1228_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.20_fft2048_track8192_runs2.98e+03_numCars1638_numDataPoints1.0e+10_halfdata_Hamm.csv",
##"/data/trafficFlowData/vel9_density0.30_fft2048_track8192_runs1.99e+03_numCars2457_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.50_fft2048_track8192_runs1.19e+03_numCars4096_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
####### Vel 12 ######
#####################
#"/data/trafficFlowData/vel12_density0.020_fft2048_track8192_runs8.99e+03_numCars163_numDataPoints3.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.030_fft2048_track8192_runs1.99e+04_numCars245_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.040_fft2048_track8192_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.045_fft2048_track8192_runs1.06e+04_numCars368_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.050_fft2048_track8192_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.060_fft2048_track8192_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.070_fft2048_track8192_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.080_fft2048_track8192_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.090_fft2048_track8192_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.100_fft2048_track8192_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.450_fft2048_track8192_runs1.06e+03_numCars3686_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.550_fft2048_track8192_runs1.08e+03_numCars4505_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.650_fft2048_track8192_runs9.17e+02_numCars5324_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel12_density0.750_fft2048_track8192_runs7.94e+02_numCars6144_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
##### Track 4096 ####
#####################
#"/data/trafficFlowData/track4096/vel5_density0.070_fft2048_track4096_runs1.71e+04_numCars286_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.130_fft2048_track4096_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.138_fft2048_track4096_runs8.64e+03_numCars565_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.145_fft2048_track4096_runs8.23e+03_numCars593_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.160_fft2048_track4096_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel5_density0.200_fft2048_track4096_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
##
#"/data/trafficFlowData/track4096/vel9_density0.040_fft2048_track4096_runs1.50e+04_numCars163_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.060_fft2048_track4096_runs9.96e+03_numCars245_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.080_fft2048_track4096_runs1.19e+04_numCars327_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.090_fft2048_track4096_runs1.06e+04_numCars368_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.100_fft2048_track4096_runs9.55e+03_numCars409_numDataPoints8.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/track4096/vel9_density0.150_fft2048_track4096_runs6.36e+03_numCars614_numDataPoints8.0e+09_halfdata_Hamm.csv",
######################
##### Track 16384 ####
######################
#"/data/trafficFlowData/track16384/vel5_density0.130_fft2048_track16384_runs2.29e+03_numCars2129_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel5_density0.138_fft2048_track16384_runs2.16e+03_numCars2260_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel5_density0.145_fft2048_track16384_runs2.06e+03_numCars2375_numDataPoints1.0e+10_halfdata_Hamm.csv",
##
#"/data/trafficFlowData/track16384/vel9_density0.060_fft2048_track16384_runs4.97e+03_numCars983_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.070_fft2048_track16384_runs4.26e+03_numCars1146_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.075_fft2048_track16384_runs3.98e+03_numCars1228_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.080_fft2048_track16384_runs3.73e+03_numCars1310_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.083_fft2048_track16384_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.085_fft2048_track16384_runs3.51e+03_numCars1392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.090_fft2048_track16384_runs3.31e+03_numCars1474_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.095_fft2048_track16384_runs3.14e+03_numCars1556_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/track16384/vel9_density0.100_fft2048_track16384_runs2.98e+03_numCars1638_numDataPoints1.0e+10_halfdata_Hamm.csv",
################################
#### VARYING TRACK LENGTH ######
###############################
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track2048_runs3.00e+04_numCars163_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track4096_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track8192_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track16384_runs3.73e+03_numCars1310_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.080_fft2048_track32768_runs1.86e+03_numCars2621_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track2048_runs2.89e+04_numCars169_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track4096_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track8192_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track16384_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.083_fft2048_track32786_runs1.79e+03_numCars2721_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track2048_runs2.84e+04_numCars172_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track4096_runs1.42e+04_numCars344_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track8192_runs7.10e+03_numCars688_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track16384_runs3.55e+03_numCars1376_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.084_fft2048_track32768_runs1.77e+03_numCars2752_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track2048_runs2.81e+04_numCars174_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track4096_runs1.40e+04_numCars348_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track8192_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track16384_runs3.51e+03_numCars1392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.085_fft2048_track32768_runs1.75e+03_numCars2785_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track2048_runs2.74e+04_numCars178_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track4096_runs1.37e+04_numCars356_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track8192_runs6.86e+03_numCars712_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track16384_runs3.43e+03_numCars1425_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel9_density0.087_fft2048_track32768_runs1.71e+03_numCars2850_numDataPoints1.0e+10_halfdata_Hamm.csv",
#
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track2048_runs1.84e+04_numCars266_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track4096_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track8192_runs4.59e+03_numCars1064_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track16384_runs2.29e+03_numCars2129_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.130_fft2048_track32768_runs1.15e+03_numCars4259_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track2048_runs1.78e+04_numCars274_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track4096_runs8.91e+03_numCars548_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track8192_runs4.45e+03_numCars1097_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track16384_runs2.22e+03_numCars2195_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.134_fft2048_track32786_runs1.11e+03_numCars4393_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track2048_runs1.73e+04_numCars282_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track4096_runs8.64e+03_numCars565_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track8192_runs4.32e+03_numCars1130_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track16384_runs2.16e+03_numCars2260_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.138_fft2048_track32768_runs1.08e+03_numCars4521_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track2048_runs1.68e+04_numCars290_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track4096_runs8.40e+03_numCars581_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track8192_runs4.20e+03_numCars1163_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track16384_runs2.10e+03_numCars2326_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.142_fft2048_track32768_runs1.05e+03_numCars4653_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track2048_runs1.63e+04_numCars299_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track4096_runs8.16e+03_numCars598_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track8192_runs4.08e+03_numCars1196_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track16384_runs2.04e+03_numCars2392_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/varTrack/vel5_density0.146_fft2048_track32768_runs1.02e+03_numCars4784_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
## Phi Vel 9 and 5 ##
#####################
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP10_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP50_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP10_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP50_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP10_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP50_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP10_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP50_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP5_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP10_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP15_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP25_runs3.31e+03_numCars737_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP30_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.200_fft2048_track8192_slowP40_runs2.98e+02_numCars1638_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP50_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.050_fft2048_track8192_slowP75_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP10_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP50_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
##########################
### P50 Vel 9 and 5 ###
##########################
#"/data/trafficFlowData/highP/vel9_density0.010_fft2048_track8192_runs6.03e+03_numCars81_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.020_fft2048_track8192_runs3.00e+03_numCars163_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.030_fft2048_track8192_runs1.99e+03_numCars245_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.050_fft2048_track8192_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.060_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.070_fft2048_track8192_runs8.52e+02_numCars573_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.075_fft2048_track8192_runs7.95e+02_numCars614_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.080_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.085_fft2048_track8192_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm.csv",
##########################
# Various slow P for V=9 #
##########################
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP5_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP5_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.095_fft2048_track8192_slowP5_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.100_fft2048_track8192_slowP5_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.110_fft2048_track8192_slowP5_runs5.42e+03_numCars901_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP10_runs3.31e+03_numCars737_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP10_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP10_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP20_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP20_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP20_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.080_fft2048_track8192_slowP20_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP20_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP25_runs4.26e+03_numCars573_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP25_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP25_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP30_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP30_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP30_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP30_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP30_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP40_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP40_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP40_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP40_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP40_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP40_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.040_fft2048_track8192_slowP50_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP50_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP50_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP50_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP50_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP50_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#
#"/data/trafficFlowData/phifft/vel9_density0.050_fft2048_track8192_slowP75_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#######################################################
### Finite size effects due to track size difference? #
#######################################################
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_phi_finiteSizeEffects_test.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+05_numCars339_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+04_numCars679_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+04_numCars1359_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track32768_slowP10_runs1.80e+04_numCars2719_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
#"/data/trafficFlowData/finiteSizeSlowP/vel9_density0.083_fft2048_track65536_slowP10_runs8.98e+03_numCars5439_numDataPoints1.0e+11_halfdata_Hamm_phi_finiteSizeEffects.csv",
]
HWplt = plt.figure(figsize=(10,6))
ax1 = HWplt.add_subplot(2,1,1)
#dfig = plt.figure()
dax = HWplt.add_subplot(2,1,2)
fftFig = plt.figure()
realax = fftFig.add_subplot(2,1,1)
imgax = fftFig.add_subplot(2,1,2)
#oscMagFigs = plt.figure(figsize=(10,6));
#oscAx1 = oscMagFigs.add_subplot(2,3,1);
#oscAx2 = oscMagFigs.add_subplot(2,3,2);
#oscAx3 = oscMagFigs.add_subplot(2,3,3);
#oscAx4 = oscMagFigs.add_subplot(2,3,4);
#oscAx5 = oscMagFigs.add_subplot(2,3,5);
#oscAx6 = oscMagFigs.add_subplot(2,3,6);
#oscAx1.grid(b=True, which='major', color='k', linestyle='-')
#oscAx2.grid(b=True, which='major', color='k', linestyle='-')
#oscAx3.grid(b=True, which='major', color='k', linestyle='-')
#oscAx4.grid(b=True, which='major', color='k', linestyle='-')
#oscAx5.grid(b=True, which='major', color='k', linestyle='-')
#oscAx6.grid(b=True, which='major', color='k', linestyle='-')
#oscAx1.set_xlabel("Density")
#oscAx2.set_xlabel("Density")
#oscAx3.set_xlabel("Density")
#oscAx4.set_xlabel("Density")
#oscAx5.set_xlabel("Density")
#oscAx6.set_xlabel("Density")
#oscAx2.set_title("Black: 5%, Blue: 10%, Red: 20%")

#ax1 = plt.subplot2grid((2,4),(0,0),colspan=4)
#ax2 = plt.subplot2grid((2,4),(1,0),colspan=4)


slopeVals = np.zeros(len(dataFiles))
slopeSigmas = np.zeros(len(dataFiles))
densityVals = np.zeros(len(dataFiles))
slowPVals = np.zeros(len(dataFiles))

verbose=False #Controls printing of debug statements

z=0
for fileCounter,file in enumerate(dataFiles):

  info,headers,data=importData(file+"_peakData.csv")
##  peakAreaNoNAN = np.log(peakArea[~np.isnan(peakArea)])


  
  trackLength = int(info[5])
  fftSamples=int(info[11])
  fftRuns = int(info[13])
  numCars=int(info[1])
  maxVel=int(info[3])
  slowP = int(info[[i for i, x in enumerate(info) if x == 'slowP'][0] + 1])
  slowPVals[fileCounter]=slowP
  density = float(numCars)/float(trackLength)
  densityVals[fileCounter]=density
  densityString = "Density: {:.4f}".format(density)
  legendString = densityString
  #legendString = densityString+"Slow P: {:d}%".format(slowP)
  #legendString = "Track Length: {}".format(trackLength)
  trackString = "Track: {}".format(trackLength)
  numDataPoints = int(info[15])
  infoText=""
  #infoText=infoText+"Num of time steps in each FFT: {}, ".format(fftSamples)
  #infoText=infoText+"Num of FFTs: {}\n".format(fftRuns)
  #infoText=infoText+"Number of cars: {}, ".format(numCars)
  infoText=infoText+densityString
#  print info
#  print "Track: {}\n".format(trackLength)

  if(slowP==5): c2 = 'k'
  elif(slowP==10): c2 = 'b'
  else: c2='r'
  

  qValsRadians = data[:,0]
  qVals = np.arange(1,len(qValsRadians)+1)
  HWHMarray = data[:,1]
  areaArray = data[:,2]
  ## Repair failed fits
  nanQs = qVals[np.isnan(HWHMarray)]
  print "nanQs: ",nanQs
  for nQ in nanQs:
    if nQ<len(HWHMarray-1):
      nQloc=nQ-1
      HWHMarray[nQloc] = (HWHMarray[nQloc-1]+HWHMarray[nQloc+1])/2.0
  ## Repair bad bits
  for i,hw in enumerate(HWHMarray):
    if i==0 or i>=(len(HWHMarray)-3):
      continue
    else:
      avg1 = (HWHMarray[i-1]+HWHMarray[i+1])/2.0
      avg2 = (HWHMarray[i]+HWHMarray[i+2])/2.0
      if hw<avg1*0.9 or hw>avg1*1.1:
        prior=HWHMarray[i+1]
        HWHMarray[i+1]=avg2
        print "Replaced {} with {} for q={}".format(prior,avg2,i+2)

  ### XXX
#  for i,val in enumerate(HWHMarray):
#    HWHMarray[i] /= mt.sin(2*mt.pi*(i+1)/trackLength)


  HWHMarrayS = sig.savgol_filter(HWHMarray,7,1,mode='interp')
  HWHMarrayS = sig.savgol_filter(HWHMarray,13,1,mode='interp')
  for x in range(4):
    HWHMarrayS = sig.savgol_filter(HWHMarrayS,23,1,mode='interp')


#  ###############################
#  #### Linear fit to low q region
#  ###############################
#  linFitParam,covariance = np.polyfit(qValsRadians[40:100],HWHMarray[40:100],1,cov=True)
#  M = linFitParam[0]
#  b = linFitParam[1]
#  linFitXdata = qVals
#  dM, db = np.sqrt(np.diagonal(covariance))
#  print "Slope: {:.2e} +/- {:.1e}, y-int: {:.2e} +/- {:.1e}".format(M,dM,b,db)
#  linFitYvals = [M*x+b for x in qValsRadians]

  ###############################
  #### derivative
  ###############################
  gradArray = np.gradient(HWHMarrayS,qValsRadians[2]-qValsRadians[1])

  m = markers.next()
  c = colors.next()
  dax.grid(b=True, which='major', color='k', linestyle='-')
  dax.plot(qVals,gradArray,"-",linewidth=3,color=c)
#  oscAx1.plot(density,max(gradArray),"o",color=c2)
#  oscAx1.set_ylabel("Max derivative")
#  oscAx1.set_xlim([0,.8])
# # oscAx1.set_ylim(bottom=0)

  
  ###############################
  #### fourier transform
  ###############################
  fullHWHMarray = np.hstack(([0],HWHMarray,np.flipud(HWHMarray)[1:]))
  fullHWHMarray = np.hstack((fullHWHMarray,-1*fullHWHMarray))
  print "fullHWHMarray size: ",len(fullHWHMarray)
  fftOutput=np.fft.rfft(fullHWHMarray)
  realax.plot(fftOutput.real,"o-",color=c,label="Vel: {}, d: {:.3f}".format(maxVel,density))
  imgax.plot(fftOutput.imag,"o-",color=c)
  realax.grid(b=True, which='major', color='k', linestyle='-')
  imgax.grid(b=True, which='major', color='k', linestyle='-')
  realax.legend(loc="upper right",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)
#  print "realOutput[10]: ",fftOutput.real[10]

#  oscAx2.plot(density,fftOutput.real[0],"o",color=c2)
#  oscAx2.set_ylabel("fft real[0]")
#  oscAx2.set_xlim([0,.8])
#  oscAx2.set_ylim([0,2500])
#
#  oscAx3.plot(density,abs(fftOutput.real[1]),"o",color=c2)
#  oscAx3.set_ylabel("fft abs(real[1])")
#  oscAx3.set_xlim([0,.8])
#
#  oscAx5.plot(density,fftOutput.real[maxVel+1],"o",color=c2)
#  oscAx5.set_ylabel("fft real[{}]".format(maxVel+1))
#  oscAx5.set_xlim([0,.8])
#
#  oscAx6.plot(density,abs(fftOutput.real[maxVel+2]),"o",color=c2)
#  oscAx6.set_ylabel("fft abs(real[{}])".format(maxVel+2))
#  oscAx6.set_xlim([0,.8])

  #############
  # Plot Things
  #############
#  #Use scaling to when plotting data from varing track lengths or time
#  #lengths on the same plot
#  scaleX = .4*np.log(32786/trackLength)
#  scaleY = 1.0*np.log(32786/trackLength)
#  print scaleX,scaleY
  ax1.plot(HWHMarray,color=c,linestyle='',marker=m,ms=3,label="maxV: {} lowP: {} density: {:.3f}".format(maxVel,slowP,density))
  ax1.plot(qVals,HWHMarrayS,color='k',linestyle='-',marker='',linewidth=2)

#  oscAx4.plot(density,max(HWHMarrayS),"o",color=c2)
#  oscAx4.set_ylabel("Max width")
#  oscAx4.set_xlim([0,.8])

##  ax1.errorbar(trackLength,M,xerr=0,yerr=.02,color=c,linestyle='',marker=m,label=legendString)
##  ax1.set_title('Slope of Jam peak in each q slice vs slowdown\n'+infoText)
#  ax1.set_title(infoText)
#
##  ax1.set_ylabel('ln(Area)',fontsize=16)
#  #ymax = np.nanmax(convertW(fitFWHM,fftSamples))/2*1.1
#  ax1.set_xlim([0,maxQ])
##  ax1.set_ylim([0,1])
##  ax1.set_xticklabels([])
  print "Completed file {}.".format(fileCounter)
##############################
#### END OF LOOP OVER FILES
##############################
dax.axvline((trackLength/maxVel),color='c',label="(2pi/{})".format(maxVel))
#ax1.axvline((trackLength/maxVel),color='c',label="(2pi/({}+1))".format(maxVel))
#ax1.axvline((trackLength/(maxVel+1)),color='c',label="(2pi/{})".format(maxVel))
ax1.axhline(y=6e-5,color='k')
ax1.axhline(y=3.14/16384.0,color='k')
dax.axvline((trackLength/(maxVel+1)),color='c',label="(2pi/{})".format(maxVel))
ax1.set_ylabel('HWHM (1 / time step)',fontsize=16)
ax1.set_xlabel('q (radians)',fontsize=16)
ax1.grid(b=True, which='major', color='k', linestyle='-')
ax1.grid(b=True, which='minor', color='r', linestyle='-', alpha=0.2)
#XXX ax1.set_ylim(bottom=0)
ax1.set_xlim(left=0)
dax.set_xlim(left=0)
realax.set_xlim([0,50])
imgax.set_xlim([0,50])
realax.set_ylabel("real part")
imgax.set_ylabel("imaginary part")
#dax.set_xlim(left=0)
ax1.legend(loc="upper left",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)
#dax.legend(loc="upper left",ncol=1,shadow=True,fancybox=True,fontsize=10,numpoints=1)
plt.tight_layout()
plt.show()
plt.close()
###  #ticks = ax2.xaxis.get_majorticklocs()
###  #tickLabels = ["$\pi$/{:.0f}".format(np.pi/i) for i in ticks]
###  #tickLabels[0] = "0"
###  #ax2.set_xticklabels(tickLabels)
###  ax2.set_xlabel('Wave vector, q (rad / unit distance)',fontsize=16)
###  ax2.grid(b=True, which='major', color='k', linestyle='-')
#
##  peakAreaNoNAN = np.log(peakArea[~np.isnan(peakArea)])
##  qDataPINoNAN = np.log(qDataPI[~np.isnan(peakArea)])
##  slopeOflnArea=np.zeros([qDataPINoNAN.size-1])
##  slopeQ = np.zeros([qDataPINoNAN.size-1])
##  for index,x in enumerate(qDataPINoNAN[0:qDataPINoNAN.size-1]):
##    x1,x2 = x,qDataPINoNAN[index+1]
##    y1,y2 = peakAreaNoNAN[index],peakAreaNoNAN[index+1]
##    slopeOflnArea[index] = (y2-y1)/(x2-x1)
##    slopeQ[index] = (x1+x2)/2.0
#
#  ax2 = plt.subplot(212)
##  ax2.plot(slopeQ,slopeOflnArea,'k',color=c,linestyle='',marker=m,label=legendString)
#  ax2.plot((peakArea),color=c,linestyle='',marker=m,label=legendString)
##  ax2.plot(np.log(qDataPI)-scaleX,np.log(peakArea)+scaleY,color=c,linestyle='',marker=m,label=legendString)
#  ax2.legend(loc="lower right",ncol=2,shadow=True,fancybox=True,fontsize=13,numpoints=1)
#  #ax2.set_xlim([0,np.log(maxQrad)])
#  ax2.set_xlim([0,maxQ])
#  ax2.set_ylim([0,1.1])
#  ax2.set_ylabel("Area",fontsize=16)
#  #ticks = ax2.xaxis.get_majorticklocs()
#  #tickLabels = ["$\pi$/{:.0f}".format(np.pi/i) for i in ticks]
#  #tickLabels[0] = "0"
#  #ax2.set_xticklabels(tickLabels)
#  ax2.set_xlabel('(q)',fontsize=16)
#  ax2.grid(b=True, which='major', color='k', linestyle='-')
# 

#print densityVals
#print slopeVals
#  
#plt.tight_layout()
#plt.subplots_adjust(hspace=.1)#.05)
#plt.savefig("./plots/.svg".format(maxVel,trackLength))
#plt.savefig("./plots/qDependance/V{:d}_T{}_FreePeak_TransDen.pdf".format(maxVel,trackLength))
#plt.savefig("./plots/qDependance/V{:d}_d{:.03f}_JamPeak.png".format(maxVel,density))
#plt.show()
##raw_input()
#
#
