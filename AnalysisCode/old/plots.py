import sys
import csv
from mpl_toolkits.mplot3d import axes3d
import matplotlib
#matplotlib.use("Agg")
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import math as m
#from scipy.optimize import least_squares

def importData(fileName):

  dataArray=[]
  file=open(fileName,'rb')
  data = csv.reader(file,delimiter=",")
  infoArray =data.next()
  maxVel=int(infoArray[3])
  trackLength = int(infoArray[5])
  maxQ = int(trackLength/2)/(1)
  for row in data:
    dataArray.append(row[1:maxQ]) #[1:] skips the first entry on each row
  file.close()
  
  return infoArray,dataArray,maxQ

#Pass in a parameter array 
#p[0] = height of the peak (ish...)
#p[1] = center of peak
#p[2] = FWHM
def lorentz(p,x):
  return p[0]*.5*p[2]/((x-p[1])**2 + (.5*p[2])**2)

def residuals(p,x,y):
  return lorentz(p,x) - y

def convertW(Wn,samples):
  return 2*np.pi*(Wn)/(samples)

def convertQ(Qn,trackLength):
  return 2*np.pi*(Qn)/(trackLength)

def format_Q(value, tick_number):
  # find number of multiples of pi/2
  trackLen = 8192.0
  fracOfPi = (value/trackLen)*2
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"$\pi/8$"
  elif N == 2:
    return r"$\pi/4$"
  elif N == 4:
    return r"$\pi/2$"
  elif N == 8:
    return r"$\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_W(value, tick_number):
  # find number of multiples of pi/2
  fftRuns = 16384.0 
  fracOfPi = (value/fftRuns)-1
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"$\frac{\pi}{8}$"
  elif N == -1:
    return r"$-\frac{\pi}{8}$"
  elif N == 2:
    return r"$\frac{\pi}{4}$"
  elif N == -2:
    return r"$-\pi/4$"
  elif N == 4:
    return r"$\frac{\pi}{2}$"
  elif N == -4:
    return r"$-\pi/2$"
  elif N == 8:
    return r"$\pi$"
  elif N == -8:
    return r"$-\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_Wsum(value, tick_number):
  if value == 0:
    return "0"
  else:
    return value #"{:.1e}".format(value)


#################
#ax1 = plt.subplot2grid((1,3), (0,0), colspan=3)

#Q = int(sys.argv[1]);
dataFiles = [
########################
##### On Server ########
########################
##### 2018-05 data #####
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201805_data/vel5_density0.100_fft16384_track8192_slowP10_runs3.73e+03_numCars819_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_SampleRate16.csv",
##### XMAS data #####
### vel5
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP5_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP20_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.021_fft16384_track8192_slowP40_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#
#"/shared/home/chris/xmasData/vel5_density0.036_fft16384_track8192_slowP5_runs5.02e+03_numCars294_numDataPoints2.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP5_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.072_fft16384_track8192_slowP5_runs5.01e+03_numCars589_numDataPoints4.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.087_fft16384_track8192_slowP5_runs5.00e+03_numCars712_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.101_fft16384_track8192_slowP5_runs5.00e+03_numCars827_numDataPoints6.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP5_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.130_fft16384_track8192_slowP5_runs5.00e+03_numCars1064_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.145_fft16384_track8192_slowP5_runs5.00e+03_numCars1187_numDataPoints9.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.035_fft16384_track8192_slowP10_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP10_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.070_fft16384_track8192_slowP10_runs5.00e+03_numCars573_numDataPoints4.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.084_fft16384_track8192_slowP10_runs5.00e+03_numCars688_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.098_fft16384_track8192_slowP10_runs5.00e+03_numCars802_numDataPoints6.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.112_fft16384_track8192_slowP10_runs5.00e+03_numCars917_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.126_fft16384_track8192_slowP10_runs5.00e+03_numCars1032_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.140_fft16384_track8192_slowP10_runs5.00e+03_numCars1146_numDataPoints9.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.032_fft16384_track8192_slowP20_runs5.00e+03_numCars262_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP20_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.064_fft16384_track8192_slowP20_runs5.00e+03_numCars524_numDataPoints4.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.077_fft16384_track8192_slowP20_runs5.01e+03_numCars630_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.116_fft16384_track8192_slowP20_runs5.00e+03_numCars950_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.103_fft16384_track8192_slowP20_runs5.00e+03_numCars843_numDataPoints6.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.090_fft16384_track8192_slowP20_runs5.00e+03_numCars737_numDataPoints6.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.129_fft16384_track8192_slowP20_runs5.00e+03_numCars1056_numDataPoints8.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.026_fft16384_track8192_slowP40_runs5.02e+03_numCars212_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.042_fft16384_track8192_slowP40_runs5.00e+03_numCars344_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.052_fft16384_track8192_slowP40_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.062_fft16384_track8192_slowP40_runs5.01e+03_numCars507_numDataPoints4.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.073_fft16384_track8192_slowP40_runs5.00e+03_numCars598_numDataPoints4.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.083_fft16384_track8192_slowP40_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.093_fft16384_track8192_slowP40_runs5.00e+03_numCars761_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel5_density0.104_fft16384_track8192_slowP40_runs5.00e+03_numCars851_numDataPoints7.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
### vel9
#"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP5_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP10_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP20_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.012_fft16384_track8192_slowP40_runs5.02e+03_numCars98_numDataPoints8.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
##
#"/shared/home/chris/xmasData/vel9_density0.022_fft16384_track8192_slowP5_runs5.01e+03_numCars180_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP5_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.043_fft16384_track8192_slowP5_runs5.00e+03_numCars352_numDataPoints2.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.052_fft16384_track8192_slowP5_runs5.01e+03_numCars425_numDataPoints3.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP5_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.069_fft16384_track8192_slowP5_runs5.00e+03_numCars565_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.078_fft16384_track8192_slowP5_runs5.01e+03_numCars638_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.086_fft16384_track8192_slowP5_runs5.00e+03_numCars704_numDataPoints5.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP10_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP10_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.050_fft16384_track8192_slowP10_runs5.01e+03_numCars409_numDataPoints3.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.058_fft16384_track8192_slowP10_runs5.00e+03_numCars475_numDataPoints3.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.066_fft16384_track8192_slowP10_runs5.01e+03_numCars540_numDataPoints4.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/xmasData/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.083_fft16384_track8192_slowP10_runs5.01e+03_numCars679_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.015_fft16384_track8192_slowP40_runs5.04e+03_numCars122_numDataPoints1.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.019_fft16384_track8192_slowP20_runs5.02e+03_numCars155_numDataPoints1.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP20_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.038_fft16384_track8192_slowP20_runs5.00e+03_numCars311_numDataPoints2.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.045_fft16384_track8192_slowP20_runs5.01e+03_numCars368_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP20_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.060_fft16384_track8192_slowP20_runs5.00e+03_numCars491_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.068_fft16384_track8192_slowP20_runs5.00e+03_numCars557_numDataPoints4.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.075_fft16384_track8192_slowP20_runs5.00e+03_numCars614_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.023_fft16384_track8192_slowP40_runs5.01e+03_numCars188_numDataPoints1.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.029_fft16384_track8192_slowP40_runs5.01e+03_numCars237_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.035_fft16384_track8192_slowP40_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.041_fft16384_track8192_slowP40_runs5.01e+03_numCars335_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.047_fft16384_track8192_slowP40_runs5.00e+03_numCars385_numDataPoints3.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.053_fft16384_track8192_slowP40_runs5.00e+03_numCars434_numDataPoints3.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/xmasData/vel9_density0.059_fft16384_track8192_slowP40_runs5.00e+03_numCars483_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#####################
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding4.csv",
##"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding8.csv",
###
#"/shared/home/chris/trafData/vel5_density0.120_fft1024_track8192_slowP10_runs4.97e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft2048_track8192_slowP10_runs2.48e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft4096_track8192_slowP10_runs1.24e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft8192_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft16384_track8192_slowP10_runs6.21e+03_numCars983_numDataPoints1.0e+11_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.120_fft32768_track8192_slowP10_runs1.55e+03_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.135_fft4096_track8192_slowP10_runs1.10e+04_numCars1105_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.138_fft4096_track8192_slowP10_runs1.08e+04_numCars1130_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.140_fft4096_track8192_slowP10_runs1.07e+04_numCars1146_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.145_fft4096_track8192_slowP10_runs1.03e+04_numCars1187_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.150_fft4096_track8192_slowP10_runs9.94e+03_numCars1228_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
###
#"/shared/home/chris/trafData/vel5_density0.116_fft4096_track8192_slowP10_runs2.06e+04_numCars950_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.107_fft4096_track8192_slowP20_runs2.23e+04_numCars876_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel5_density0.087_fft4096_track8192_slowP40_runs2.74e+04_numCars712_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",#jam
#"/shared/home/chris/trafData/vel5_density0.075_fft4096_track8192_slowP50_runs3.18e+04_numCars614_numDataPoints8.0e+10_halfdata_NoNorm_hamming_padding2.csv",#jam
#"/shared/home/chris/trafData/vel9_density0.040_fft2048_track8192_slowP50_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.060_fft2048_track8192_slowP50_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.080_fft2048_track8192_slowP50_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel9_density0.100_fft2048_track8192_slowP50_runs2.98e+04_numCars819_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
### Short tests
#"/shared/home/chris/trafData/vel5_density0.080_fft16384_track8192_slowP5_runs9.31e+02_numCars655_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.077_fft16384_track8192_slowP10_runs1.01e+03_numCars630_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.070_fft16384_track8192_slowP20_runs1.02e+03_numCars573_numDataPoints9.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel5_density0.057_fft16384_track8192_slowP40_runs1.01e+03_numCars466_numDataPoints7.7e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#
#"/shared/home/chris/trafData/vel9_density0.050_fft16384_track8192_slowP5_runs9.55e+02_numCars409_numDataPoints6.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.045_fft16384_track8192_slowP10_runs1.01e+03_numCars368_numDataPoints6.1e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.040_fft16384_track8192_slowP20_runs1.04e+03_numCars327_numDataPoints5.6e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
#"/shared/home/chris/trafData/vel9_density0.033_fft16384_track8192_slowP40_runs9.94e+02_numCars270_numDataPoints4.4e+09_halfdata_NoNorm_hamming_padding2_dec14.csv",
###
#"/shared/home/chris/trafData/vel5_density0.050_fft16384_track8192_slowP10_runs3.43e+02_numCars409_numDataPoints2.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
#"/shared/home/chris/trafData/vel5_density0.100_fft16384_track8192_slowP10_runs3.20e+02_numCars819_numDataPoints4.3e+09_halfdata_NoNorm_hamming_padding2dec13.csv",
###
#"/shared/home/chris/trafData/vel15_density0.028_fft2048_track8192_slowP10_runs4.26e+04_numCars229_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel15_density0.043_fft8192_track8192_slowP10_runs1.73e+04_numCars352_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/shared/home/chris/trafData/vel22_density0.019_fft2048_track8192_slowP10_runs6.30e+04_numCars155_numDataPoints2.0e+10_halfdata_NoNorm_hamming_padding2.csv",
##OLD
#"/shared/home/chris/trafData/downloaded/vel5_density0.130_fft2048_track8192_runs4.59e+03_numCars1064_numDataPoints1.0e+10_halfdata_Hamm.csv",
#########################
####### New files #######
#########################
#"/data/trafficFlowData/20171138_newdata/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding2.csv",
#"/data/trafficFlowData/20171138_newdata/vel5_density0.130_fft4096_track8192_slowP10_runs2.29e+03_numCars1064_numDataPoints1.0e+10_halfdata_NoNorm_hamming_padding6.csv",
#####################################
############### random number testing
#####################################
## divided by L
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs1.53e+04_numCars256_numDataPoints1.0e+09_halfdata_rand_noWin.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs1.53e+04_numCars256_numDataPoints1.0e+09_halfdata_Hamm_rand.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft1024_track512_slowP10_runs3.81e+03_numCars256_numDataPoints1.0e+09_halfdata_Hamm_rand.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track2048_slowP10_runs3.81e+03_numCars1024_numDataPoints1.0e+09_halfdata_Hamm_rand.csv",
## divided by T*L^2
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs1.53e+04_numCars256_numDataPoints1.0e+09_halfdata_Hamm_rand_scaledF.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track2048_slowP10_runs3.81e+03_numCars1024_numDataPoints1.0e+09_halfdata_Hamm_rand_scaledF.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft1024_track512_slowP10_runs3.81e+03_numCars256_numDataPoints1.0e+09_halfdata_Hamm_rand_scaledF.csv",
## divided by T*L
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs7.63e+03_numCars256_numDataPoints5.0e+08_halfdata_Hamm_rand_scaledF_1L.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track2048_slowP10_runs1.91e+03_numCars1024_numDataPoints5.0e+08_halfdata_Hamm_rand_scaledF_1L.csv",
# unscaled
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs7.63e+03_numCars256_numDataPoints5.0e+08_halfdata_Hamm_rand_unscaled.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track2048_slowP10_runs1.91e+03_numCars1024_numDataPoints5.0e+08_halfdata_Hamm_rand_unscaled.csv",
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft1024_track512_slowP10_runs1.91e+03_numCars256_numDataPoints5.0e+08_halfdata_Hamm_rand_unscaled.csv",
# unscaled no window
#"/data/Dropbox/Research/TrafficFlow/traffic-flow/vel9_density0.500_fft256_track512_slowP10_runs7.63e+03_numCars256_numDataPoints5.0e+08_halfdata_rand_unscaled_noWin.csv",
############### Testing for p slope
#"/data/trafficFlowData/20171019_ptest/vel9_density0.040_fft1024_track4096_slowP50_runs5.99e+03_numCars163_numDataPoints1.0e+09_halfdata_p_test_hamm.csv",
#####################
####### Vel 5 #######
#####################
#"/data/trafficFlowData/vel5_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.04_fft2048_track8192_runs1.49e+03_numCars327_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.06_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.08_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.09_fft2048_track8192_runs3.31e+04_numCars737_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.10_fft2048_track8192_runs2.98e+03_numCars819_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.11_fft2048_track8192_runs2.71e+04_numCars901_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.12_fft2048_track8192_runs4.96e+02_numCars983_numDataPoints1.0e+09_halfdata_Hamm.csv",
###"/data/trafficFlowData/vel5_density0.13_fft2048_track8192_runs2.29e+04_numCars1064_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.135_fft2048_track8192_runs4.42e+03_numCars1105_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.138_fft2048_track8192_runs4.32e+03_numCars1130_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.14_fft2048_track8192_runs8.52e+02_numCars1146_numDataPoints2.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.145_fft2048_track8192_runs2.06e+04_numCars1187_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.15_fft2048_track8192_runs7.95e+02_numCars1228_numDataPoints2.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.20_fft2048_track8192_runs5.96e+02_numCars1638_numDataPoints2.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.30_fft2048_track8192_runs9.93e+02_numCars2457_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.40_fft2048_track8192_runs7.45e+02_numCars3276_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.50_fft2048_track8192_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.60_fft2048_track8192_runs4.96e+02_numCars4915_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.70_fft2048_track8192_runs4.25e+02_numCars5734_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel5_density0.80_fft2048_track8192_runs3.72e+02_numCars6553_numDataPoints5.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/old/vel5_density0.05_fft2048_track8192_runs1e+04_numCars409_halfdata_Welch.csv",
#####################
####### Vel 9 #######
#####################
#"/data/trafficFlowData/vel9_density0.01_fft2048_track8192_runs3.01e+05_numCars81_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.02_fft2048_track8192_runs1.50e+05_numCars163_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.03_fft2048_track8192_runs9.96e+04_numCars245_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.04_fft2048_track8192_runs7.47e+04_numCars327_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.05_fft2048_track8192_runs5.97e+04_numCars409_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.06_fft2048_track8192_runs4.97e+04_numCars491_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.07_fft2048_track8192_runs4.26e+04_numCars573_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.075_fft2048_track8192_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.08_fft2048_track8192_runs3.73e+04_numCars655_numDataPoints5.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.082_fft2048_track8192_runs7.28e+03_numCars671_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.083_fft2048_track8192_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.0835_fft2048_track8192_runs7.14e+03_numCars684_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.084_fft2048_track8192_runs7.10e+03_numCars688_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.085_fft2048_track8192_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.087_fft2048_track8192_runs6.86e+03_numCars712_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.088_fft2048_track8192_runs6.78e+03_numCars720_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.089_fft2048_track8192_runs6.70e+03_numCars729_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.09_fft2048_track8192_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.095_fft2048_track8192_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.10_fft2048_track8192_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.15_fft2048_track8192_runs3.98e+03_numCars1228_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.20_fft2048_track8192_runs2.98e+03_numCars1638_numDataPoints1.0e+10_halfdata_Hamm.csv",
##"/data/trafficFlowData/vel9_density0.30_fft2048_track8192_runs1.99e+03_numCars2457_numDataPoints1.0e+10_halfdata_Hamm.csv",
#"/data/trafficFlowData/vel9_density0.50_fft2048_track8192_runs1.19e+03_numCars4096_numDataPoints1.0e+10_halfdata_Hamm.csv",
#####################
## Phi Vel 9 and 5 ##
#####################
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP10_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.135_fft2048_track8192_slowP50_runs4.41e+02_numCars1105_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP10_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.145_fft2048_track8192_slowP50_runs4.11e+02_numCars1187_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP10_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel5_density0.160_fft2048_track8192_slowP50_runs3.72e+02_numCars1310_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP10_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.085_fft2048_track8192_slowP50_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP10_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.090_fft2048_track8192_slowP50_runs6.62e+02_numCars737_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP10_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/phifft/vel9_density0.100_fft2048_track8192_slowP50_runs5.96e+02_numCars819_numDataPoints1.0e+09_halfdata_Hamm_phi.csv",
##########################
### High P Vel 9 and 5 ###
##########################
#"/data/trafficFlowData/highP/vel9_density0.010_fft2048_track8192_runs6.03e+03_numCars81_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.020_fft2048_track8192_runs3.00e+03_numCars163_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.030_fft2048_track8192_runs1.99e+03_numCars245_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.050_fft2048_track8192_runs1.19e+03_numCars409_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.060_fft2048_track8192_runs9.94e+02_numCars491_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.070_fft2048_track8192_runs8.52e+02_numCars573_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.075_fft2048_track8192_runs7.95e+02_numCars614_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.080_fft2048_track8192_runs7.45e+02_numCars655_numDataPoints1.0e+09_halfdata_Hamm.csv",
#"/data/trafficFlowData/highP/vel9_density0.085_fft2048_track8192_runs7.01e+02_numCars696_numDataPoints1.0e+09_halfdata_Hamm.csv",
##########################
# Various slow P for V=9 #
##########################
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP5_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP5_runs6.62e+03_numCars737_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.095_fft2048_track8192_slowP5_runs6.28e+03_numCars778_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.100_fft2048_track8192_slowP5_runs5.96e+03_numCars819_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.110_fft2048_track8192_slowP5_runs5.42e+03_numCars901_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
###"/data/trafficFlowData/mixedSlowP/vel9_density0.090_fft2048_track8192_slowP10_runs3.31e+03_numCars737_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP10_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP10_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP20_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP20_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP20_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.080_fft2048_track8192_slowP20_runs7.45e+03_numCars655_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.085_fft2048_track8192_slowP20_runs7.02e+03_numCars696_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP25_runs4.26e+03_numCars573_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP25_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.500_fft2048_track8192_slowP25_runs5.96e+02_numCars4096_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP30_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP30_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP30_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP30_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.075_fft2048_track8192_slowP30_runs7.95e+03_numCars614_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP40_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP40_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP40_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP40_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.065_fft2048_track8192_slowP40_runs9.18e+03_numCars532_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.070_fft2048_track8192_slowP40_runs8.52e+03_numCars573_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.040_fft2048_track8192_slowP50_runs1.49e+04_numCars327_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.045_fft2048_track8192_slowP50_runs1.33e+04_numCars368_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.050_fft2048_track8192_slowP50_runs1.19e+04_numCars409_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.055_fft2048_track8192_slowP50_runs1.08e+04_numCars450_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.060_fft2048_track8192_slowP50_runs9.94e+03_numCars491_numDataPoints1.0e+10_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/mixedSlowP/vel9_density0.200_fft2048_track8192_slowP50_runs1.49e+03_numCars1638_numDataPoints5.0e+09_halfdata_Hamm_phi.csv",
###############################
## Low density Phi
###############################
#"/data/trafficFlowData/lowDensityPhi/vel9_density0.075_fft2048_track8192_slowP10_runs7.95e+05_numCars614_numDataPoints1.0e+12_halfdata_Hamm_phi.csv",
#
###############################
## Phi for various track Lengths
###############################
#"/data/trafficFlowData/varTrackPhi/vel9_density0.0825_fft2048_track4096_slowP10_runs4.35e+03_numCars337_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.0825_fft2048_track8192_slowP10_runs2.17e+03_numCars675_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.0825_fft2048_track16384_slowP10_runs1.08e+03_numCars1351_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.0825_fft2048_track32768_slowP10_runs3.61e+02_numCars2703_numDataPoints2.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.084_fft2048_track4096_slowP10_runs4.26e+03_numCars344_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.084_fft2048_track8192_slowP10_runs2.13e+03_numCars688_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.084_fft2048_track16384_slowP10_runs1.06e+03_numCars1376_numDataPoints3.0e+09_halfdata_Hamm_phi.csv",
#"/data/trafficFlowData/varTrackPhi/vel9_density0.084_fft2048_track32768_slowP10_runs3.54e+02_numCars2752_numDataPoints2.0e+09_halfdata_Hamm_phi.csv",
#
###############################
## Phi S(q)
##############################
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs4.32e+02_numCars339_numDataPoints3.0e+08_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs2.15e+02_numCars679_numDataPoints3.0e+08_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs1.07e+02_numCars1359_numDataPoints3.0e+08_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track32768_slowP10_runs5.30e+01_numCars2719_numDataPoints3.0e+08_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track65536_slowP10_runs8.90e+01_numCars5439_numDataPoints5.0e+08_halfdata_Hamm_SofQ.csv",
#
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track2048_slowP10_runs2.89e+03_numCars169_numDataPoints1.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs2.88e+03_numCars339_numDataPoints2.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+02_numCars679_numDataPoints1.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs1.80e+03_numCars1359_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track32768_slowP10_runs3.59e+02_numCars2719_numDataPoints2.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1048_track65536_slowP10_runs5.26e+02_numCars5439_numDataPoints3.0e+09_halfdata_Hamm_SofQ.csv",
#
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track1024_slowP10_runs5.81e+04_numCars84_numDataPoints1.0e+10_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track2048_slowP10_runs2.89e+04_numCars169_numDataPoints1.0e+10_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_SofQA.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_SofQB.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_SofQC.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_SofQD.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints1.0e+10_halfdata_Hamm_SofQE.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm_SofQA.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm_SofQB.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm_SofQC.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm_SofQD.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints1.0e+10_halfdata_Hamm_SofQE.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm_SofQA.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm_SofQB.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm_SofQC.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm_SofQD.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints1.0e+10_halfdata_Hamm_SofQE.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track32768_slowP10_runs3.59e+03_numCars2719_numDataPoints2.0e+10_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track65536_slowP10_runs1.80e+03_numCars5439_numDataPoints2.0e+10_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft2048_track131072_slowP10_runs8.97e+02_numCars10878_numDataPoints2.0e+10_halfdata_Hamm_SofQ.csv",
################################
## Phi S(q) (with phiAvg)
################################
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track1024_slowP10_runs5.81e+04_numCars84_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track2048_slowP10_runs2.89e+04_numCars169_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track4096_slowP10_runs1.44e+04_numCars339_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track8192_slowP10_runs7.19e+03_numCars679_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track16384_slowP10_runs3.59e+03_numCars1359_numDataPoints5.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track32768_slowP10_runs7.18e+02_numCars2719_numDataPoints2.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track65536_slowP10_runs3.59e+02_numCars5439_numDataPoints2.0e+09_halfdata_Hamm_SofQ.csv",
#"/data/trafficFlowData/SofQ/vel9_density0.083_fft1024_track131072_slowP10_runs1.79e+02_numCars10878_numDataPoints2.0e+09_halfdata_Hamm_SofQ.csv",
#################################
## Phi S(q) (with phiAvg) more
#################################
##"/data/trafficFlowData/SofQmore/vel9_density0.082_fft512_track16384_slowP10_runs1.45e+04_numCars1343_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.083_fft512_track16384_slowP10_runs1.44e+04_numCars1359_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.084_fft512_track16384_slowP10_runs1.42e+04_numCars1376_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.085_fft512_track16384_slowP10_runs1.40e+04_numCars1392_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.086_fft512_track16384_slowP10_runs1.39e+04_numCars1409_numDataPoints1.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.087_fft512_track16384_slowP10_runs1.37e+04_numCars1425_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.088_fft512_track16384_slowP10_runs1.36e+04_numCars1441_numDataPoints1.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.089_fft512_track16384_slowP10_runs1.34e+04_numCars1458_numDataPoints1.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.091_fft512_track16384_slowP10_runs6.55e+03_numCars1490_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.092_fft512_track16384_slowP10_runs6.48e+03_numCars1507_numDataPoints5.0e+09_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.093_fft512_track16384_slowP10_runs6.41e+03_numCars1523_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.094_fft512_track16384_slowP10_runs6.34e+03_numCars1540_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.095_fft512_track16384_slowP10_runs6.28e+03_numCars1556_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.100_fft512_track16384_slowP10_runs5.96e+03_numCars1638_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.130_fft512_track16384_slowP10_runs4.59e+03_numCars2129_numDataPoints5.0e+09_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmore/vel9_density0.150_fft512_track16384_slowP10_runs3.97e+03_numCars2457_numDataPoints5.0e+09_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.180_fft512_track16384_slowP10_runs3.31e+03_numCars2949_numDataPoints5.0e+09_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.200_fft512_track16384_slowP10_runs2.98e+03_numCars3276_numDataPoints5.0e+09_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.250_fft512_track16384_slowP10_runs2.38e+03_numCars4096_numDataPoints5.0e+09_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmore/vel9_density0.300_fft512_track16384_slowP10_runs1.99e+03_numCars4915_numDataPoints5.0e+09_histogram_SofQ.csv",
###################################
## Phi S(q) (with phiAvg) many
###################################
#"/data/trafficFlowData/SofQmany/vel9_density0.08100_fft512_track8192_slowP10_runs5.89e+04_numCars663_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08120_fft512_track8192_slowP10_runs5.87e+04_numCars665_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08140_fft512_track8192_slowP10_runs5.87e+04_numCars666_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08160_fft512_track8192_slowP10_runs5.85e+04_numCars668_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08180_fft512_track8192_slowP10_runs5.83e+04_numCars670_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08200_fft512_track8192_slowP10_runs5.82e+04_numCars671_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08220_fft512_track8192_slowP10_runs5.80e+04_numCars673_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08240_fft512_track8192_slowP10_runs5.79e+04_numCars675_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08260_fft512_track8192_slowP10_runs5.78e+04_numCars676_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08280_fft512_track8192_slowP10_runs5.76e+04_numCars678_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08300_fft512_track8192_slowP10_runs5.75e+04_numCars679_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08320_fft512_track8192_slowP10_runs5.74e+04_numCars681_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08340_fft512_track8192_slowP10_runs5.72e+04_numCars683_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08360_fft512_track8192_slowP10_runs5.71e+04_numCars684_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08380_fft512_track8192_slowP10_runs5.69e+04_numCars686_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08400_fft512_track8192_slowP10_runs5.68e+04_numCars688_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08420_fft512_track8192_slowP10_runs5.67e+04_numCars689_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08440_fft512_track8192_slowP10_runs5.65e+04_numCars691_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08460_fft512_track8192_slowP10_runs5.64e+04_numCars693_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08480_fft512_track8192_slowP10_runs5.63e+04_numCars694_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08500_fft512_track8192_slowP10_runs5.61e+04_numCars696_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08520_fft512_track8192_slowP10_runs5.60e+04_numCars697_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08540_fft512_track8192_slowP10_runs5.59e+04_numCars699_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08560_fft512_track8192_slowP10_runs5.57e+04_numCars701_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08580_fft512_track8192_slowP10_runs5.56e+04_numCars702_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08100_fft512_track16384_slowP10_runs2.94e+04_numCars1327_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08120_fft512_track16384_slowP10_runs2.94e+04_numCars1330_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08140_fft512_track16384_slowP10_runs2.93e+04_numCars1333_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08160_fft512_track16384_slowP10_runs2.92e+04_numCars1336_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08180_fft512_track16384_slowP10_runs2.92e+04_numCars1340_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08200_fft512_track16384_slowP10_runs2.91e+04_numCars1343_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08220_fft512_track16384_slowP10_runs2.90e+04_numCars1346_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08240_fft512_track16384_slowP10_runs2.89e+04_numCars1350_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08260_fft512_track16384_slowP10_runs2.89e+04_numCars1353_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08280_fft512_track16384_slowP10_runs2.88e+04_numCars1356_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08300_fft512_track16384_slowP10_runs2.87e+04_numCars1359_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08320_fft512_track16384_slowP10_runs2.87e+04_numCars1363_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08340_fft512_track16384_slowP10_runs2.86e+04_numCars1366_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08360_fft512_track16384_slowP10_runs2.85e+04_numCars1369_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08380_fft512_track16384_slowP10_runs2.85e+04_numCars1372_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08400_fft512_track16384_slowP10_runs2.84e+04_numCars1376_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08420_fft512_track16384_slowP10_runs2.83e+04_numCars1379_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08440_fft512_track16384_slowP10_runs2.83e+04_numCars1382_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08460_fft512_track16384_slowP10_runs2.82e+04_numCars1386_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08480_fft512_track16384_slowP10_runs2.81e+04_numCars1389_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08500_fft512_track16384_slowP10_runs2.81e+04_numCars1392_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08520_fft512_track16384_slowP10_runs2.80e+04_numCars1395_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08540_fft512_track16384_slowP10_runs2.79e+04_numCars1399_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08560_fft512_track16384_slowP10_runs2.79e+04_numCars1402_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08580_fft512_track16384_slowP10_runs2.78e+04_numCars1405_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08100_fft512_track32768_slowP10_runs1.47e+04_numCars2654_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08120_fft512_track32768_slowP10_runs1.47e+04_numCars2660_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08140_fft512_track32768_slowP10_runs1.46e+04_numCars2667_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08160_fft512_track32768_slowP10_runs1.46e+04_numCars2673_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08180_fft512_track32768_slowP10_runs1.46e+04_numCars2680_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08200_fft512_track32768_slowP10_runs1.45e+04_numCars2686_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08220_fft512_track32768_slowP10_runs1.45e+04_numCars2693_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08240_fft512_track32768_slowP10_runs1.45e+04_numCars2700_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08260_fft512_track32768_slowP10_runs1.44e+04_numCars2706_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08280_fft512_track32768_slowP10_runs1.44e+04_numCars2713_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08300_fft512_track32768_slowP10_runs1.44e+04_numCars2719_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08320_fft512_track32768_slowP10_runs1.43e+04_numCars2726_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08340_fft512_track32768_slowP10_runs1.43e+04_numCars2732_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08360_fft512_track32768_slowP10_runs1.43e+04_numCars2739_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08380_fft512_track32768_slowP10_runs1.42e+04_numCars2745_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08400_fft512_track32768_slowP10_runs1.42e+04_numCars2752_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08420_fft512_track32768_slowP10_runs1.42e+04_numCars2759_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08440_fft512_track32768_slowP10_runs1.41e+04_numCars2765_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08460_fft512_track32768_slowP10_runs1.41e+04_numCars2772_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08480_fft512_track32768_slowP10_runs1.41e+04_numCars2778_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08500_fft512_track32768_slowP10_runs1.40e+04_numCars2785_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08520_fft512_track32768_slowP10_runs1.40e+04_numCars2791_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08540_fft512_track32768_slowP10_runs1.40e+04_numCars2798_numDataPoints2.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08560_fft512_track32768_slowP10_runs1.39e+04_numCars2804_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08580_fft512_track32768_slowP10_runs1.39e+04_numCars2811_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08100_fft512_track65536_slowP10_runs3.68e+04_numCars5308_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08120_fft512_track65536_slowP10_runs3.67e+04_numCars5321_numDataPoints1.0e+11_histogram_SofQ.csv",
###"/data/trafficFlowData/SofQmany/vel9_density0.08140_fft512_track65536_slowP10_runs3.66e+04_numCars5334_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08160_fft512_track65536_slowP10_runs3.65e+04_numCars5347_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08180_fft512_track65536_slowP10_runs3.64e+04_numCars5360_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08200_fft512_track65536_slowP10_runs1.82e+04_numCars5373_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08220_fft512_track65536_slowP10_runs1.81e+04_numCars5387_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08240_fft512_track65536_slowP10_runs1.81e+04_numCars5400_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08260_fft512_track65536_slowP10_runs1.80e+04_numCars5413_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08280_fft512_track65536_slowP10_runs1.80e+04_numCars5426_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08300_fft512_track65536_slowP10_runs1.80e+04_numCars5439_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08320_fft512_track65536_slowP10_runs1.79e+04_numCars5452_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08340_fft512_track65536_slowP10_runs1.79e+04_numCars5465_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08360_fft512_track65536_slowP10_runs1.78e+04_numCars5478_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08380_fft512_track65536_slowP10_runs1.78e+04_numCars5491_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08400_fft512_track65536_slowP10_runs1.77e+04_numCars5505_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08420_fft512_track65536_slowP10_runs1.77e+04_numCars5518_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08440_fft512_track65536_slowP10_runs1.77e+04_numCars5531_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08460_fft512_track65536_slowP10_runs1.76e+04_numCars5544_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08480_fft512_track65536_slowP10_runs1.76e+04_numCars5557_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08500_fft512_track65536_slowP10_runs1.75e+04_numCars5570_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08520_fft512_track65536_slowP10_runs1.75e+04_numCars5583_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08540_fft512_track65536_slowP10_runs1.75e+04_numCars5596_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08560_fft512_track65536_slowP10_runs1.74e+04_numCars5609_numDataPoints5.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/SofQmany/vel9_density0.08580_fft512_track65536_slowP10_runs1.74e+04_numCars5622_numDataPoints5.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08240_fft512_track131072_slowP10_runs3.62e+03_numCars10800_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08260_fft512_track131072_slowP10_runs3.61e+03_numCars10826_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08280_fft512_track131072_slowP10_runs3.60e+03_numCars10852_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08340_fft512_track131072_slowP10_runs3.57e+03_numCars10931_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08360_fft512_track131072_slowP10_runs3.56e+03_numCars10957_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08380_fft512_track131072_slowP10_runs3.56e+03_numCars10983_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08440_fft512_track131072_slowP10_runs3.53e+03_numCars11062_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08460_fft512_track131072_slowP10_runs3.52e+03_numCars11088_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08480_fft512_track131072_slowP10_runs3.51e+03_numCars11114_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08540_fft512_track131072_slowP10_runs3.49e+03_numCars11193_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08560_fft512_track131072_slowP10_runs3.48e+03_numCars11219_numDataPoints2.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/SofQmany/vel9_density0.08580_fft512_track131072_slowP10_runs3.47e+03_numCars11245_numDataPoints2.0e+10_histogram_SofQ.csv",
########
##"/data/trafficFlowData/new/vel9_density0.08100_fft512_track131072_slowP10_runs1.84e+04_numCars10616_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08120_fft512_track131072_slowP10_runs1.84e+04_numCars10643_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08140_fft512_track131072_slowP10_runs1.83e+04_numCars10669_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08160_fft512_track131072_slowP10_runs1.83e+04_numCars10695_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08180_fft512_track131072_slowP10_runs1.82e+04_numCars10721_numDataPoints1.0e+11_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08200_fft512_track131072_slowP10_runs1.45e+04_numCars10747_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08220_fft512_track131072_slowP10_runs1.45e+04_numCars10774_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08300_fft512_track131072_slowP10_runs1.44e+04_numCars10878_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08320_fft512_track131072_slowP10_runs1.43e+04_numCars10905_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08400_fft512_track131072_slowP10_runs1.42e+04_numCars11010_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08420_fft512_track131072_slowP10_runs1.42e+04_numCars11036_numDataPoints8.0e+10_histogram_SofQ.csv",
##"/data/trafficFlowData/new/vel9_density0.08500_fft512_track131072_slowP10_runs1.40e+04_numCars11141_numDataPoints8.0e+10_histogram_SofQ.csv",
#"/data/trafficFlowData/new/vel9_density0.08520_fft512_track131072_slowP10_runs1.40e+04_numCars11167_numDataPoints8.0e+10_histogram_SofQ.csv",
]

priorName = 0
skipCount = 0
for file in dataFiles:

  info,data,maxQ=importData(file)
  
  trackLength = int(info[5])
  fftSamples=int(info[11])
  fftRuns = int(info[13])
  numCars=int(info[1])
  maxVel=int(info[3])
  density = float(numCars)/float(trackLength)
  densityString = "Density: {:.4f}".format(density)
  numDataPoints = int(info[15])
  slowP = int(info[17])
#XXX  Nbar = float(info[21])

  z=(np.array(data).astype(float))
#  for i in range(len(z[0,:])):
#    z[:,i]=np.roll(z[:,i],3*i)

  infoText="Max Vel: {}, ".format(maxVel)
  infoText=infoText+"Car density: {:.04f}, ".format(density)
  infoText=infoText+"Track Length: {}\n".format(trackLength)
  infoText=infoText+"Num of time steps in each FFT: {}, ".format(fftSamples)
  #infoText=infoText+"Num of FFTs: {}\n".format(fftRuns)
  #infoText=infoText+"Number of cars: {}, ".format(numCars)
  #infoText=infoText+"Num of datums: {:.03e}".format(numDataPoints)
  infoText=infoText+"SlowP: {}%".format(slowP)
  print infoText
  print densityString
  
  
  #########################################################
  #Create S(q,w) intensity plot and S(q)
  #########################################################
  
  if 1:
    fig = plt.figure()
    ax1 = plt.subplot2grid((3,3), (0,0), colspan=3)
    ax2 = plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2)
    #ax2.matshow(np.log(z),cmap='autumn')
    padding = 2
    ny = fftSamples*padding
    #Rearrange in standard form
    zLower = np.flipud(z[0:ny/2]) #does not include end index
    zUpper = np.flipud(z[ny/2:ny])
    lnz = np.log(np.vstack((zLower, zUpper)))
#    lnz =  np.log(z)
    #lnzfull = np.hstack((lnz,np.flipud(np.fliplr(lnz))))
#XXX    ax2.pcolormesh(lnz,cmap='Greys',vmin=10,vmax=np.max(lnz))
#    print "max(lnz): ",np.max(lnz)
#    print "min(lnz): ",np.min(lnz)
    #ax2.axvline(4096*2/(maxVel+.9),color='k')
    ax2.set_xlim(0,maxQ)
    ax2.set_ylim(0,ny)
    ax2.grid(True)
    ax1.tick_params(labelsize=15)
    ax2.tick_params(labelsize=15)
    ax2.xaxis.set_major_locator(plt.MultipleLocator((trackLength/2)/4))
    ax2.yaxis.set_major_locator(plt.MultipleLocator((fftSamples*2)/4))
    ax1.xaxis.set_major_locator(plt.MultipleLocator((trackLength/2)/4))
    #ax2.xaxis.set_minor_locator(plt.MultipleLocator((trackLength/2)/8))
    ax2.xaxis.set_major_formatter(plt.FuncFormatter(format_Q))
    ax2.yaxis.set_major_formatter(plt.FuncFormatter(format_W))

    #[lowX,highX] = ax2.get_xlim()
    #[lowY,highY] = ax2.get_ylim()
    #print "[lowX, highX]",lowX,", ",highX
    #lowX_rads = convertQ(lowX,trackLength)
    #highX_rads = convertQ(highX,trackLength)
    #lowY_rads = convertW(lowY,ny)
    #highY_rads = convertW(highY,ny)
    #print "highX_rads: ",highX_rads
    #num_ticks_X = (highX_rads - lowX_rads)/(np.pi/9.0) #Every 1/8th of PI
    #num_ticks_Y = (highY_rads - lowY_rads)/(2*np.pi/9.0) #Every 1/8th of 2*PI
    #print "num_ticks_X: ",num_ticks_X
    #spatial_pi_ticks_locations = np.linspace(lowX, highX,num=int(num_ticks_X))
    #temporal_pi_ticks_locations = np.linspace(lowY, highY,num=int(num_ticks_Y))
    #spatial_pi_ticks_labels = [r'$0$',r'$\frac{\pi}{8}$',r'$\frac{\pi}{4}$',r'$\frac{3\pi}{8}$',r'$\frac{\pi}{2}$',r'$\frac{5\pi}{8}$',r'$\frac{3\pi}{4}$',r'$\frac{7\pi}{8}$',r'$\pi$']
    #temporal_pi_ticks_labels = [r'${-\pi}$',r'$-\frac{3\pi}{4}$',r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$',r'$0$',r'$\frac{\pi}{4}$',r'$\frac{\pi}{2}$',r'$\frac{3\pi}{4}$',r'$\pi$',]
    #pi_ticks_labels = np.linspace(lowX_rads, highX_rads,num=int(num_ticks_X))
    #print spatial_pi_ticks_locations
    #print temporal_pi_ticks_locations
    #ax2.set_xticks(spatial_pi_ticks_locations)
    #ax2.set_yticks(temporal_pi_ticks_locations)
    #ax2.set_xticklabels(spatial_pi_ticks_labels, size=20)
    #ax2.set_yticklabels(temporal_pi_ticks_labels, size=20)
    #ax2.get_yticklabels()[0].set_size(15)
    #ax2.get_yticklabels()[4].set_size(15)

#    ax2.get_xticklabels()[0].set_size(15)
#    ax2.get_xticklabels()[4].set_size(15)
    #for tick in ax2.get_yticklabels()[0,4]:
    #    tick.set_size(15)
    


##my_xticks = []
##for xtic in binDivs:
##  my_xticks.append(str(int(m.floor(xtic)))+":"+str(int((xtic-m.floor(xtic))*60)).zfill(2))
##plt.xticks(binDivs, my_xticks) 
    ax2.set_xlabel("Wave Vector, q", size=15)
    ax2.set_ylabel(r"frequency, $\omega$", size=15)
    #ax2.colorbar()
    #plt.show()
  
   # #nx: time (w)
   # #ny: space (q)
   # nx,ny = z.shape
   # Z = np.transpose(z);
   # x=np.linspace(0,nx-1,nx)
   # y=np.linspace(0,ny-1,ny)
  
   # print z.shape
   # print Z.shape
   # print len(x)
   # print len(y)
  
  
   # xmin = x.min()
   # xmax = x.max()
   # ymin = y.min()
   # ymax = y.max()
   # 
   # plt.subplots_adjust(hspace=0.5)
   # plt.subplot(121)
   # plt.hexbin(x, y, C=z, cmap=plt.cm.YlOrRd_r)
   # plt.axis([xmin, xmax, ymin, ymax])
   # plt.title("Hexagon binning")
   # cb = plt.colorbar()
   # cb.set_label('counts')
   # 
   # plt.subplot(122)
   # plt.hexbin(x, y, Z, bins='log', cmap=plt.cm.YlOrRd_r)
   # plt.axis([xmin, xmax, ymin, ymax])
   # plt.title("With a log color scale")
   # cb = plt.colorbar()
   # cb.set_label('log10(N)')
   # 
   # plt.show()
  

  
  #########################################################
  #Create S(q) plot (sum over all w for each q)
  #########################################################
  
    Z = np.sum(z,axis=0) #sum along time freq axis

   # phiAvgCorrection = (float(Nbar)**2.0/float(trackLength))
   # Z[0] = Z[0] - phiAvgCorrection
    #Z = Z/Z[0]
#    maxZ = np.max(Z)
#    if maxZ != Z[0]:
#      print("Warning: Max S(Q) value not at Q=0")
    ####################
    # Find half width
    #####################
#    halfMax = maxZ/2.0
#    halfQ = 0
#    for q,sq in enumerate(Z):
#      halfQ = q
#      if sq<halfMax:
#        break
#    halfQ = halfQ-.5
    #print "S(0) = ",Z[0]
   # print "halfMax: ",halfMax
   # print "halfQ int: ",halfQ
   # print "halfQ rad: ",convertQ(halfQ,trackLength)
   # halfQrad =  convertQ(halfQ,trackLength)
   # invHalfQ = 1.0/convertQ(halfQ,trackLength)
   # print "1/halfQ: ",1.0/convertQ(halfQ,trackLength)
    #print "Nbar: ",Nbar
    #print "(Nbar^2/L): ",phiAvgCorrection

   # xmin = 0
   # #xmax = trackLength/2#(maxVel-2)
   # ymin = 0
   # ymax = maxZ*1.1
  
    #Zfull = np.hstack((Z,np.flipud(Z)))
    #ax1 = plt.subplot2grid((3,3), (0,0), colspan=3)
    #ax1.semilogy([i/(1.0*trackLength) for i in np.arange(0,len(Z))],Z,label="TrackLength: {}".format(trackLength))
    ###ax1.semilogy([convertQ(i,trackLength) for i in np.arange(0,len(Z))],Z,"o-",label=densityString)
    #ax1.axvline(4096*2/(maxVel+.9),color='k')

    if trackLength == 8192:
      c = 'r'
    elif trackLength == 16384:
      c = 'b'
    elif trackLength == 32768:
      c = 'g'
    elif trackLength == 65536:
      c = 'k'
    elif trackLength == 131072:
      c = 'm'

    ## Scaled Z0 plot
    #dc = .0812 + 2.01*trackLength**(-.76) #dc = d0 + c*L^-b
    #x0 = (density - dc)*trackLength**.13 # (d - dc)L^\nu
    #y0 = np.log((Z0/(Nbar/float(trackLength)))*trackLength**(-.2)) #L^a
    #Y = Z/(Nbar/float(trackLength))
    #dc = float(.0812 + 1.15*float(trackLength)**(-.71)) #dc = d0 + c*L^-b
    #X = [convertQ(i,trackLength) for i in np.arange(0,len(Z))]

    X = np.array([i for i in np.arange(0,len(Z)+0)])#*np.absolute(density/dc - 1)**(-1/.14)
    Y = Z[0:]#XXX/Nbar
    #dY = np.zeros(Y.shape,np.float)
    #dY[0:-1] = np.diff(np.log(Y))/np.diff(np.log(X))
    #dY[-1] = (Y[-1]-Y[-2])/(X[-1]-X[-2])
    dCrit = .0821+1.95*trackLength**(-.76)
    if priorName != trackLength:
      priorName = trackLength
      ax1.plot((X),(Y),"-",color='k',label="T:{:} D:{:.05}".format(trackLength,density/dCrit-1))
    else:
      ax1.plot((X),(Y),"-",color=c,label="D:{:.05}".format(density/dCrit-1))

#    Zfull = np.hstack((Z,np.flipud(Z)))
#
#    zSpaceDomain = np.absolute(np.fft.ifft(Zfull))[0:trackLength/2.0]
#    X = np.array([i/float(trackLength) for i in np.arange(0,len(zSpaceDomain)+0)])
#    ax1.semilogy(X,zSpaceDomain,"-o",markersize=3,markeredgewidth=0,color=c,label="D:{:.05}".format(density/dCrit-1))

    


      

  
    ax1.set_title("Structure Factor\n"+infoText)

    #ax1.set_xlabel('Wave vector, 2$\pi$*Q/L')
    ax1.set_xlabel('')
    ax1.set_xlim([0,max(X)])
    #ax1.set_xticks(spatial_pi_ticks_locations)
    ax1.set_xticklabels('')
    


    #ax1.yaxis.set_major_locator(plt.MultipleLocator(max(Z)/4))
    #ax1.yaxis.set_major_formatter(plt.FuncFormatter(format_Wsum))
    #ax1.set_ylabel(r"S(q)/Nbar")
    ax1.set_ylabel(r'Sum over freq', family='serif', size=15)
    #XXX ax1.set_yticklabels('')
    ax1.set_ylim([0,max(Y)*1.1])

    ax1.grid(b=True,which='major',color='k', linestyle='dotted')
 #   ax1.grid(b=True, which='minor', color='k', linestyle='dotted')
    plt.show()
  
  #########################################################
  # Look at the omega peaks in a single q slice
  #########################################################

  if 0:
    #xmin = 0
    #xmax = len(Z)
    #ymin = 0
    #ymax = np.max(Z)*1.1
  
    fig = plt.figure()
    for Qn in range(10,40,2):
      plt.plot((np.roll(z[:,Qn],0)),'-o',label=Qn) #grab every omega value for given q values
    
#    plt.title("Frequency Power Spectrum\n"+infoText)
    plt.xlabel('Freq, w')
    plt.ylabel("Power")
    plt.legend()
  
#   plt.axis([xmin, xmax, ymin, ymax])
    plt.grid(True)
    plt.title("Q slices\n"+infoText)
    fig.tight_layout()
  
  
  
  #########################################################
  #Plot S(q,w) in 3D
  #########################################################
  
  if 0:
    #nx: time (w)
    #ny: space (q)
    nx,ny = z.shape
    x=np.linspace(0,nx-1,nx)
    y=np.linspace(0,ny-1,ny)
    Z=np.transpose(z)
    
    #Enable to rearange data to make (0,0) in the middle
    if False:
      uLeft=z[0:nx/2+1,0:ny/2+1]
      lLeft=z[nx/2+1:nx,0:ny/2+1]
      uRight=z[0:nx/2+1,ny/2+1:ny]
      lRight=z[nx/2+1:nx,ny/2+1:ny]
      
      left=np.concatenate((lLeft,uLeft))
      right=np.concatenate((lRight,uRight))
      Z=np.concatenate((right,left),axis=1)
      
      x=np.linspace(-nx/2+1,nx/2,nx)
      y=np.linspace(-ny/2+1,ny/2,ny)
  
    #Enable to normalize as S(q,w)/S(q)
    if False:
      Z=np.empty([nx,ny])
      Sq = np.sum(z,axis=0)
      for i in range(0,ny):
        for j in range(0,nx):
          Z[j,i] = z[j,i]/Sq[i]
   
  
    X,Y=np.meshgrid(x,y)
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')  #space     #time 
    #X, Y, Z = axes3d.get_test_data(0.05)
    cset = ax.contourf(X, Y, Z, cmap=cm.coolwarm)
    ax.plot_surface(X, Y, Z, rstride=80, cstride=50, alpha=0.3,linewidth=1)
  #  cset = ax.contour(X, Y, Z, zdir='z', offset=-100, cmap=cm.coolwarm)
    cset = ax.contour(X, Y, Z, zdir='x', offset=-400, cmap=cm.coolwarm)
    cset = ax.contour(X, Y, Z, zdir='y', offset=400, cmap=cm.coolwarm)
  
    #ax.set_xlabel('X')
    #ax.set_xlim(-40, 40)
    #ax.set_ylabel('Y')
    #ax.set_ylim(-40, 40)
    #ax.set_zlabel('Z')
    #ax.set_zlim(-100, 100)
  
  #  surf=ax.plot_wireframe(np.fliplr(X),Y,np.transpose(Z),rstride=80,cstride=30,
  #                        linewidth=1,cmap=cm.coolwarm)
  
    #plt.xlabel('Omega')
    #plt.ylabel("q")
    plt.grid(True)
    #plt.tight_layout()
    #plt.savefig("data_1e5datums",ext="png", close=True)
    #savefig('foo.png', bbox_inches='tight')
    #plt.close()

# Enable for subfigure plots
#plt.tight_layout()
#plt.subplots_adjust(hspace=.15)
#plt.legend(loc="upper right",fontsize="small",numpoints=1,ncol=5)
plt.show()

