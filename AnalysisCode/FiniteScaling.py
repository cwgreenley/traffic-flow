import sys
import csv
from mpl_toolkits.mplot3d import axes3d
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.widgets import Slider
import itertools

import math as m
from scipy.optimize import least_squares

def importData(fileName):

  dataArray=[]
  file=open(fileName,'rb')
  data = csv.reader(file,delimiter=",")
  for row in data:
    dataArray.append(row) #[1:] skips the first entry on each row
  file.close()
  
  return dataArray

def xScaling(b,c,nu,xdata,trackLength):
  dc = .0812 + c*trackLength**(-b) #dc = d0 + c*L^-b
  return (xdata - dc)*trackLength**nu # (d - dc)L^\nu

def yScaling(a,ydata,trackLength):
  return np.log(ydata*trackLength**(-a)) #L^a

#################
fig = plt.figure()
ax1 = fig.add_subplot(111)
fig.subplots_adjust(left=.25,bottom=.3)
axis_color = 'lightgoldenrodyellow'

colors = itertools.cycle(('b', 'g', 'r', 'brown','c', 'm', 'y', 'k','darksalmon'))

a0 = 1.0
c0 = 1.0
b0 = 1.0
nu0 = 0.0 
#a0 = .2
#c0 = 2.01
#b0 = .76
#nu0 = .13
#Good sets
a0 = .17
b0 = .71
c0 = 1.15
nu0 = .14
#Q5
a0 = 0.0
b0 = .68
c0 = 4.33
nu0 = .5


data=importData("S0to50.dat")


dataX = []
dataY = []
dataMeta = []


#for q in range(1,3):
priorName = 0
T = -1
qVal = 0
for i in data:
  
  trackLength = int(i[0])
  density = float(i[1])
  Nbar = float(i[2])
  #S(0) = i[3]
  Sq = float(i[qVal+3])/(Nbar/float(trackLength))

  if priorName != trackLength:
    priorName = trackLength
    dataX.append([])
    dataY.append([])
    dataMeta.append(trackLength)
    T += 1

  dataX[T].append(density)
  dataY[T].append(Sq)

residualSum = 1000000.0

##### BEST FIT FINDING #####
for a in np.linspace(.13,.18,12):
  print "Started a="+str(a)
  for b in np.linspace(.3,1.0,15):
    for c in np.linspace(.1,2,20):
      for nu in np.linspace(0,.2,20):
        ######## Binning #########
        scaledXtemp = []
        scaledYtemp = []
        for k,trackL in enumerate(dataMeta):
          scaledXtemp.append(xScaling(b,c,nu,np.array(dataX[k]),trackL))
          scaledYtemp.append(yScaling(a,np.array(dataY[k]),trackL))
        scaledX = np.array(scaledXtemp).flatten()
        scaledY = np.array(scaledYtemp).flatten()
        
        minBin = scaledX.min()-.01*abs(scaledX.min())
        binNum = 8
        bins = np.linspace(minBin,scaledX.max(),binNum+1)
        binIndex = np.digitize(scaledX,bins,right=True)
        
        fitCoef = np.zeros((binNum,2))
        res = np.zeros((binNum,1))
        for i in range(binNum): #Range starts at 0, but lowest bin index is 1
          toFitX = []
          toFitY = []
          for j,xVal in enumerate(scaledX):
            if binIndex[j] == i+1:
              toFitX.append(xVal)
              toFitY.append(scaledY[j])
          if len(toFitX) == 0 or len(toFitX) == 1:
            break
          if len(toFitX) == 2:
            continue
          fitCoef[i], [res[i]], _, _, _  = np.polyfit(toFitX,toFitY,1,full=True)

        if res.sum() < residualSum:
          residualSum = res.sum()
          print residualSum
          a0 = a
          b0 = b
          c0 = c
          nu0 = nu
print "{},{},{},{},{}".format(qVal,a0,b0,c0,nu0)

##infoText=infoText+"Num of datums: {:.03e}".format(numDataPoints)

#for b in bins:
#  ax1.axvline(b,0,1)
#
#for i,p in enumerate(fitCoef):
#  xp = np.linspace(bins[i],bins[i+1],3)
#  yp = p[0]*xp+p[1]
#  ax1.plot(xp,yp,'k-',linewidth=2)

#a0 = .16
#b0 = .76
#c0 = 1.95
#nu0 = .11

lines = []
lines1 = []
for k,trackL in enumerate(dataMeta):
  xVals = xScaling(b0,c0,nu0,np.array(dataX[k]),float(trackL))
  yVals = yScaling(a0,np.array(dataY[k]),float(trackL))
  print(trackL)
  for i in range(0,len(xVals)):
    print(xVals[i], yVals[i])
  c = colors.next()
  [line] = ax1.plot(xVals,yVals,'o-',markersize=2,label=str(trackL)+'_S1', color=c)
  lines.append(line)
ax1.legend(loc="lower right")


a_slider_ax  = fig.add_axes([0.25, 0.2, 0.65, 0.03], axisbg=axis_color)
a_slider = Slider(a_slider_ax, 'a', -.2, 1.0, valinit=a0)
b_slider_ax  = fig.add_axes([0.25, 0.15, 0.65, 0.03], axisbg=axis_color)
b_slider = Slider(b_slider_ax, 'b', 0, 2, valinit=b0)
c_slider_ax  = fig.add_axes([0.25, 0.1, 0.65, 0.03], axisbg=axis_color)
c_slider = Slider(c_slider_ax, 'c', 0, 10, valinit=c0)
nu_slider_ax  = fig.add_axes([0.25, 0.05, 0.65, 0.03], axisbg=axis_color)
nu_slider = Slider(nu_slider_ax, 'nu', -.1, .8, valinit=nu0)
def sliders_on_changed(val):
  for k,trackL in enumerate(dataMeta):
    lines[k].set_ydata(yScaling(a_slider.val,np.array(dataY[k]),trackL))
    lines[k].set_xdata(xScaling(b_slider.val,c_slider.val,nu_slider.val,np.array(dataX[k]),trackL))
  # recompute the ax.dataLim
  ax1.relim()
  # update ax.viewLim using the new dataLim
  ax1.autoscale_view()
  fig.canvas.draw_idle()
a_slider.on_changed(sliders_on_changed)
b_slider.on_changed(sliders_on_changed)
c_slider.on_changed(sliders_on_changed)
nu_slider.on_changed(sliders_on_changed)

#  if priorName != trackLength:
#    priorName = trackLength
#    ax1.plot(x0,y0,"o",color=c,label=trackLength)
#  else:
#    ax1.plot(x0,y0,"o",color=c)


##infoText=infoText+"Num of datums: {:.03e}".format(numDataPoints)
#
##########################################################
##Create S(q) plot (sum over all w for each q)
##########################################################
#
#  if trackLength == 8192:
#    c = 'r'
#  elif trackLength == 16384:
#    c = 'b'
#  elif trackLength == 32768:
#    c = 'g'
#  elif trackLength == 65536:
#    c = 'k'
#  elif trackLength == 131072:
#    c = 'm'
#
  ## Scaled Z0 plot
#    
#  ax1.plot(x0,y0,"o",color=c)
#
#  #ax1.set_title("Structure Factor\n"+infoText)
#  #ax1.set_xlabel('Wave vector, 2$\pi$*Q/L')
#  #ax1.set_ylabel(r"Sum over $\omega$, S(q)")
#
#  #ax1.set_xlim([])
#  #ax1.set_ylim([2, 30])

ax1.set_title("S(q={})".format(qVal))
ax1.grid(True)
#plt.tight_layout()
plt.show()


