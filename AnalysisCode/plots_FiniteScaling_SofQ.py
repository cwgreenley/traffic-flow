# Built-in python libraries
import sys
import os
import csv
import scipy.signal as sig

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
#import seaborn as sns
import pandas as pd
import numpy as np
import json


dataFiles = [
"/data/trafficFlowData/201810_data/15/vel9_density0.08160_fft512_track131072_slowP10_runs7.50e+03_numCars10695_numDataPoints4.1e+10_histogram_phi_SofQ.csv",
"/data/trafficFlowData/201810_data/15/vel9_density0.08176_fft512_track65536_slowP10_runs7.50e+03_numCars5358_numDataPoints2.1e+10_histogram_phi_SofQ.csv",
"/data/trafficFlowData/201810_data/15/vel9_density0.08203_fft512_track32768_slowP10_runs7.50e+03_numCars2687_numDataPoints1.0e+10_histogram_phi_SofQ.csv",
"/data/trafficFlowData/201810_data/15/vel9_density0.08252_fft512_track16384_slowP10_runs7.50e+03_numCars1352_numDataPoints5.2e+09_histogram_phi_SofQ.csv",
"/data/trafficFlowData/201810_data/15/vel9_density0.08325_fft512_track8192_slowP10_runs7.51e+03_numCars681_numDataPoints2.6e+09_histogram_phi_SofQ.csv",
]

markers = itertools.cycle(('o', 'v', 's','*', '^', 'p',  'h', '<', 'H', 'D', '>','d'))
lineSty = itertools.cycle(([18,4],[4,2],[10,2,3,2],[8,2,3,2,3,2],[2,2]))
#colors = itertools.cycle(('k', 'g', 'b', 'r','brown','c', 'm', 'y','darksalmon'))
colors = ['k', 'g', 'b', 'r','brown','c', 'm','y']

savePlot = False
saveFileName = "scaling_StaticStructureFactor"
skipNum = 1
legend = True
#xlimits = (1e-4,None)
xlimits = (0,.01*1000)
ylimits = (1,None)

def main():

  fig,ax = prepare_plot_canvas()
  metaInfo = range(len(dataFiles))

  for f,filePath in enumerate(dataFiles):
    info,data = importData(filePath)
    print(f)
    metaInfo[f] = info
    data = prepareData(info,data)
    plot_SofQ(info,data,ax)
  formatAxes(info,data,ax)

  if(savePlot): save_plot(fig,metaInfo)
  plt.show()

###################################################################

def importData(fileName):

  with open(fileName,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()

    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "sampleRate"  : int(infoArray[7]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fracJams"    : float(infoArray[19]),
            "phiAvg"      : float(infoArray[21]),
            "notes"       : str(infoArray[22])
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])
    dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])
    info['fracCritDen'] = info['density']/dCrit

    maxQ = int((info['trackLength']/2+1)) #max possible Q, may be smaller
    info['maxQ'] = maxQ
    dataArray = np.array(data.next()[0:maxQ],dtype='d')

  return info,dataArray


def prepareData(info,ydata):
  maxQ = info['maxQ']
  skips = skipNum
  data = np.empty([2,maxQ])
  data.fill(np.NAN)
  #Create wave vector values in units of vmax
  radians = np.array(range(0,maxQ))*2.0*np.pi/float(info['trackLength'])
  spacing = float(info['maxVel'])/(2.0*np.pi)
  data[0] = radians*spacing
  ydata = ydata/float(info['trackLength'])
  deltathing = (info['phiAvg'])**2/float(info['trackLength'])
  print(deltathing)
  ydata[0] = ydata[0] - deltathing
  print(info['phiAvg']**2)#/float(info['trackLength']))
  data[1] = ydata/ydata[0]#info['phiAvg']
  #data = data[0:maxQ:skips]

  return data

##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def set_style():
#  sns.set()

  singleColWidth_mm = 89.0*2
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch#/1.6

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'lines.markersize': 5,
    'lines.linewidth': 2.0,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  plt.style.use('classic')
  mpl.rcParams.update(params)

def plot_SofQ(info,data,ax):

  makePlots(ax,info,data)
  formatLabels(info,ax)
  plt.tight_layout()
  plt.legend(loc="upper right",numpoints=2,ncol=1)


def makePlots(ax,info,data):
  m = markers.next()
  l = lineSty.next()
  if info['trackLength'] == 8192: c = colors[0]
  elif info['trackLength'] == 16384: c = colors[1]
  elif info['trackLength'] == 32768: c = colors[2]
  elif info['trackLength'] == 65536: c = colors[3]
  elif info['trackLength'] == 131072: c = colors[4]
  else: c = colors[5]

  legendText = "{}".format(info['trackLength'])
  ax.plot(data[0]*1000,data[1],dashes=l,marker='',color=c,label=legendText)


def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def formatAxes(info,data,ax):
  # X-axes
  #ax.xaxis.set_major_locator(plt.MultipleLocator(1))
  ax.set_xlim(xlimits)

  # Y-axes
  ax.set_ylim(ylimits)
  #axSq.yaxis.set_major_locator(plt.MultipleLocator(SofQmax/3))
  #axSq.yaxis.set_major_formatter(plt.FuncFormatter(format_Wsum))
  #axSq.set_yticklabels('')
  #axSq.yaxis.labelpad = 28

  #for tick in axSqw.get_yticklabels():
  #  tick.set_rotation(35)

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(info,ax):
    ax.set_xlabel(r'$10^3\times$qV$_{\mathrm{max}}$/(2$\pi$)')
    #ax.set_xlabel(r'Integer Q')
    ax.set_ylabel(r'$S_\phi(q)$/$S_\phi(q=0)$')
    #ax.set_ylabel(r'$S_\phi(q)$/$\overline{S_\phi(q)}$')

def format_Wsum(value, tick_number):
  if value == 0:
    return "0"
  else:
    return "{:.1f}".format(value)

def save_plot(fig,metaInfo):
  global saveFileName
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
      for f,filePath in enumerate(dataFiles):
        json.dump(metaInfo[f], myfile)
        myfile.write("\n")
        myfile.write(filePath+"\n\n")
  

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))


#############
if __name__ == "__main__":
  main()


