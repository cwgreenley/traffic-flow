# Built-in python libraries
import sys
import os
import csv
import scipy.signal as sig

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json


dataFiles = [
### 2018-07 ###
"/shared/home/chris/201807_data/08/vel9_density0.081_fft16384_track8192_slowP10_runs7.00e+03_numCars663_sampleRate1_numDataPoints7.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201901_data/03/vel9_density0.08140_fft16384_track8192_slowP10_runs5.00e+03_numCars667_sampleRate1_numDataPoints5.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201901_data/03/vel9_density0.08180_fft16384_track8192_slowP10_runs5.00e+03_numCars670_sampleRate1_numDataPoints5.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201901_data/03/vel9_density0.08220_fft16384_track8192_slowP10_runs5.00e+03_numCars673_sampleRate1_numDataPoints5.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201901_data/03/vel9_density0.08260_fft16384_track8192_slowP10_runs5.00e+03_numCars677_sampleRate1_numDataPoints5.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.083_fft16384_track8192_slowP10_runs7.01e+03_numCars679_sampleRate1_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08301_fft16384_track8192_slowP10_runs5.00e+03_numCars680_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08313_fft16384_track8192_slowP10_runs5.00e+03_numCars681_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08325_fft16384_track8192_slowP10_runs5.00e+03_numCars682_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08337_fft16384_track8192_slowP10_runs5.00e+03_numCars683_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08350_fft16384_track8192_slowP10_runs5.00e+03_numCars684_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08362_fft16384_track8192_slowP10_runs5.00e+03_numCars685_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08374_fft16384_track8192_slowP10_runs5.00e+03_numCars686_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08386_fft16384_track8192_slowP10_runs5.00e+03_numCars687_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08398_fft16384_track8192_slowP10_runs5.00e+03_numCars688_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08411_fft16384_track8192_slowP10_runs5.00e+03_numCars689_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08423_fft16384_track8192_slowP10_runs5.00e+03_numCars690_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08435_fft16384_track8192_slowP10_runs5.00e+03_numCars691_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08447_fft16384_track8192_slowP10_runs5.00e+03_numCars692_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08459_fft16384_track8192_slowP10_runs5.00e+03_numCars693_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/20/vel9_density0.08484_fft16384_track8192_slowP10_runs5.00e+03_numCars695_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.085_fft16384_track8192_slowP10_runs7.00e+03_numCars696_sampleRate1_numDataPoints8.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.087_fft16384_track8192_slowP10_runs7.01e+03_numCars712_sampleRate1_numDataPoints8.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.088_fft16384_track8192_slowP10_runs7.01e+03_numCars720_sampleRate1_numDataPoints8.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.090_fft16384_track8192_slowP10_runs7.00e+03_numCars737_sampleRate1_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
"/shared/home/chris/201807_data/08/vel9_density0.092_fft16384_track8192_slowP10_runs7.01e+03_numCars753_sampleRate1_numDataPoints8.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
]

slopeData = np.full((len(dataFiles),3),np.nan)

##first col is for density, second is for peakVal
#dataShape = (len(dataFiles)/6+1,2) 
#dataDict = {
#  "8192"  : {
#    "5" : np.full(dataShape,np.nan),
#    "9" : np.full(dataShape,np.nan)},
#  "16384" : {
#    "5" : np.full(dataShape,np.nan),
#    "9" : np.full(dataShape,np.nan)},
#  "32768" : {
#    "5" : np.full(dataShape,np.nan),
#    "9" : np.full(dataShape,np.nan)}
#}

markers = itertools.cycle(('o', 'v', 's','*', '^', 'p',  'h', '<', 'H', 'D', '>','d'))
lineSty = itertools.cycle(([18,2],[4,2],[10,2,3,2]))
colors = itertools.cycle(('k', 'r', 'b', 'brown','c', 'm', 'y', 'g','darksalmon'))

savePlot = False

saveFileName = "jamVelocity_vs_density"
legend = False
#xlimits = (.7,1.2)
xlimits = (0,None)
ylimits = (0,None)

def main():

  fig,ax = prepare_plot_canvas()
  metaInfo = range(len(dataFiles))

  for f,filePath in enumerate(dataFiles):
    info,headers,data = importData(filePath+"_JamPeakDataNew.csv")
    metaInfo[f] = info
    prepareData(info,data,f)

  #print(slopeData)
  plot_data(ax)
  formatAxes(ax)

  if(savePlot): save_plot(fig,metaInfo)
  #plt.show()

###################################################################

def importData(fileName):

  with open(fileName,'rb') as file:
    numLines = len(file.readlines())
  with open(fileName,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()
    headerArray = data.next()

    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "sampleRate" : int(infoArray[7]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fftPadding"  : int(infoArray[19]),
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])
    dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])
    info['fracCritDen'] = info['density']/dCrit

    #maxQ = int(info['trackLength']/2) #max possible Q, may be smaller
    maxQ = numLines-3

    #Prepare numpy array for holding data
    dataShape = [maxQ+1, 2]
    dataArray=np.empty(dataShape,dtype=float)
    dataArray.fill(np.NAN)

    nRows = 0
    for i,row in enumerate(data):
      dataArray[i][0] = row[0]
      dataArray[i][1] = row[3]
      nRows += 1
     
    info['nRows'] = nRows

  return info,headerArray,dataArray


def prepareData(info,data,f):
  maxQ = info['nRows']

  data = data[0:maxQ+1]
  xData = data[:,0]
  yData = data[:,1]
  indices = np.isfinite(yData) & np.isfinite(yData)
  linFitParam,covariance = np.polyfit(xData[indices],yData[indices],1,cov=True)
  M = linFitParam[0]
  dM,_ = np.sqrt(np.diagonal(covariance))
  slopeData[f][0] = info["fracCritDen"]
  slopeData[f][1] = abs(M)
  slopeData[f][2] = dM

  #data[:,1] = sig.medfilt(data[:,1],3)
  #data[:,1] = sig.savgol_filter(data[:,1],5,1,mode='interp')
  #data[:,1] = sig.savgol_filter(data[:,1],5,1,mode='interp')



##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def set_style():
#  sns.set()

  singleColWidth_mm = 89.0
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch/1.4

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'lines.markersize': 2,
    'lines.linewidth': 1.8,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  plt.style.use('classic')
  mpl.rcParams.update(params)

def plot_data(ax):

  makePlots(ax)
  formatLabels(ax)
  plt.tight_layout()
#  if legend: plt.legend(loc="upper left",numpoints=2,ncol=1)

def makePlots(ax):

  X = slopeData[:,0]
  Y = slopeData[:,1]
  dY= slopeData[:,2]
  plt.errorbar(X, Y, yerr=dY, 
                marker='o', linestyle='',color='black',
                ecolor='gray', elinewidth=1, capsize=3);
  plt.xticks(rotation=45)


def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def formatAxes(ax):
  # X-axes
  #ax.set_xlim(xlimits)
  # Y-axes
  #ax.set_ylim(ylimits)

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(ax):
  ax.set_xlabel("Fraction of Critical density")
  ax.set_ylabel("Jam Velocity")


def save_plot(fig,metaInfo):
  global saveFileName
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
      for f,filePath in enumerate(dataFiles):
        json.dump(metaInfo[f], myfile)
        myfile.write("\n")
        myfile.write(filePath+"\n\n")
  

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))


#############
if __name__ == "__main__":
  main()


