import numpy as np

def criticalDensity(vmax,slowp,trackLen):
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def critDenFrac(vmax,slowp,trackLen,den):
  return den/criticalDensity(vmax,slowp,trackLen)

slowp = .1
vmax = [5,9]
track = [8192,16384,32768]
for v in vmax:
  for tL in track:
    crit = criticalDensity(v,slowp,tL)
    print("Critical Density for slowp={},vmax={},track={}: {}"\
            .format(slowp,v,tL,crit))
