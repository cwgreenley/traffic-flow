# Built-in python libraries
import sys
import os
import csv

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json

noJams_path5 = "/shared/home/chris/xmasData/vel5_density0.126_fft16384_track8192_slowP10_runs5.00e+03_numCars1032_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv"
noJams_path9 = "/shared/home/chris/xmasData/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv"

jams_path5 = "/shared/home/chris/201807_data/08/vel5_density0.157_fft16384_track8192_slowP10_runs7.00e+03_numCars1286_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv"
jams_path9 = "/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate1_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv"

phi_path5 = "/shared/home/chris/201807_data/31/vel5_density0.15698_fft16384_track8192_slowP10_runs4.00e+03_numCars1286_sampleRate1_numDataPoints8.4e+10_pad2_halfdata_NoNorm_hamming_phi.csv"
phi_path9 = "/shared/home/chris/201807_data/31/vel9_density0.09094_fft16384_track8192_slowP10_runs4.00e+03_numCars745_sampleRate1_numDataPoints4.9e+10_pad2_halfdata_NoNorm_hamming_phi.csv"


smallfilePath =  "/shared/home/chris/trafData/vel5_density0.120_fft1024_track8192_slowP10_runs4.97e+04_numCars983_numDataPoints5.0e+10_halfdata_NoNorm_hamming_padding2.csv"
trackLength = 0
fftSamples = 0

### MODIFY ###
currentDataPath = phi_path9
savePlot = False

if currentDataPath==jams_path5: fileName = "SofQandW_withjams_vmax5"
if currentDataPath==jams_path9: fileName = "SofQandW_withjams_vmax9"
if currentDataPath==noJams_path5: fileName = "SofQandW_withoutjams_vmax5"
if currentDataPath==noJams_path9: fileName = "SofQandW_withoutjams_vmax9"
if currentDataPath==phi_path5: fileName = "SofQandW_phi_vmax5"
if currentDataPath==phi_path9: fileName = "SofQandW_phi_vmax9"

def main():
  global trackLength
  global fftSamples

  info,data = importData(currentDataPath)
  trackLength = info['trackLength']
  fftSamples = info['fftSamples']

  SofQW,SofQ = prepareData(info,data)
  fig = plot_Sq_and_Sqw(info,SofQW,SofQ)

  if savePlot: save_plot(fig,info)
  plt.show()
#  plt.close()
  #return info, SofQW, SofQ

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))


def importData(fileName):

  with open(fileName,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()
    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fftPadding"  : int(infoArray[19])
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])

    maxQ    =  int(info['trackLength']/2) 
    firstCol = 1 #Use '1' to skip the first col which holds the DC component
    lastCol = maxQ #Decrease lastCol to only look at lower Q values

    info['nRows']   =  info['fftSamples']*info['fftPadding']
    info['nCols']   =  lastCol - firstCol

    #Prepare numpy array for holding 
    dataShape = [info['nRows'], info['nCols']]
    dataArray=np.empty(dataShape,dtype=float)

    midPoint = info['nRows']/2-1
    for i,row in enumerate(data):
      if(i<=midPoint):
        index = midPoint - i
      else:
        index = info['nRows'] + midPoint - i
      dataArray[index] = row[firstCol:lastCol]

  return info,dataArray


def prepareData(info,SofQW):

  SofQ = np.sum(SofQW,axis=0)
  #SofQmax = np.max(SofQ)
  #info['SofQdivisor'] = 10**(len(str(int(SofQmax)))-1)
  #SofQ = SofQ/info['SofQdivisor']

  SofQW = np.log(SofQW)

  return SofQW,SofQ

def set_style():
#  sns.set()

  singleColumnWidth_mm = 89.0*2
  singleColumnWidth_inch = singleColumnWidth_mm/25.4
  singleColumnHeight_inch = singleColumnWidth_inch

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'figure.figsize': [singleColumnWidth_inch,singleColumnHeight_inch]
  }
  plt.style.use('classic')
#  plt.style.use(['seaborn-white', 'seaborn-paper'])
#  sns.set(context='paper',rc=params)
  mpl.rcParams.update(params)
##  matplotlib.rc("font", family="Times New Roman")

def prepareFigure():
  fig   = plt.figure()
  axSq  = plt.subplot2grid((3,3), (0,0), colspan=3)
  axSqw = plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2)
  return fig,axSq,axSqw

def makePlots(axSq,axSqw,SofQ,SofQW):
  if currentDataPath in [jams_path5, jams_path9, phi_path5, phi_path9]:
    axSq.plot(np.log(SofQ/(8192*32768)),color='k')
  else:
    axSq.plot((SofQ),color='k')
  minAmp = np.min(SofQW)
  maxAmp = np.max(SofQW)
  print( "min: {}, max: {}".format(minAmp,maxAmp) )
  if currentDataPath in [phi_path5,phi_path9]:
    minFrac = .2
    maxFrac = .4
  else:
    minFrac = .4
    maxFrac = .2
  vMin = (maxAmp-minAmp)*minFrac + minAmp
  vMax = maxAmp - (maxAmp-minAmp)*maxFrac
  print( "vmin: {}, vmax: {}".format(vMin,vMax) )
  #axSqw.pcolormesh(SofQW,cmap='Greys',vmin=vMin,vmax=vMax)

def formatAxes(info,axSq,axSqw,SofQmax):
  #trackLength = info["trackLength"]
  #fftSamples = info["fftSamples"]

  # X-axes
  axSq.set_xlim(0, info['nCols'])
#  if currentDataPath in [jams_path9,phi_path9]: axSq.set_ylim([0,SofQmax*0.6])
#  elif currentDataPath in [phi_path5]: axSq.set_ylim([0,SofQmax*2])
#  else:  axSq.set_ylim([0,SofQmax*1.2])

  axSq.xaxis.set_major_locator(plt.MultipleLocator((trackLength/2)/4))
  axSq.set_xticklabels('')

  axSqw.set_xlim(0, info['nCols'])
  axSqw.xaxis.set_major_locator(plt.MultipleLocator((trackLength/2)/4))
  axSqw.xaxis.set_major_formatter(plt.FuncFormatter(format_Q))

  # Y-axes
#  axSq.yaxis.set_major_locator(plt.MultipleLocator(SofQmax/3))
#  axSq.yaxis.set_major_formatter(plt.FuncFormatter(format_Wsum))
#  axSq.set_yticklabels('')
  axSq.yaxis.labelpad = 28

  axSqw.set_ylim(0, info['nRows'])
  axSqw.yaxis.set_major_locator(plt.MultipleLocator((fftSamples*2)/4))
  axSqw.yaxis.set_major_formatter(plt.FuncFormatter(format_W))
  axSqw.yaxis.labelpad = -10
  for tick in axSqw.get_yticklabels():
    tick.set_rotation(35)

  axSq.grid(b=True,which='major',axis='x',color='k', linestyle='dotted')
  axSqw.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(info,axSq,axSqw):
  #axSq.set_ylabel("S(q) (1e{})".format(int(np.log10(info['SofQdivisor']))))
  axSq.set_ylabel("ln(S(q))")
  
  axSqw.set_xlabel("Wave Vector, q")
  axSqw.set_ylabel(r"Freq, $\omega$")

def format_Q(value, tick_number):
  global trackLength
  # find number of multiples of pi/2
  fracOfPi = (value/trackLength)*2
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"$\pi/8$"
  elif N == 2:
    return r"$\pi/4$"
  elif N == 4:
    return r"$\pi/2$"
  elif N == 8:
    return r"$\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_W(value, tick_number):
  global fftSamples
  # find number of multiples of pi/2
  fracOfPi = (value/fftSamples)-1
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"${\pi}/{8}$"
    #return r"$\frac{\pi}{8}$"
  elif N == -1:
    return r"$-{\pi}/{8}$"
    #return r"$-\frac{\pi}{8}$"
  elif N == 2:
    return r"${\pi}/{4}$"
    #return r"$\frac{\pi}{4}$"
  elif N == -2:
    return r"$-\pi/4$"
  elif N == 4:
    return r"${\pi}/{2}$"
    #return r"$\frac{\pi}{2}$"
  elif N == -4:
    return r"$-{\pi}/{2}$"
    #return r"$-\frac{\pi}{2}$"
  elif N == 8:
    return r"$\pi$"
  elif N == -8:
    return r"$-\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_Wsum(value, tick_number):
  if value == 0:
    return "0"
  else:
    return "{:.1f}".format(value)

def save_plot(fig,info):
  directory = "./temp_plots/"
  plt.savefig(directory+"{}.png".format(fileName),dpi=300)
  metaFileName = directory+"{}_metadata.txt".format(fileName)
  json.dump(info, open(metaFileName,'w'))
  with open(metaFileName, "a") as myfile:
      myfile.write("\n"+currentDataPath)
  

def plot_Sq_and_Sqw(info,SofQW,SofQ):

  set_style()
  fig,axSq,axSqw = prepareFigure()
  makePlots(axSq,axSqw,SofQ,SofQW)
  formatAxes(info,axSq,axSqw,np.max(SofQ))
  formatLabels(info,axSq,axSqw)
  plt.tight_layout()
  plt.subplots_adjust(hspace=0.2)

  return fig



#############
if __name__ == "__main__":
  main()


