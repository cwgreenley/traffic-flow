# Built-in python libraries
import sys
import os
import csv
import scipy.signal as sig

import itertools

# 3rd-party libraries I'll be using
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import json


dataFiles = [
### 2018-07 ###

##"/shared/home/chris/201807_data/11/vel5_density0.131_fft16384_track8192_slowP10_runs5.00e+03_numCars1073_sampleRate1_numDataPoints8.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.133_fft16384_track8192_slowP10_runs5.00e+03_numCars1089_sampleRate1_numDataPoints8.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.134_fft16384_track8192_slowP10_runs5.00e+03_numCars1097_sampleRate1_numDataPoints9.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel5_density0.136_fft16384_track8192_slowP10_runs5.00e+03_numCars1114_sampleRate1_numDataPoints9.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.137_fft16384_track8192_slowP10_runs7.00e+03_numCars1122_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.140_fft16384_track8192_slowP10_runs7.00e+03_numCars1146_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.143_fft16384_track8192_slowP10_runs7.00e+03_numCars1171_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.146_fft16384_track8192_slowP10_runs7.00e+03_numCars1196_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.149_fft16384_track8192_slowP10_runs7.00e+03_numCars1220_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.151_fft16384_track8192_slowP10_runs7.00e+03_numCars1236_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.154_fft16384_track8192_slowP10_runs7.00e+03_numCars1261_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel5_density0.157_fft16384_track8192_slowP10_runs7.00e+03_numCars1286_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",


#"/shared/home/chris/201712_data/part2/vel9_density0.074_fft16384_track8192_slowP10_runs5.00e+03_numCars606_numDataPoints5.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
##"/shared/home/chris/201807_data/11/vel9_density0.078_fft16384_track8192_slowP10_runs5.01e+03_numCars638_sampleRate1_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.079_fft16384_track8192_slowP10_runs5.00e+03_numCars647_sampleRate1_numDataPoints5.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/11/vel9_density0.080_fft16384_track8192_slowP10_runs5.00e+03_numCars655_sampleRate1_numDataPoints5.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.081_fft16384_track8192_slowP10_runs7.00e+03_numCars663_sampleRate1_numDataPoints7.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.083_fft16384_track8192_slowP10_runs7.01e+03_numCars679_sampleRate1_numDataPoints7.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08301_fft16384_track8192_slowP10_runs5.00e+03_numCars680_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08313_fft16384_track8192_slowP10_runs5.00e+03_numCars681_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08325_fft16384_track8192_slowP10_runs5.00e+03_numCars682_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08337_fft16384_track8192_slowP10_runs5.00e+03_numCars683_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08350_fft16384_track8192_slowP10_runs5.00e+03_numCars684_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08362_fft16384_track8192_slowP10_runs5.00e+03_numCars685_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08374_fft16384_track8192_slowP10_runs5.00e+03_numCars686_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08386_fft16384_track8192_slowP10_runs5.00e+03_numCars687_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08398_fft16384_track8192_slowP10_runs5.00e+03_numCars688_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08411_fft16384_track8192_slowP10_runs5.00e+03_numCars689_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08423_fft16384_track8192_slowP10_runs5.00e+03_numCars690_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08435_fft16384_track8192_slowP10_runs5.00e+03_numCars691_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08447_fft16384_track8192_slowP10_runs5.00e+03_numCars692_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08459_fft16384_track8192_slowP10_runs5.00e+03_numCars693_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
##error "/shared/home/chris/201807_data/20/vel9_density0.08472_fft16384_track8192_slowP10_runs5.00e+03_numCars694_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/20/vel9_density0.08484_fft16384_track8192_slowP10_runs5.00e+03_numCars695_sampleRate1_numDataPoints5.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.085_fft16384_track8192_slowP10_runs7.00e+03_numCars696_sampleRate1_numDataPoints8.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.087_fft16384_track8192_slowP10_runs7.01e+03_numCars712_sampleRate1_numDataPoints8.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.088_fft16384_track8192_slowP10_runs7.01e+03_numCars720_sampleRate1_numDataPoints8.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.090_fft16384_track8192_slowP10_runs7.00e+03_numCars737_sampleRate1_numDataPoints8.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/08/vel9_density0.092_fft16384_track8192_slowP10_runs7.01e+03_numCars753_sampleRate1_numDataPoints8.6e+10_pad2_halfdata_NoNorm_hamming_.csv",


#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate100_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate10_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.126_fft16384_track8192_slowP10_runs3.00e+03_numCars1032_sampleRate1_numDataPoints5.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate100_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate10_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.140_fft16384_track8192_slowP10_runs3.00e+03_numCars1146_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate100_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate10_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel5_density0.154_fft16384_track8192_slowP10_runs3.00e+03_numCars1261_sampleRate1_numDataPoints6.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate100_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate10_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.074_fft16384_track8192_slowP10_runs3.00e+03_numCars606_sampleRate1_numDataPoints3.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate100_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate10_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.083_fft16384_track8192_slowP10_runs3.00e+03_numCars679_sampleRate1_numDataPoints3.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate100_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate10_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201807_data/05/vel9_density0.091_fft16384_track8192_slowP10_runs3.00e+03_numCars745_sampleRate1_numDataPoints3.7e+10_pad2_halfdata_NoNorm_hamming_.csv",

### 2018-06 ###
# Vel 5

#Varying samplerate
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate1_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate8_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate16_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate32_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate1_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate8_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate16_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate32_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate10_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.028_fft16384_track8192_slowP10_runs5.01e+03_numCars229_sampleRate100_numDataPoints1.9e+10_pad2_halfdata_NoNorm_hamming_.csv",

#varying slowdown probability
#"/shared/home/chris/201806_data/vel5_density0.015_fft16384_track8192_slowP2_runs1.01e+04_numCars122_sampleRate1_numDataPoints2.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel5_density0.030_fft16384_track8192_slowP2_runs1.00e+04_numCars245_sampleRate1_numDataPoints4.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP5_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel5_density0.035_fft16384_track8192_slowP10_runs5.01e+03_numCars286_numDataPoints2.3e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP20_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel5_density0.032_fft16384_track8192_slowP20_runs5.00e+03_numCars262_numDataPoints2.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

# Vel 9

#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate1_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate8_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate16_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate32_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate1_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate8_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate16_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201806_data/vel9_density0.017_fft16384_track8192_slowP10_runs5.01e+03_numCars139_sampleRate32_numDataPoints1.1e+10_pad2_halfdata_NoNorm_hamming_.csv",

#"/shared/home/chris/201712_data/part2/vel5_density0.112_fft16384_track8192_slowP10_runs5.00e+03_numCars917_numDataPoints7.5e+10_pad2_halfdata_NoNorm_hamming_.csv",
#"/shared/home/chris/201712_data/part2/vel9_density0.066_fft16384_track8192_slowP10_runs5.01e+03_numCars540_numDataPoints4.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
]

markers = itertools.cycle(('o', 'v', 's','*', '^', 'p',  'h', '<', 'H', 'D', '>','d'))
lineSty = itertools.cycle(([18,4],[4,2],[10,2,3,2],[8,2,3,2,3,2]))
colors = itertools.cycle(('k', 'g', 'r', 'b','brown','c', 'm', 'y','darksalmon'))

savePlot = True
plotType = "JamPeak"
q_scaling = False

if plotType=="varV":
  dataFiles = [
  "/shared/home/chris/201806_data/vel5_density0.014_fft16384_track8192_slowP10_runs5.03e+03_numCars114_sampleRate1_numDataPoints9.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate1_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  print(plotType)
  saveFileName = "lifetime_plot_varying_vmax_lowDensity"
  skipNum = 50
  legend = True
  xlimits = (0,None)
  ylimits = (0,None)

elif plotType=="midD":
  dataFiles = [
  "/shared/home/chris/201712_data/part2/vel9_density0.066_fft16384_track8192_slowP10_runs5.01e+03_numCars540_numDataPoints4.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201806_data/vel9_density0.008_fft16384_track8192_slowP10_runs5.04e+03_numCars65_sampleRate1_numDataPoints5.4e+09_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  print(plotType)
  saveFileName = "lifetime_plot_midRange_density"
  skipNum = 70
  legend = False
  xlimits = (0,None)
  ylimits = (0,None)

elif plotType=="maxLifetime":
  print(plotType)
  saveFileName = "lifetime_plot_maxLifetime"
  skipNum = 1
  legend = True
  xlimits = (.5,None)
  ylimits = (0,None)

elif plotType=="varSR":
  print(plotType)
  saveFileName = "lifetime_plot_varying_samplerate"
  skipNum = 50 
  legend = True
  xlimits = (None,None)
  ylimits = (None,None)

elif plotType=="varP":
  dataFiles = [
  "/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP20_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP10_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201712_data/part2/vel5_density0.021_fft16384_track8192_slowP5_runs5.00e+03_numCars172_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201806_data/vel5_density0.015_fft16384_track8192_slowP2_runs1.01e+04_numCars122_sampleRate1_numDataPoints2.0e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  print(plotType)
  saveFileName = "lifetime_plot_varying_slowp_lowDensity"
  skipNum = 50 
  legend = True
  xlimits = (0,None)
  ylimits = (0,None)

elif plotType=="freePeak":
  dataFiles5 = [
  "/shared/home/chris/201807_data/08/vel5_density0.157_fft16384_track8192_slowP10_runs7.00e+03_numCars1286_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/08/vel5_density0.143_fft16384_track8192_slowP10_runs7.00e+03_numCars1171_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/11/vel5_density0.131_fft16384_track8192_slowP10_runs5.00e+03_numCars1073_sampleRate1_numDataPoints8.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles9 = [
  "/shared/home/chris/201807_data/08/vel9_density0.087_fft16384_track8192_slowP10_runs7.01e+03_numCars712_sampleRate1_numDataPoints8.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/20/vel9_density0.08325_fft16384_track8192_slowP10_runs5.00e+03_numCars682_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/11/vel9_density0.078_fft16384_track8192_slowP10_runs5.01e+03_numCars638_sampleRate1_numDataPoints5.2e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles = dataFiles9
  print(plotType)
  saveFileName = "lifetime_plot_JamDensities_FreePeak"
  skipNum = 30 
  legend = True
  xlimits = (0,1)
  ylimits = (0,None)

elif plotType=="JamPeak":
  dataFiles5 = [
  "/shared/home/chris/201807_data/08/vel5_density0.140_fft16384_track8192_slowP10_runs7.00e+03_numCars1146_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/08/vel5_density0.143_fft16384_track8192_slowP10_runs7.00e+03_numCars1171_sampleRate1_numDataPoints1.3e+11_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/08/vel5_density0.146_fft16384_track8192_slowP10_runs7.00e+03_numCars1196_sampleRate1_numDataPoints1.4e+11_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/08/vel5_density0.157_fft16384_track8192_slowP10_runs7.00e+03_numCars1286_sampleRate1_numDataPoints1.5e+11_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles9 = [
  "/shared/home/chris/201807_data/20/vel9_density0.08313_fft16384_track8192_slowP10_runs5.00e+03_numCars681_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/20/vel9_density0.08362_fft16384_track8192_slowP10_runs5.00e+03_numCars685_sampleRate1_numDataPoints5.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/08/vel9_density0.092_fft16384_track8192_slowP10_runs7.01e+03_numCars753_sampleRate1_numDataPoints8.6e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles = dataFiles5
  print(plotType)
  saveFileName = "lifetime_plot_JamDensities_JamPeak"
  skipNum = 10
  legend = True
  xlimits = (0,.3)
  ylimits = (0,None)

elif plotType=="SizeEffectsFree":
  dataFiles5 = [
  "/shared/home/chris/201807_data/25/vel5_density0.14023_fft4096_track8192_slowP10_runs1.50e+03_numCars1149_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel5_density0.13942_fft4096_track16384_slowP10_runs1.50e+03_numCars2284_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel5_density0.13893_fft4096_track32768_slowP10_runs1.50e+03_numCars4552_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles9 = [
  "/shared/home/chris/201807_data/25/vel9_density0.08335_fft4096_track8192_slowP10_runs1.50e+03_numCars683_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel9_density0.08254_fft4096_track16384_slowP10_runs1.50e+03_numCars1352_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel9_density0.08205_fft4096_track32768_slowP10_runs1.50e+03_numCars2689_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles = dataFiles9
  print(plotType)
  saveFileName = "lifetime_plot_FiniteSizeEffect_FreePeak"
  skipNum = 1
  legend = True
#  xlimits = (0,1)
  xlimits = (None,None)
#  ylimits = (0,None)
  ylimits = (None,None)

elif plotType=="SizeEffectsJam":
  dataFiles5 = [
  "/shared/home/chris/201807_data/25/vel5_density0.14023_fft4096_track8192_slowP10_runs1.50e+03_numCars1149_sampleRate1_numDataPoints7.1e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel5_density0.13942_fft4096_track16384_slowP10_runs1.50e+03_numCars2284_sampleRate1_numDataPoints1.4e+10_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel5_density0.13893_fft4096_track32768_slowP10_runs1.50e+03_numCars4552_sampleRate1_numDataPoints2.8e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles9 = [
  "/shared/home/chris/201807_data/25/vel9_density0.08335_fft4096_track8192_slowP10_runs1.50e+03_numCars683_sampleRate1_numDataPoints4.2e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel9_density0.08254_fft4096_track16384_slowP10_runs1.50e+03_numCars1352_sampleRate1_numDataPoints8.3e+09_pad2_halfdata_NoNorm_hamming_.csv",
  "/shared/home/chris/201807_data/25/vel9_density0.08205_fft4096_track32768_slowP10_runs1.50e+03_numCars2689_sampleRate1_numDataPoints1.7e+10_pad2_halfdata_NoNorm_hamming_.csv",
  ]
  dataFiles = dataFiles9
  print(plotType)
  saveFileName = "lifetime_plot_FiniteSizeEffect_JamPeak"
  skipNum = 10
  legend = True
  xlimits = (0,.3)
  ylimits = (0,None)

else:
  skipNum = 10
  legend = True
  xlimits = (0,None)
  ylimits = (0,None)



def main():

  fig,ax = prepare_plot_canvas()
  metaInfo = range(len(dataFiles))

  for f,filePath in enumerate(dataFiles):
    info,headers,data = importData(filePath)
    metaInfo[f] = info
    data = prepareData(info,data)
    plot_Sq_and_Sqw(info,data,ax)
  formatAxes(info,data,ax)

  if(savePlot): save_plot(fig,metaInfo)
  plt.show()
  #return info, SofQW, SofQ

###################################################################

def importData(fileName):
  if plotType in ["JamPeak","SizeEffectsJam"]:
    fileNameModifier = "_JamPeakData.csv"
  else:
    fileNameModifier = "_peakData.csv"

  with open(fileName+fileNameModifier,'rb') as file:
    data = csv.reader(file,delimiter=",")

    infoArray = data.next()
    headerArray = data.next()

    info = {
            "numCars"     : int(infoArray[1]),
            "maxVel"      : int(infoArray[3]),
            "trackLength" : int(infoArray[5]),
            "sampleRate" : int(infoArray[7]),
            "fftSamples"  : int(infoArray[11]),
            "fftRuns"     : int(infoArray[13]),
            "slowP"       : int(infoArray[17]),
            "fftPadding"  : int(infoArray[19]),
            "q_scaling"   : q_scaling
           }
    info['density'] =  float(info['numCars'])/float(info['trackLength'])
    dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])
    info['fracCritDen'] = info['density']/dCrit

    maxQ = int(info['trackLength']/2) #max possible Q, may be smaller

    #Prepare numpy array for holding data
    dataShape = [maxQ, 3]
    dataArray=np.empty(dataShape,dtype=float)
    dataArray.fill(np.NAN)

    nRows = 0
    for i,row in enumerate(data):
      dataArray[i] = row
      nRows += 1
     
    info['nRows'] = nRows

  return info,headerArray,dataArray


def prepareData(info,data):
  if   plotType=="varV":            maxQ = info['trackLength']/4
  elif plotType=="midD":            maxQ = info['trackLength']/2
  elif plotType=="maxLifetime":     maxQ = info['trackLength']/(2*info['maxVel'])
  elif plotType=="varSR":           maxQ = info['trackLength']/4
  elif plotType=="varP":            maxQ = info['trackLength']/4
  elif plotType=="freePeak":        maxQ = info['trackLength']/(info['maxVel'])
  elif plotType=="JamPeak":         maxQ = info['trackLength']/(info['maxVel'])
  elif plotType=="SizeEffectsFree": maxQ = 2*info['trackLength']/(info['maxVel'])
  elif plotType=="SizeEffectsJam":  maxQ = info['trackLength']/(2*info['maxVel'])
  else:                             maxQ = info['trackLength']


  if q_scaling: 
    skips = 100/info['maxVel']
  else: 
    skips = skipNum

  skips = skips*info['trackLength']/8192

  data[:,1] = sig.medfilt(data[:,1],3)
  data = cleanData(info,data)
  data = data[0:maxQ:skips]

  return data

def cleanData(info,data):
  startQ = 60
  ## Make sure the starting value is a valid number
  goodStart = 0
  while(goodStart==0):
    if np.isnan(data[startQ][1]):
      startQ -= 1
    else:
      goodStart = 1

  lastGood = startQ
  for q in range(startQ,0,-1):
    if np.isnan(data[q-1][1]): continue
    thisVal = data[lastGood][1]
    nextVal = data[q-1][1]
    if(nextVal < 0.9*thisVal): ##Too much change indicates bad data
      data[q-1][1] = np.nan
      data[q-1][2] = np.nan
    else:
      lastGood = q-1

  return data

##################################################################
def prepare_plot_canvas():
  set_style()
  fig   = plt.figure()
  ax  = plt.subplot2grid((1,1), (0,0))
  return fig,ax
  
def set_style():
#  sns.set()

  singleColWidth_mm = 89.0
  singleColWidth_inch = singleColWidth_mm/25.4
  singleColHeight_inch = singleColWidth_inch#/1.6

  params = {
    'axes.labelsize': 11,
    'font.size': 8,
    'legend.fontsize': 11,
    'xtick.labelsize': 13,
    'ytick.labelsize': 13,
    'text.usetex': False,
    'lines.markersize': 5,
    'lines.linewidth': 2.0,
    'figure.figsize': [singleColWidth_inch,singleColHeight_inch]
  }
  plt.style.use('classic')
#  plt.style.use(['seaborn-white', 'seaborn-paper'])
#  sns.set(context='paper',rc=params)
  mpl.rcParams.update(params)
##  matplotlib.rc("font", family="Times New Roman")

def plot_Sq_and_Sqw(info,data,ax):

  makePlots(ax,info,data)
  formatLabels(info,ax)
  plt.tight_layout()
  if legend: 
    if plotType in ["freePeak","JamPeak","SizeEffectsFree","SizeEffectsJam"]:
      plt.legend(loc="upper left",numpoints=2,ncol=1)
    else:
      plt.legend(loc="upper left",numpoints=1,ncol=1)


def makePlots(ax,info,data):
  if q_scaling:
    qVals = [q*info['maxVel']/(4*np.pi) for q in data[:,0]]
  else:
    qVals = data[:,0]
  HWHM = data[:,1]
  AREA = data[:,2]
  m = markers.next()
  l = lineSty.next()
  c = colors.next()
  dCrit = criticalDensity(info['maxVel'],info['slowP'],info['trackLength'])

  if plotType=="varV":
    legendText = r'V$_{{\mathrm{{max}}}}$ {}'.format(info['maxVel'])
    #legendText = "Cars: {}".format(info['numCars'])
    ax.plot((qVals**2),(HWHM),linestyle='',marker=m,color=c,label=legendText)

  elif plotType=="midD":
    legendText = "{:.2f}".format(info['fracCritDen'])
    spacing = 2.0*np.pi/float(info['maxVel'])
    ax.plot((qVals/spacing),(HWHM),linestyle=l,marker='',color=c,label=legendText)

  elif plotType=="maxLifetime":
    if info['trackLength'] == 8192: c = 'k'
    if info['trackLength'] == 16384: c = 'b'
    if info['trackLength'] == 32768: c = 'r'
    plotQ = int(info['trackLength']/(2*info['maxVel']*1.36))
    ax.plot(info['fracCritDen'],HWHM[plotQ],linestyle='-',marker='o',color=c)

  elif plotType=="varSR":
    legendText = "samplerate: {}".format(info['sampleRate'])
    ax.plot(np.log(qVals),np.log(HWHM),linestyle='',marker=m,color=c,label=legendText)

  elif plotType=="varP":
    legendText = "{:.2f}".format(info['slowP']/100.0)
    ax.plot((qVals**2),(HWHM),linestyle='',marker=m,color=c,label=legendText)

  elif plotType in ["freePeak","JamPeak"]:
    legendText = '$\mathrm{{d/d}}_c=${:.3f}'.format(info['fracCritDen'])
    spacing = 2.0*np.pi/float(info['maxVel'])
    ax.plot((qVals/spacing),(HWHM),dashes=l,marker='',markeredgecolor=c,color=c,label=legendText)

  elif plotType in ["SizeEffectsFree","SizeEffectsJam"]:
    legendText = "{}".format(info['trackLength'])
    spacing = 2.0*np.pi/float(info['maxVel'])
    ax.loglog((qVals/spacing),(HWHM),linestyle='',marker=m,markeredgecolor=c,color=c,label=legendText)


def criticalDensity(vmax,slowp,trackLen):
  slowp = slowp/100.0
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*0.963

def formatAxes(info,data,ax):
  # X-axes
  if plotType in [""]:
    ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi/8))
    ax.xaxis.set_major_formatter(plt.FuncFormatter(format_Q_rad))
  elif plotType in ["freePeak","SizeEffectsFree"]:
    ax.xaxis.set_major_locator(plt.MultipleLocator(.2))
  elif plotType in ["JamPeak","SizeEffectsJam"]:
    ax.xaxis.set_major_locator(plt.MultipleLocator(.1))
  elif plotType in ["midD"]:
    ax.xaxis.set_major_locator(plt.MultipleLocator(1))
  elif plotType=="varSR":
    ax.xaxis.set_major_locator(plt.MultipleLocator(2))
  ax.set_xlim(xlimits)

  # Y-axes
  ax.set_ylim(ylimits)
  #axSq.yaxis.set_major_locator(plt.MultipleLocator(SofQmax/3))
  #axSq.yaxis.set_major_formatter(plt.FuncFormatter(format_Wsum))
  #axSq.set_yticklabels('')
  #axSq.yaxis.labelpad = 28

  #axSqw.set_ylim(0, info['nRows'])
  #axSqw.yaxis.set_major_locator(plt.MultipleLocator((fftSamples*2)/4))
  #axSqw.yaxis.set_major_formatter(plt.FuncFormatter(format_W))
  #axSqw.yaxis.labelpad = -10
  #for tick in axSqw.get_yticklabels():
  #  tick.set_rotation(35)

  ax.grid(b=True,which='major',color='k', linestyle='dotted')

def formatLabels(info,ax):
  if plotType=="varSR":
    ax.set_xlabel("ln(q)")
    ax.set_ylabel("ln(HWHM)")

  elif plotType in ["varV","varP"]:
    ax.set_xlabel(r'$q^2$')
    ax.set_ylabel("HWHM")

  elif plotType in ["midD","freePeak","JamPeak","SizeEffectsFree","SizeEffectsJam"]:
    ax.set_xlabel(r"qV$_{\mathrm{max}}$/(2$\pi$)")
    ax.set_ylabel("HWHM")

  elif plotType=="maxLifetime":
    ax.set_xlabel("Fraction of Critical density")
    ax.set_ylabel("First lifetime peak")

  else:
    ax.set_xlabel("Wave Vector, q (rad)")
    ax.set_ylabel("HWHM")

def format_Q_scaling(value, tick_number):
  # find number of multiples of pi/maxVel 
  frac = (value/(np.pi/maxVel))
  N = int(np.round( frac )) 
  if N == 0:
    return "0"
  elif N==1:
    return r"$\pi/v_{max}$"
  else:
    return r"${0}\pi/v_{{max}}$".format(N)

def format_Q_rad(value, tick_number):
  # find number of multiples of pi/2
  fracOfPi = (value/np.pi)
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"$\pi/8$"
  elif N == 2:
    return r"$\pi/4$"
  elif N == 4:
    return r"$\pi/2$"
  elif N == 8:
    return r"$\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_W(value, tick_number):
  # find number of multiples of pi/2
  fracOfPi = (value/fftSamples)-1
  N = int(np.round( fracOfPi*8 )) #find multiple of pi/8
  if N == 0:
    return "0"
  elif N == 1:
    return r"${\pi}/{8}$"
    #return r"$\frac{\pi}{8}$"
  elif N == -1:
    return r"$-{\pi}/{8}$"
    #return r"$-\frac{\pi}{8}$"
  elif N == 2:
    return r"${\pi}/{4}$"
    #return r"$\frac{\pi}{4}$"
  elif N == -2:
    return r"$-\pi/4$"
  elif N == 4:
    return r"${\pi}/{2}$"
    #return r"$\frac{\pi}{2}$"
  elif N == -4:
    return r"$-{\pi}/{2}$"
    #return r"$-\frac{\pi}{2}$"
  elif N == 8:
    return r"$\pi$"
  elif N == -8:
    return r"$-\pi$"
  elif N % 2 > 0:
    return r"${0}\pi/8$".format(N)
  elif N % 8 == 0:
    return r"$(0)\pi$".format(N // 8)
  elif N % 4 == 0:
    return r"$(0)\pi/2$".format(N // 4)
  else:
    return r"${0}\pi/4$".format(N // 2)

def format_Wsum(value, tick_number):
  if value == 0:
    return "0"
  else:
    return "{:.1f}".format(value)


def save_plot(fig,metaInfo):
  global saveFileName
  directory = "./temp_plots/"
  if(plotType in ['freePeak','JamPeak','SizeEffectsFree','SizeEffectsJam']): 
    saveFileName = saveFileName+"_vmax{:d}".format(metaInfo[0]['maxVel'])
  plt.savefig(directory+"{}.png".format(saveFileName),dpi=300)
  plt.savefig(directory+"{}.pdf".format(saveFileName))
  metaFileName = directory+"{}_metadata.txt".format(saveFileName)
  with open(metaFileName, "w") as myfile:
      for f,filePath in enumerate(dataFiles):
        json.dump(metaInfo[f], myfile)
        myfile.write("\n")
        myfile.write(filePath+"\n\n")
  

###################################################################

def printVersions():
  print("Python version:\n{}\n".format(sys.version))
  print("matplotlib version: {}".format(matplotlib.__version__))
  print("seaborn version: {}".format(sns.__version__))
  print("pandas version: {}".format(pd.__version__))
  print("numpy version: {}".format(np.__version__))


#############
if __name__ == "__main__":
  main()


