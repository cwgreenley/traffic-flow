//
//
//gcc simulation.c -lfftw3 -lm -o sim.exe
//
//
//#include <complex.h>
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define PI 3.1415926535897932384626433832795

////////////////////////////////////////////////////////////////////
///// FUNCTION DEFINITIONS
////////////////////////////////////////////////////////////////////
int phiBool(int i, int *posArray);
void storeTrackState(int *posArray,double *trackState);
void fft(fftw_complex *fftOutput,fftw_plan plan_fwd);
void runSimulation(int *posArray, int *velArray);
void updateCar(int *pos1, int *vel1, int *pos2);
void delay(int milliseconds);
int boolCheck(double input);

////////////////////////////////////////////////////////////////////
////// DEFINE GLOBAL PARAMETERS
////////////////////////////////////////////////////////////////////

//Passed in as parameters
int numSamples; //Number of steps for which data is recorded
int trackLength;
int stepSize; //Time steps between FFTs
double density;
int maxVel;
int p; //percent chance of slowdown

//calculated or set in code
int numCars;
int dataStartStep = 1000000;//10000000;
int numSteps; //Total number of steps including thermalizing 
char *fileName; //filename string
//FILE *outputFile;


int main(int argc, char *argv[])
{


if (argc != 8)
{
  printf("Usage: %s <numSamples> <trackLength> <density> <maxVel> <slowPercent> <save directory> <\"notes\">\n",argv[0]);
  return(-1);
}

//First argument
numSamples = atof(argv[1]);

//Second argumnet
trackLength = atoi(argv[2]);

//Third argument
density = atof(argv[3]);
numCars = trackLength*density;

//Fourth argument
maxVel = atoi(argv[4]);

//Fifth argument
p = atoi(argv[5]);

numSteps = numSamples;//*(stepSize); //to run after thermalization, fft if(currentStep%stepSize==0)


//Create filename to save to
asprintf(&fileName, "%svel_%d_density_%.3f_numSamples_%d_track_%d_slowP_%d_numCars_%d_%s.csv",\
                        argv[6],maxVel,density,numSamples,trackLength,p,numCars,argv[7]);
                        
printf("%s\n",fileName);

//outputFile = fopen(fileName,"w");

//if (outputFile == 0)
//{
//  printf("Could not open file %s.\n",argv[2]);
//  return(-1);
//}
//fprintf(outputFile,"numCars,%d,maxVel,%d,trackLength,%d,numSamples,%d,stepSize,%d,dataStartStep,%d,slowP,%d,%s\n",\
//                   numCars,  maxVel,   trackLength,    numSamples,   stepSize,   dataStartStep,    p,    argv[8]);
//fprintf(outputFile,"#Re[Q1],Im[Q1],Re[Q5],Im[Q5],... for every 5th Q starting at Q=1\n");

//ALLOCATE DATA ARRAYS
int i,j,k;
int *posArray, *velArray;
posArray = malloc((numCars+2)*sizeof(int));
velArray = malloc((numCars+2)*sizeof(int));

srand (time(NULL));

//Initialize starting positions and speeds 
for (i=0;i<numCars;i++){
  posArray[i] = i*2;
  velArray[i] = 0;
}

//Setup DFT stuff
//double *trackState;
//fftw_complex *fftOutput;
//fftw_plan plan_fwd; //Note that this creates 'opaque pointers'
//int spaceLength = trackLength;
//int outputSL = spaceLength/2+1;
//trackState = fftw_malloc(sizeof(double)*spaceLength);
//fftOutput = fftw_malloc(sizeof(fftw_complex)*outputSL);
//
//int t0 = (int)time(NULL);
//plan_fwd = fftw_plan_dft_r2c_1d(spaceLength,trackState,fftOutput,FFTW_MEASURE);
//int t1 = (int)time(NULL);
//printf("Plan creation completed in %.2f minutes.\n",(t1-t0)/60.0);


//Initialization runs
int step;
for (step=0;step<dataStartStep;step++){
  runSimulation(posArray, velArray);
}
printf("Completed Initialization Runs\n");


int startTimeStamp = (int)time(NULL);
///////////////////////////////////////////////////////////
////// MAIN LOOP
///////////////////////////////////////////////////////////
//Data taking runs
//int fftNum=0;
//double priorAvgVel=0;
//double avgVel=0;
//double SigmaTerm1=0;
//double SigmaTerm2=0;
//double sigma;
//double a = maxVel-(double)p/100.0;
//double stepAvgVel;
//printf("a: %.3f\n",a);
double slowCars1=0;
double slowCars2=0;
for (step=0;step<numSteps;step++)
{
  //zero out array for holding track history
//  for(j=0;j<spaceLength;j++)
//    trackState[j]=0;
//  int velSum=0;


  runSimulation(posArray, velArray);
//  storeTrackState(posArray,trackState); //Store output of a given step
  for(i=0;i<numCars;i++){ //add up all the velocities
//    velSum += velArray[i];
    if(velArray[i]<(maxVel))
      if(velArray[i]==maxVel-1)
        slowCars1+=1;
      else
        slowCars2+=1;
  }
  

//  if((step+1)%stepSize==0){ //runs every stepSize steps
//    fft(fftOutput,plan_fwd);
//    fftNum++;
//
//    if((fftNum%10000) == 0){
//      printf("TotAvgVel: %.0f diff: %.0f velSum: %d fftNum: %d steps: %d  avgVel: %.5f SlowCars: %d\n",\
//              avgVel,avgVel-priorAvgVel,velSum,fftNum,step+1,avgVel/((double)(step+1)),slowCars);
//      priorAvgVel = avgVel;
//      int currentTimeStamp = (int)time(NULL);
//      int seconds = (currentTimeStamp - startTimeStamp);
//      int hours = seconds/60/60;
//      int minutes = (seconds/60)-(hours*60);
//      int remainingSeconds = ((double)seconds/fftNum)*(numSamples-fftNum);
//      printf("Completed %d of %d ffts in %d hrs %d min. Abt %.3f hrs remaining.\n",\
//            fftNum,numSamples,hours,minutes,remainingSeconds/60.0/60.0);
//    }
//  }

}//Completed all ffts
//avgVel = (sum(x_i-a)/N)+a
slowCars1 = slowCars1/(double)numSteps;
slowCars2 = slowCars2/(double)numSteps;
printf("SlowCars Vmax-1: %.5e\n",slowCars1);
printf("SlowCars Vmax-2 or slower: %.5e\n",slowCars2);
//avgVel = (SigmaTerm2/(double)numSteps)+a;
//sigma = sqrt((SigmaTerm1/(double)numSteps)-pow(SigmaTerm2/(double)numSteps,2));
//printf("Average Vel: %.10e +/- %.3e\n",avgVel,sigma);
//fprintf(outputFile,"AverageVel %.10e sigma %.1e\n",avgVel,sigma);

//int index;
//double avg;
////double scaleF = pow(fftSamples*spaceLength,-2.0);
//  //Print result of DFT
//  for (i=0;i<timeLength;i++){
//    for (j=0;j<outputSL;j++){
//      index = i*outputSL+j;
//      avg = (outputModSqr[index]/(double)fftRuns);
//      if(j<outputSL-1)
//        fprintf(outputFile,"%.5e,",avg);
//      else
//        fprintf(outputFile,"%.5e",avg);
//    }
//    fprintf(outputFile,"\n");
//  }


//fclose(outputFile);
//fftw_destroy_plan(plan_fwd);
//fftw_free(fftOutput);
//fftw_free(trackState);
free(posArray);
free(velArray);
//free(fileName);
}/////// END MAIN


/////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////

//Fill in array which stores the track state at each sample step
void storeTrackState(int *posArray,double *trackState)
{
  int i,t;
  int test = 0;//0:standard, 1:phi, else:testing

  //Standard
  if(test==0){
    for(i=0;i<numCars;i++){
      trackState[posArray[i]]=1;
    }
    //XXX printf("%d: ",fftCounter);
    //XXX for(i=0;i<trackLength;i++){
    //XXX   printf("%d",(int)ceil(track2D[i+fftCounter*trackLength]));
    //XXX }
    //XXX printf("\n");
    //XXX delay(100);
  }
//  //phi
//  else if(test==1){
//    for(i=0;i<numCars;i++){
//      if(phiBool(i,posArray)==1){
//        track2D[posArray[i]+fftCounter*trackLength]=Hamm(fftCounter);
//      }
//    }
//  }
//  //testing
//  else{
//    double sVal,wavelength,period;
//    wavelength = (double)trackLength/1.0;
//    period = (double)fftSamples/1.0;
//  
//    for(i=0;i<trackLength;i++){
//     sVal =  cos(2*PI*(3*i/wavelength + 1*fftCounter/period));
//     // sVal += cos(2*PI*(0*i/wavelength + 17*fftCounter/period));
//     // sVal += cos(2*PI*(1*i/wavelength + 0*fftCounter/period));
//     // sVal += cos(2*PI*(1*i/wavelength + 1*fftCounter/period));
//     // sVal += cos(2*PI*(3*i/wavelength + 0*fftCounter/period));
//     // sVal += cos(2*PI*(0*i/wavelength + 7*fftCounter/period));
//     // sVal += cos(2*PI*(5*i/wavelength + 9*fftCounter/period));
//     // sVal += cos(2*PI*(6*i/wavelength + 10*fftCounter/period));
//     // sVal += (rand()%100)/70;
//      track2D[fftCounter*trackLength+i]=sVal*Hamm(fftCounter);//(((double)(rand()%101))/100.0-0.5);//*Hamm(fftCounter);
//    }
//  }
}
////////// End storeTrackState


//void fft(fftw_complex *fftOutput,fftw_plan plan_fwd)
//{
//  fftw_execute(plan_fwd);
//
//  int i;
//  for(i=1;i<150;i=i+5){
//    if(i==1)
//      fprintf(outputFile,"%.10e %.10e",fftOutput[i][0],fftOutput[i][1]);
//    else
//      fprintf(outputFile," %.10e %.10e",fftOutput[i][0],fftOutput[i][1]);
//  }
//  fprintf(outputFile,"\n");
//}


///////////////////////////////////////////////////////////////
// Simulation wrapper
///////////////////////////////////////////////////////////////
void runSimulation(int *posArray, int *velArray)
{
  int j;

  posArray[numCars] = posArray[0];
  velArray[numCars] = velArray[0];

  for (j=0;j<numCars;j++){
    updateCar(&posArray[j],&velArray[j],&posArray[j+1]); 
  }
}


///////////////////////////////////////////////////////////////
// Update for one car
///////////////////////////////////////////////////////////////
void updateCar(int *pos1, int *vel1, int *pos2){

  int dist,newVel;
  dist = *pos2 - *pos1 - 1; //gap
  while(dist<0){dist = dist + trackLength;} //Account for wraparound
  
  
  //Assume speedup if possible
  if(*vel1+1 <= maxVel)
    newVel = *vel1+1;
  else
    newVel = *vel1;
  
  //Check for available space
  if (dist >= newVel){
    *vel1 = newVel; //Set new speed if enough distance
    }
  else{ //else set new speed to slow down and exactly close gap
    *vel1 = dist;
  }
  
  if (*vel1 > 0){
    if((rand()%100)+1 <= p)
      *vel1 = *vel1 - 1;
  }
  
  //update position based on new velocity //XXX + (trackLength-maxVel)
  *pos1 = (*pos1 + *vel1) % trackLength;


}


int boolCheck(double input){
  if(input==0){
    return 0;
  }
  else{
    return 1;
  }
}

//Calculate phi
int phiBool(int i, int *posArray)
{
  int d1, d2;

  posArray[numCars] = posArray[0];
  posArray[numCars+1] = posArray[1];

  d1 = posArray[i+1] - posArray[i];
  while(d1<0){d1 = d1 + trackLength;}
  d2 = posArray[i+2] - posArray[i+1];
  while(d2<0){d2 = d2 + trackLength;}

  if (d1<=maxVel/2 && d2<=maxVel/2)
    return 1;
  else
    return 0;
}


void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}
