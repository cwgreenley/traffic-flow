import numpy as np
import matplotlib.pyplot as plt


def dCrit(vmax,slowp,trackLen):
  a = 1-slowp
  b = a*(1 - (2*a*(1-a))/(2-a+np.sqrt((2-a)**2-4*(1-a)*a**3)) )
  critDen = b / (b+vmax-slowp) + 1.95*trackLen**-.76
  return critDen*.963


#plot=False
#
#if plot:
#  fig = plt.figure()
#  ax = fig.add_subplot(1,1,1)

maxV = np.array([9.0])
trackLength = np.array([8192,16384,32768,65536,131072])
fftSamples = np.array([512])
sampleRate = np.array([1])
fftAverages = 7500
slowP = np.array([.1])
dCritFrac = np.concatenate([
            np.arange(1.000,1.032,.004),
            np.arange(1.040,1.100,.010)])
#numCarArr = np.array([745])
#constDfrac = np.array([])

#ArraySize = len(maxV)*len(trackLength)*len(fftSamples)*len(sampleRate)*len(slowP)*len(numCarArr)
#densityArray = np.zeros(ArraySize)
#datumsArray = np.zeros(ArraySize)
#SlowPArray = np.zeros(ArraySize)

for mV in maxV:
  for tL in trackLength:
    for fS in fftSamples:
      for sR in sampleRate:
        for sP in slowP:
          for dF in dCritFrac:
          #for car in numCarArr:
            density = np.around(dF*dCrit(mV,sP,tL),5)
            #density = np.around(float(car)/tL,5)
            datums = fftAverages*(tL*density*fS)
            print "date;time ./SofQsim.exe {:d} {} {} {} {} {} ~/201808_data/03/ \"phi_SofQ\""\
                    .format(int(datums),tL,fS,density,int(mV),int(sP*100))
#            print "date;time ./SofQsim.exe {:d} {} {} {} {} {} {} ~/201808_data/03/ \"phi\""\
#                    .format(int(datums),tL,fS,sR,density,int(mV),int(sP*100))


##constnat P, varying density
#for i,p in enumerate(slowP):
#  for j,f in enumerate(dCritFrac):
#    index = i*len(dCritFrac)+j
#    d = np.around(f*dCrit(p,maxV),3)
#    densityArray[index] = d
#    datumsArray[index] = fftAverages*(trackLength*d*fftSamples)
#    SlowPArray[index] = p
#    if plot:
#      if index==0:ax.plot(p,d,linestyle='',marker='o',color='k',label="Simulations: Constant Slowdown")
#      else: ax.plot(p,d,linestyle='',marker='o',color='k')
#
##constant density, varying P
#for i,f in enumerate(constDfrac):
#  for j,p in enumerate(slowP):
#    index = i*len(slowP)+j+(len(slowP)*len(dCritFrac))
#    d = np.around(f*dCrit(slowP[-1],maxV),3)
#    densityArray[index] = d
#    datumsArray[index] = fftAverages*(trackLength*d*fftSamples)
#    SlowPArray[index] = p
#    if plot:
#      if i==0 and j==0:ax.plot(p,d,linestyle='',marker='*',color='k',label="Simulations: Constant Density")
#      else: ax.plot(p,d,linestyle='',marker='*',color='k')
#
#for i,d in enumerate(densityArray):
#  datums = datumsArray[i]
#  p = SlowPArray[i]
#  print "date;time ./simpad2_measure.exe {:d} {} {} {} {} {} {} ~/xmasData/ \"\"".format(
#      int(datums),trackLength,fftSamples,sampleRate,d,int(maxV),int(p*100))
#
#
#if plot:
#  px = np.arange(5,51)/100.0
#  criticalDensities = dCrit(px,maxV)
#  ax.plot(px,criticalDensities,linestyle='-',marker='',color='r',linewidth=3,label="Critical Density")
#  #ax.plot(SlowPArray,densityArray,linestyle='',marker='o',color='k',label="Simulations")
#  ax.set_title("Vmax = {:}".format(int(maxV)))
#  ax.set_xlabel('slowP')
#  ax.set_ylabel('density')
#  ax.set_xlim(0,.5)
#  ax.set_ylim(0,1.2*max(criticalDensities))
#  ax.legend(loc="upper right",shadow=True,fancybox=True,numpoints=1)
#  ax.grid(b=True,which='major')
#  
#  plt.show()
   
