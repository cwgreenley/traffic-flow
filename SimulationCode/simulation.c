//
//
//gcc simulation.c -lfftw3 -lm -o sim.exe
//
//
//#include <complex.h>
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define PI 3.1415926535897932384626433832795

////////////////////////////////////////////////////////////////////
///// FUNCTION DEFINITIONS
////////////////////////////////////////////////////////////////////
void distProbDistributionHist(int *posArray, int *velArray, int *distArray);
void asciiDisplay(int *posArray, int *velArray);
void trackDiffusion(int *posArray, int *velArray);
void pairCorrelation(int *posArray, int *velArray, int *distArray);
int phiBool(int i, int *posArray);
void storeTrackState(int *posArray,double *track2D,int fftCounter);
void fft(double *track2D,fftw_complex *fftOutput,double *outputModSqr,fftw_plan plan_fwd, int timeLength, int spaceLength);
void runSimulation(int *posArray, int *velArray);
void updateCar(int *pos1, int *vel1, int *pos2);
float welch(int timeStep);
float Hamm(int timeStep);
//void WindowReweight(double *track2D, int spaceLength, int timeLength);
void printTrack2D(double *track2D,int spaceLength, int fftSamples);
void delay(int milliseconds);
int boolCheck(double input);

////////////////////////////////////////////////////////////////////
////// DEFINE GLOBAL PARAMETERS
////////////////////////////////////////////////////////////////////

//Passed in as parameters
float numDataPoints=0;
int trackLength=0;
int fftSamples=0; //Time steps per FFT
float density=0;
int maxVel=0;
int p=0; //percent chance of slowdown
int sampleRate=0;

//calculated or set in code
int numCars=0;
int dataStartStep = 10000000;
int padding = 2;
//unsigned long long numSamples=0; //Number of steps for which data is recorded
//unsigned long long numSteps=0; //Total number of steps including thermalizing 
unsigned int fftRuns=0; //Number of matrices to do ffts of
char *fileName; //filename string
FILE *outputFile;


int main(int argc, char *argv[])
{


if (argc != 10)
{
  printf("Usage: %s <numDataPoints> <trackLength> <fftSamples> <sampleRate> <density> <maxVel> <slowPercent> <save directory> <\"notes\">\n",argv[0]);
  return(-1);
}
else
{
  //First argument
  numDataPoints = atof(argv[1]);
  
  //Second argumnet
  trackLength = atoi(argv[2]);

  //Third argument
  fftSamples = atoi(argv[3]);
  
  //Fourth argument
  sampleRate = atoi(argv[4]);

  //Fifth argument
  density = atof(argv[5]);
  numCars = trackLength*density+0.5;

  //Sixth argument
  maxVel = atoi(argv[6]);
  
  //Seventh argument
  p = atoi(argv[7]);
  
  fftRuns = numDataPoints/(numCars*fftSamples);
  //numSamples = fftRuns*fftSamples;
  //numSteps = (numSamples*sampleRate)+dataStartStep;
  //printf("numSamples: %d\n",numSamples);
  //printf("numSteps: %d\n",numSteps);


  if (fftRuns < 100)
  {
    printf("Don't perform fewer than 100 fftRuns");
    return(-1);
  }

  //Create filename to save to
  asprintf(&fileName, "%svel%d_density%.5f_fft%d_track%d_slowP%d_runs%.2e"
                          "_numCars%d_sampleRate%d_numDataPoints%.1e_pad%d_halfdata_NoNorm_hamming_%s.csv",\
                          argv[8],maxVel,density,fftSamples,trackLength,p,\
                          (float)fftRuns,numCars,sampleRate,numDataPoints,padding,argv[9]);
  printf("%s\n",fileName);
  
  outputFile = fopen(fileName,"w");

  if (outputFile == 0)
  {
    printf("Could not open file %s.\n",fileName);
    return(-1);
  }

  //if (!(numSteps > sampleRate*2))
  //{
  //  printf("Number of steps not a valid entry," 
  //  "or not more than twice the sample rate of %d.\n",sampleRate);
  //  return(-1);
  //}
}

//Check that the number of samples for each fft is equal to or less than
//the total number of samples to be taken. 
//if(fftSamples > numSamples)
//{printf("fftSamples must be less than or equal to numSamples\n");return(-1);}
//Also check that numSamples is an integer multiple of fftSamples
//if(!(numSamples%fftSamples==0))
//{printf("numSamples must be an integer multiple of fftSamples\n");return(-1);}
//And check that fftSamples is an integer multiple of sampleRate
//if(!(fftSamples%sampleRate==0))
//{printf("fftSamples must be an integer multiple of sampleRate\n");return(-1);}


//ALLOCATE DATA ARRAYS
int i,j,k,fftRun,step;
int *posArray, *velArray;
posArray = malloc((numCars+2)*sizeof(int));
velArray = malloc((numCars+2)*sizeof(int));

srand (time(NULL));

//Initialize starting positions and speeds 
for (i=0;i<numCars;i++){
  posArray[i] = i;
  velArray[i] = 0;
  }

// setup stuff
int fftCounter = 0;


//Setup DFT stuff
double *track2D;
double *outputModSqr;
fftw_complex *fftOutput;
fftw_plan plan_fwd;//,plan_bwd; //Note that this creates 'opaque pointers'
int spaceLength = trackLength;
int timeLength = fftSamples*padding;//pad with zeros for reasons
int outputSL = spaceLength/2+1;
track2D = fftw_malloc(sizeof(double)*timeLength*spaceLength);
fftOutput = fftw_malloc(sizeof(fftw_complex)*timeLength*outputSL);
outputModSqr = fftw_malloc(sizeof(double)*timeLength*outputSL);

for(i=0;i<timeLength*outputSL;i++){
  outputModSqr[i]=0;
}

int t0 = (int)time(NULL);
if(!fftw_import_wisdom_from_filename("32768x8192.wisdom")){
  printf("Wisdom import failed.");
}
plan_fwd = fftw_plan_dft_r2c_2d(timeLength,spaceLength,track2D,fftOutput,FFTW_MEASURE);
int t1 = (int)time(NULL);
printf("Plan creation completed in %.1f minutes.\n",(t1-t0)/60.0);


//Initialization runs
for (step=0;step<dataStartStep;step+=sampleRate){
  runSimulation(posArray, velArray);//Will run sampleRate times
}
printf("Completed Initialization Runs\n");


int startTimeStamp = (int)time(NULL);
///////////////////////////////////////////////////////////
////// MAIN LOOP
///////////////////////////////////////////////////////////
//Data taking runs
//FFT will happen each time this runs and the results will be averaged
for (fftRun=0;fftRun<fftRuns;fftRun++)
{
  fftCounter = 0; //Tracks the number of fftsamples that run

  //zero out array for holding track history
  for(j=0;j<timeLength*spaceLength;j++)
    track2D[j]=0;


  //fills time-space matrix to input to fft
  for (i=0;i<fftSamples;i++)
  {
    runSimulation(posArray, velArray);//Will run sampleRate times
    storeTrackState(posArray,track2D,fftCounter); //Store output of a given step
    fftCounter++;
  }
  //XXX if(fftRun==1){
  //XXX printTrack2D(track2D,spaceLength,timeLength);
  //XXX }
  //return(0);
  //Check that the counter has counted up to fftSamples
  if(fftCounter != fftSamples){
    printf("fftCounter(%d) & fftSamples(%d) mismatch\n",fftCounter,fftSamples);return(-1);
  }

  //printf("Print track\n");
  //WindowReweight(track2D,spaceLength,timeLength);
  //printTrack2D(track2D,spaceLength,timeLength);

  //return(0);
  
  fft(track2D,fftOutput,outputModSqr,plan_fwd,timeLength,spaceLength);

  if((fftRun+1)%250 == 0){
    int currentTimeStamp = (int)time(NULL);
    int seconds = (currentTimeStamp - startTimeStamp);
    int hours = seconds/60/60;
    int minutes = (seconds/60)-(hours*60);
    int remainingSeconds = ((float)seconds/(fftRun+1))*(fftRuns-fftRun);
    printf("Completed %d of %d ffts in %d hrs %d min. Abt %.2f hrs remaining.\n",\
          fftRun+1,fftRuns,hours,minutes,remainingSeconds/60.0/60.0);
  }

}//Completed all ffts

int index;
double avg;
//double scaleF = pow(fftSamples*spaceLength,-2.0);
  //Print result of DFT
  fprintf(outputFile,"numCars,%d,maxVel,%d,trackLength,%d,sampleRate,%d,"
                    "dataStartStep,%d,fftSamples,%d,fftRuns,%d,numDataPoints,%.0f,slowP,%d,padding,%d,%s\n",\
                    numCars,maxVel,trackLength,sampleRate,\
                    dataStartStep,fftSamples,fftRuns,numDataPoints,p,padding,argv[9]);
  for (i=0;i<timeLength;i++){
    for (j=0;j<outputSL;j++){
      index = i*outputSL+j;
      avg = (outputModSqr[index]/(double)fftRuns);
      if(j<outputSL-1)
        fprintf(outputFile,"%.5e,",avg);
      else
        fprintf(outputFile,"%.5e",avg);
    }
    fprintf(outputFile,"\n");
  }


//fclose(outputFile);
fftw_destroy_plan(plan_fwd);
fftw_free(fftOutput);
free(outputModSqr);
fftw_free(track2D);
free(posArray);
free(velArray);
free(fileName);
}/////// END MAIN


/////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////

//Fill in array which stores the track state at each sample step
void storeTrackState(int *posArray,double *track2D, int fftCounter)
{
  int i,t;
  static int test = 0;//0:standard, 1:phi, else:testing

  //Standard
  if(test==0){
    for(i=0;i<numCars;i++){
      track2D[posArray[i]+fftCounter*trackLength]=Hamm(fftCounter);
    }
    //XXX printf("%d: ",fftCounter);
    //XXX for(i=0;i<trackLength;i++){
    //XXX   printf("%d",(int)ceil(track2D[i+fftCounter*trackLength]));
    //XXX }
    //XXX printf("\n");
    //XXX delay(100);
  }
  //phi
  else if(test==1){
    for(i=0;i<numCars;i++){
      if(phiBool(i,posArray)==1){
        track2D[posArray[i]+fftCounter*trackLength]=Hamm(fftCounter);
      }
    }
  }
  //testing
  else{
    double sVal,wavelength,period;
    wavelength = (double)trackLength/1.0;
    period = (double)fftSamples/1.0;
  
    for(i=0;i<trackLength;i++){
     sVal =  cos(2*PI*(3*i/wavelength + 1*fftCounter/period));
     // sVal += cos(2*PI*(0*i/wavelength + 17*fftCounter/period));
     // sVal += cos(2*PI*(1*i/wavelength + 0*fftCounter/period));
     // sVal += cos(2*PI*(1*i/wavelength + 1*fftCounter/period));
     // sVal += cos(2*PI*(3*i/wavelength + 0*fftCounter/period));
     // sVal += cos(2*PI*(0*i/wavelength + 7*fftCounter/period));
     // sVal += cos(2*PI*(5*i/wavelength + 9*fftCounter/period));
     // sVal += cos(2*PI*(6*i/wavelength + 10*fftCounter/period));
     // sVal += (rand()%100)/70;
      track2D[fftCounter*trackLength+i]=sVal*Hamm(fftCounter);//(((double)(rand()%101))/100.0-0.5);//*Hamm(fftCounter);
    }
  }
}
////////// End storeTrackState


void fft(double *track2D,fftw_complex *fftOutput,double *outputModSqr,fftw_plan plan_fwd, int timeLength, int spaceLength)
{
  int i,j,index;
  int outputSL = spaceLength/2+1;
  double modSqr;



  fftw_execute(plan_fwd);



  for(i=0;i<timeLength;i++){
    for(j=0;j<outputSL;j++){
      index = i*outputSL+j;
      modSqr = pow(fftOutput[index][0],2.0)+pow(fftOutput[index][1],2.0);
      outputModSqr[index] = outputModSqr[index]+modSqr;
    }
  }

}


///////////////////////////////////////////////////////////////
// Simulation wrapper
///////////////////////////////////////////////////////////////
void runSimulation(int *posArray, int *velArray)
{
  int i,j;

  for (i=0;i<sampleRate;i++){ //Loop through sampleRate times
    posArray[numCars] = posArray[0];
    velArray[numCars] = velArray[0];
  
    for (j=0;j<numCars;j++){
      updateCar(&posArray[j],&velArray[j],&posArray[j+1]); 
    }
  }
}


///////////////////////////////////////////////////////////////
// Update for one car
///////////////////////////////////////////////////////////////
void updateCar(int *pos1, int *vel1, int *pos2){

  int dist,newVel;
  dist = *pos2 - *pos1 - 1; //gap
  while(dist<0){dist = dist + trackLength;} //Account for wraparound
  
  
  //Assume speedup if possible
  if(*vel1+1 <= maxVel)
    newVel = *vel1+1;
  else
    newVel = *vel1;
  
  //Check for available space
  if (dist >= newVel){
    *vel1 = newVel; //Set new speed if enough distance
    }
  else{ //else set new speed to slow down and exactly close gap
    *vel1 = dist;
  }
  
  if (*vel1 > 0){
    if((rand()%100)+1 <= p)
      *vel1 = *vel1 - 1;
  }
  
  //update position based on new velocity //XXX + (trackLength-maxVel)
  *pos1 = (*pos1 + *vel1) % trackLength;


}

// Welch Window
float welch(int timeStep)
{
  float n = timeStep;
  float N = fftSamples;

  return 1.0 - pow(((n+1)-.5*(N+1))/(.5*(N+1)),2.0);
}

// Hamming Window
float Hamm(int timeStep)
{
  float n = timeStep;
  float N = fftSamples;

  return .5*(1-cos(2*PI*n/(N-1)));
}


int boolCheck(double input){
  if(input==0){
    return 0;
  }
  else{
    return 1;
  }
}

//Calculate phi
int phiBool(int i, int *posArray)
{
  int d1, d2;

  posArray[numCars] = posArray[0];
  posArray[numCars+1] = posArray[1];

  d1 = posArray[i+1] - posArray[i];
  while(d1<0){d1 = d1 + trackLength;}
  d2 = posArray[i+2] - posArray[i+1];
  while(d2<0){d2 = d2 + trackLength;}

  if (d1<(float)maxVel/2.0 && d2<(float)maxVel/2.0)
    return 1;
  else
    return 0;
}


void printTrack2D(double *track2D,int spaceLength,int timeLength)
{
  printf("%d",timeLength);
//track2D array: [first step][second step][third step]...
  char *trackFile;
  asprintf(&trackFile, "../Track_random_nopad_nowin.csv"); //XXX
  printf("%s\n",trackFile);
  FILE *outputFileT;
  outputFileT = fopen(trackFile,"w");

  int i,j,index;
  for(i=0;i<timeLength;i++){
    //printf("row: %2d ",i);
    for(j=0;j<spaceLength;j++){
      index = i*trackLength+j;
     // printf("%.2f ",track2D[index]);
      fprintf(outputFileT,"%.5e",track2D[index]);
      if(j<spaceLength-1){
        fprintf(outputFileT,",");
      }
    }
    fprintf(outputFileT,"\n");
    //printf("\n");
  }
  //printf("\n\n");
free(trackFile);
}


void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}
